# Security

Globular takes security seriously.

Globular uses a LN node, via a Lighter instance, as a wallet.
It is therefore a lightweight application and only needs to handle a few secrets that can be
easily managed.
This means keeping funds in a proper and secure place while providing ease of use through a simple
authorization mechanism.

In case the credentials are compromised, the Lighter instance needs to be reconfigured to use new
ones, so everyone is forced to update them in order to regain access.
We chose to store only what is really necessary and to generate encryption secrets at runtime.


## Connection to Lighter

In order to securely connect to a Lighter instance, Globular uses gRPC,
which uses HTTP/2 along with a TLSv1.2 certificate.
The certificate needs to match the one configured in Lighter, as it is validated client-side, and
it can be self-signed.

To authenticate the request, a Lighter macaroon has to be provided.
A macaroon is an arbitrary long string which embeds a list of allowed requests.
This is included in each call as metadata in the gRPC context.

An option to disable secure connection (plain text communication without authentication) is
available in Globular settings,
although this is highly discouraged in general and especially when connecting to a mainnet node.


## Secrets storage and encryption

The certificate and the macaroon can be scanned or copied from clipboard,
they are stored in encrypted form in Android's SharedPreferences.

To encrypt these secrets, a 6 digits PIN is set at first-time configuration.
After 3 PIN wrong insertions, all data stored in the SharedPreferences gets deleted. 

A 32-bytes stretched key (`accessKey`) is generated at runtime through
[Scrypt](https://en.wikipedia.org/wiki/Scrypt)
(using [Bouncy Castle Crypto Package for Java](https://github.com/bcgit/bc-java)).
The generation parameters, a 32-byte randomly generated salt, a cost factor, a parallelization
factor and an output key length, are stored in clear in the SharedPreferences and used together
with the PIN to generate the key.
This access key is calculated and available only at runtime, after PIN insertion and validation,
and reset after session timeout (3 minutes).

Using the access key, we encrypt a hardcoded
string (`accessToken`) and save the result in the SharedPreferences.
Later, to verify its correctness and grant access to the app, we decrypt the stored secret
and assure it matches the expected string.

All encryption/decryption operations are done using Secretbox
(using [Lazysodium for Java](https://github.com/terl/lazysodium-java)), which uses
a new 32-byte randomly generated salt (attached in clear to the cipher text)
and the stretched key generated with Scrypt.
Secretbox, created by Daniel J. Bernstein, uses
[XSalsa20](https://en.wikipedia.org/wiki/Salsa20) and
[Poly1305](https://en.wikipedia.org/wiki/Poly1305) to encrypt and authenticate messages
with secret-key cryptography.

These same technologies are used by
[Lighter](https://gitlab.com/inbitcoin/lighter/blob/master/doc/security.md) and 
[LND](https://github.com/lightningnetwork/lnd/tree/master/macaroons).
