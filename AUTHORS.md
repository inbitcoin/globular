Globular contributors, in chronological order of contribution:

* [Zoe Faltibà](https://gitlab.com/zoedberg) - zoe@inbitcoin.it
* [Nicola Busanello](https://gitlab.com/dieeasy) - sudo@inbitcoin.it
* [Antonio Parrella](https://gitlab.com/zidagar) - zidagar@inbitcoin.it
* [Nicola Vaccari](https://gitlab.com/nik7B7) - nik@inbitcoin.it
* [Patrick Peterle](https://gitlab.com/patrix252) - patrix@inbitcoin.it