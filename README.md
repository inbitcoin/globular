# Globular - Cross-implementation Lightning Network wallet

Globular is the first cross-implementation LN Android remote wallet.

It connects to [Lighter](https://gitlab.com/inbitcoin/lighter) (develop branch), a LN node wrapper,
with macaroon authentication. Lighter is open source and can easily be self hosted.

Globular is a demo client for Lighter but can be easily extended to become a fully-fledged
wallet application.


## Current features

At the moment, Globular offers:
* first time configuration
* Lighter pairing procedure
* pin entry and session timeout
* on-chain and off-chain balances
* a transfers list: on-chain transactions and off-chain invoices and payments
* a transfer detail view
* payments over the LN and on-chain
* creation and sharing of LN invoices and on-chain payment requests
* a channels list
* a channel detail view
* LN channel opening and closing
* showing and sharing the node's URI to ease opening of channels to the LN node
* an optional background service regularly checking the connection to Lighter and sending error
messages through in-app and drawer notifications
* a settings view to lock and unlock Lighter, manage PIN, background service, demo mode and every
Lighter connection parameter

The demo mode, available in settings, makes Globular connect to a testnet Lighter instance
(keeping stored credentials untouched).

 
## Project overview

The application is protected by a PIN that is used to encrypt all the secrets.
Read [Security](/doc/Security.md) to learn more.

Connection to the Lighter server is achieved over [gRPC](https://grpc.io/) with
[protobuf](https://github.com/protocolbuffers/protobuf), client TLS certificate
validation and using [macaroons](https://ai.google/research/pubs/pub41892)
as authorization credentials.

Globular is built according to the MVVM architecture principles, using
[Dagger 2](https://github.com/google/dagger),
[LiveData](https://developer.android.com/topic/libraries/architecture/livedata),
[RxJava 2](https://github.com/ReactiveX/RxJava)
and [Retrofit](https://github.com/square/retrofit).

[Android X](https://developer.android.com/jetpack/androidx)
is used as support library and the minimum SDK version is 24 (Android 7.0 Nougat).

Globular has the following structure:

* **java/it/inbitcoin/globular**
  * **data**: storage and network access and management
  * **di**: dependency injection
  * **ui**: user interface
  * **utils**: utility classes
* **proto**
  * lighter.proto: Lighter's API protobuf definition
* **res**: XML layouts, string files and images

Globular needs permission to:
* access the **internet** connection: to connect to Lighter and the QR code generation service
* access the **camera**: to read QR codes
* access the **network state**: to check connectivity
* **vibrate**: to provide haptic feedback


## Code of Conduct

This project adheres to No Code of Conduct. We are all adults. We accept anyone's contributions.
Nothing else matters.

For more information please visit the [No Code of Conduct](https://github.com/domgetter/NCoC)
homepage.


## Contributing

All contributions to Globular are welcome.

If you like the project and would like to donate: `bc1qvh23ppfxwkkk706w2gufszmkljzkzjav38en8m`