/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.data;


import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.goterl.lazycode.lazysodium.exceptions.SodiumException;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import it.inbitcoin.globular.AddressType;
import it.inbitcoin.globular.data.model.GrpcResponse;
import it.inbitcoin.globular.data.model.LightningGrpcCall;
import it.inbitcoin.globular.data.model.LightningGrpcRunnable;
import it.inbitcoin.globular.data.model.LockerGrpcCall;
import it.inbitcoin.globular.data.model.LockerGrpcRunnable;
import it.inbitcoin.globular.data.model.UnlockerGrpcCall;
import it.inbitcoin.globular.data.model.UnlockerGrpcRunnable;
import it.inbitcoin.globular.data.network.LightningGrpcRunnableImpl;
import it.inbitcoin.globular.data.network.LockerGrpcRunnableImpl;
import it.inbitcoin.globular.data.network.UnlockerGrpcRunnableImpl;
import it.inbitcoin.globular.data.prefs.PreferencesHelper;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.ApiEndPoint;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.CipherUtil;
import it.inbitcoin.globular.utils.PrefDefaults;

@Singleton
public class AppRepository {

    private static final String TAG = "AppRepository";

    private final ApiEndPoint mApiEndPoint;

    private final PreferencesHelper mPreferencesHelper;

    private final AccessFactory mAccessFactory;


    @Inject
    public AppRepository(AccessFactory accessFactory, ApiEndPoint apiEndPoint,
                         PreferencesHelper preferencesHelper) {
        mAccessFactory = accessFactory;
        mApiEndPoint = apiEndPoint;
        mPreferencesHelper = preferencesHelper;
    }

    public static class MergedResponse {

        public final GrpcResponse getInfoResponse;
        public final GrpcResponse channelBalanceResponse;
        public final GrpcResponse walletBalanceResponse;
        public final GrpcResponse invoicesResponse;
        public final GrpcResponse paymentsResponse;
        public final GrpcResponse transactionsResponse;

        MergedResponse(GrpcResponse getInfoResponse, GrpcResponse channelBalanceResponse,
                              GrpcResponse walletBalanceResponse, GrpcResponse invoicesResponse,
                              GrpcResponse paymentsResponse, GrpcResponse transactionsResponse){
            this.getInfoResponse = getInfoResponse;
            this.channelBalanceResponse = channelBalanceResponse;
            this.walletBalanceResponse = walletBalanceResponse;
            this.invoicesResponse = invoicesResponse;
            this.paymentsResponse = paymentsResponse;
            this.transactionsResponse = transactionsResponse;
        }
    }

    public Single<MergedResponse> getMainData() {
        return Single.zip(getInfo(), getChannelBalance(), getWalletBalance(), getInvoices(),
                getPayments(), getTransactions(), MergedResponse::new)
                .subscribeOn(Schedulers.io());
    }

    public Single<GrpcResponse> getInfo() {
        LightningGrpcRunnable grpcRunnable = new LightningGrpcRunnableImpl.GetInfoRunnable();
        return Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .subscribeOn(Schedulers.io());
    }

    private Single<GrpcResponse> getChannelBalance() {
        LightningGrpcRunnable grpcRunnable = new LightningGrpcRunnableImpl.ChannelBalanceRunnable();
        return Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .subscribeOn(Schedulers.io());
    }

    private Single<GrpcResponse> getWalletBalance() {
        LightningGrpcRunnable grpcRunnable = new LightningGrpcRunnableImpl.WalletBalanceRunnable();
        return Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .subscribeOn(Schedulers.io());
    }

    private Single<GrpcResponse> getInvoices() {
        LightningGrpcRunnable grpcRunnable = new LightningGrpcRunnableImpl.ListInvoicesRunnable();
        return Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .subscribeOn(Schedulers.io());
    }

    private Single<GrpcResponse> getPayments() {
        LightningGrpcRunnable grpcRunnable = new LightningGrpcRunnableImpl.ListPaymentsRunnable();
        return Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .subscribeOn(Schedulers.io());
    }

    private Single<GrpcResponse> getTransactions() {
        LightningGrpcRunnable grpcRunnable = new LightningGrpcRunnableImpl.ListTransactionsRunnable();
        return Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .subscribeOn(Schedulers.io());
    }

    public static class ChannelsResponse {

        public final GrpcResponse listChannelsResponse;
        public final GrpcResponse listPeersResponse;

        ChannelsResponse(GrpcResponse listChannelsResponse, GrpcResponse listPeersResponse){
            this.listChannelsResponse = listChannelsResponse;
            this.listPeersResponse = listPeersResponse;
        }
    }

    public Single<ChannelsResponse> getChannelsData() {
        return Single.zip(getChannels(), getPeers(), ChannelsResponse::new)
                .subscribeOn(Schedulers.io());
    }

    public Single<GrpcResponse> getChannels() {
        LightningGrpcRunnable grpcRunnable = new LightningGrpcRunnableImpl.ListChannelsRunnable();
        return Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .subscribeOn(Schedulers.io());
    }

    private Single<GrpcResponse> getPeers() {
        LightningGrpcRunnable grpcRunnable = new LightningGrpcRunnableImpl.ListPeersRunnable();
        return Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .subscribeOn(Schedulers.io());
    }

    public Single<GrpcResponse> openChannel(@NonNull String nodeUri, @NonNull double fundingBits,
                                            double pushBits, boolean privateChannel) {
        LightningGrpcRunnable grpcRunnable = new LightningGrpcRunnableImpl.OpenChannelRunnable(
                nodeUri, fundingBits, pushBits, privateChannel);
        return Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .subscribeOn(Schedulers.io());
    }

    public void payOffchain(MutableLiveData<GrpcResponse> cachedPayInvoiceResponse, String paymentRequest, double amount) {
        LightningGrpcRunnable grpcRunnable = new LightningGrpcRunnableImpl.PayInvoiceRunnable(paymentRequest, amount);
        Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<GrpcResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(GrpcResponse grpcResponse) {
                        cachedPayInvoiceResponse.postValue(grpcResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        GrpcResponse grpcResponse = new GrpcResponse(
                                null, new StatusRuntimeException(Status.ABORTED));
                        cachedPayInvoiceResponse.postValue(grpcResponse);
                    }
                });
    }

    public void payOnChain(MutableLiveData<GrpcResponse> cachedPayOnChainResponse,
                           String paymentRequest, double amount, long satByte) {
        LightningGrpcRunnable grpcRunnable = new LightningGrpcRunnableImpl.PayOnChainRunnable(
                paymentRequest, amount, satByte);
        Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<GrpcResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(GrpcResponse grpcResponse) {
                        cachedPayOnChainResponse.postValue(grpcResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        GrpcResponse grpcResponse = new GrpcResponse(
                                null, new StatusRuntimeException(Status.ABORTED));
                        cachedPayOnChainResponse.postValue(grpcResponse);
                    }
                });
    }

    public void createInvoice(MutableLiveData<GrpcResponse> cachedCreateInvoiceResponse, double amount, String description) {
        LightningGrpcRunnable grpcRunnable = new LightningGrpcRunnableImpl.CreateInvoiceRunnable(amount, description);
        Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<GrpcResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(GrpcResponse grpcResponse) {
                        cachedCreateInvoiceResponse.postValue(grpcResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        GrpcResponse grpcResponse = new GrpcResponse(
                                null, new StatusRuntimeException(Status.ABORTED));
                        cachedCreateInvoiceResponse.postValue(grpcResponse);
                    }
                });
    }

    public void decodeInvoice(MutableLiveData<GrpcResponse> cachedDecodeInvoiceResponse, String paymentRequest) {
        LightningGrpcRunnable grpcRunnable = new LightningGrpcRunnableImpl.DecodeInvoiceRunnable(paymentRequest);
        Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<GrpcResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(GrpcResponse grpcResponse) {
                        cachedDecodeInvoiceResponse.postValue(grpcResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        GrpcResponse grpcResponse = new GrpcResponse(
                                null, new StatusRuntimeException(Status.ABORTED));
                        cachedDecodeInvoiceResponse.postValue(grpcResponse);
                    }
                });
    }

    public void newAddress(MutableLiveData<GrpcResponse> cachedNewAddressResponse,
                           AddressType addressType) {
        LightningGrpcRunnable grpcRunnable =
                new LightningGrpcRunnableImpl.NewAddressRunnable(addressType);
        Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<GrpcResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(GrpcResponse grpcResponse) {
                        cachedNewAddressResponse.postValue(grpcResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        GrpcResponse grpcResponse = new GrpcResponse(
                                null, new StatusRuntimeException(Status.ABORTED));
                        cachedNewAddressResponse.postValue(grpcResponse);
                    }
                });
    }

    public Single<GrpcResponse> unlockLighter(String password, boolean unlockNode) {
        UnlockerGrpcRunnable grpcRunnable = new UnlockerGrpcRunnableImpl
                .UnlockLighterRunnable(password, unlockNode);
        return Single.create(new UnlockerGrpcCall(mApiEndPoint, grpcRunnable))
                .subscribeOn(Schedulers.io());
    }

    public Single<GrpcResponse> lockLighter(String password) {
        LockerGrpcRunnable grpcRunnable = new LockerGrpcRunnableImpl.LockLighterRunnable(password);
        return Single.create(new LockerGrpcCall(mApiEndPoint, grpcRunnable))
                .subscribeOn(Schedulers.io());
    }

    public Single<GrpcResponse> unlockNode(String password) {
        LightningGrpcRunnable grpcRunnable = new LightningGrpcRunnableImpl
                .UnlockNodeRunnable(password);
        return Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .subscribeOn(Schedulers.io());
    }

    public Single<GrpcResponse> closeChannel(String channelId, boolean forceClose) {
        LightningGrpcRunnable grpcRunnable =
                new LightningGrpcRunnableImpl.CloseChannelRunnable(channelId, forceClose);
        return Single.create(new LightningGrpcCall(mApiEndPoint, grpcRunnable))
                .subscribeOn(Schedulers.io());
    }

    public void updateLighterCredentials() {
        updateLighterDemoModeAsync()
                .andThen(updateLighterHostAsync())
                .andThen(updateLighterPortAsync())
                .andThen(updateLighterSecureAsync())
                .andThen(updateLighterTLSCertificateAsync())
                .andThen(updateLighterMacaroonAsync())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "updated lighter credentials");
                        mApiEndPoint.setReady(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "error updating credentials: ", e);
                    }
                });
    }

    public Completable updateLighterDemoModeAsync() {
        return Completable.create(emitter -> {
            boolean lighterDemoMode = mPreferencesHelper.getLighterDemoMode();
            mApiEndPoint.setLighterDemoMode(lighterDemoMode);
            emitter.onComplete();
        });
    }

    public Completable updateLighterHostAsync() {
        return Completable.create(emitter -> {
            String host = mPreferencesHelper.getLighterHost();
            mApiEndPoint.setHost(host);
            emitter.onComplete();
        });
    }

    public Completable updateLighterPortAsync() {
        return Completable.create(emitter -> {
            int port = PrefDefaults.lighterPort;
            try {
                port = Integer.parseInt(mPreferencesHelper.getLighterPort());
            } catch (NumberFormatException ignored) {}
            mApiEndPoint.setPort(port);
            emitter.onComplete();
        });
    }

    public Completable updateLighterSecureAsync() {
        return Completable.create(emitter -> {
            boolean isSecureEnabled = mPreferencesHelper.getLighterSecureConn();
            mApiEndPoint.setSecureEnabled(isSecureEnabled);
            emitter.onComplete();
        });
    }

    public Completable updateLighterTLSCertificateAsync() {
        return Completable.create(emitter -> {
            String cryptedCert = mPreferencesHelper.getLighterTlsCertificate();
            if (!cryptedCert.isEmpty()) {
                try {
                    String TLSCertificate = CipherUtil.decryptData(cryptedCert, mAccessFactory);
                    mApiEndPoint.setTLSCertificate(TLSCertificate);
                    Log.d(TAG, "certificate updated");
                } catch (SodiumException ignored) {}
            }
            emitter.onComplete();
        });
    }

    public Completable updateLighterMacaroonAsync() {
        return Completable.create(emitter -> {
            String cryptedMac = mPreferencesHelper.getLighterMacaroon();
            if (!cryptedMac.isEmpty()) {
                try {
                    String decryptedMacaroon = CipherUtil.decryptData(cryptedMac, mAccessFactory);
                    mApiEndPoint.setMacaroon(AppUtils.parseMacaroon(decryptedMacaroon));
                    Log.d(TAG, "macaroon updated");
                } catch (SodiumException ignored) {}
            }
            emitter.onComplete();
        });
    }

    public void updateLighterHost() {
        updateLighterHostAsync()
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    public void updateLighterPort() {
        updateLighterPortAsync()
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    public void updateLighterSecure() {
        updateLighterSecureAsync()
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    public void updateLighterTLSCertificate() {
        updateLighterTLSCertificateAsync()
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    public void updateLighterMacaroon() {
        updateLighterMacaroonAsync()
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    public Completable recryptData() {
        return Completable.create(emitter -> {
            Log.d(TAG, "recrypting data");
            RxJavaPlugins.setErrorHandler(e -> {
                if (e instanceof SodiumException)
                    return;
                Log.w("Undeliverable exception received: ", e);
            });

            byte[] oldAccessKey = mAccessFactory.getOldAccessKey();
            try {
                String cryptedCert = mPreferencesHelper.getLighterTlsCertificate();
                if (!cryptedCert.isEmpty()) {
                    String certificate = CipherUtil.decrypt(cryptedCert, oldAccessKey);
                    String cryptCert = CipherUtil.cryptData(certificate, mAccessFactory);
                    mPreferencesHelper.setLighterTlsCertificate(cryptCert);
                }
                String cryptedMac = mPreferencesHelper.getLighterMacaroon();
                if (!cryptedMac.isEmpty()) {
                    String macaroon = CipherUtil.decrypt(cryptedMac, oldAccessKey);
                    String cryptMac = CipherUtil.cryptData(macaroon, mAccessFactory);
                    mPreferencesHelper.setLighterMacaroon(cryptMac);
                }
                emitter.onComplete();
                mAccessFactory.resetOldAccessKey();
            } catch (SodiumException e) {
                Log.d(TAG, "SodiumExcaption");
                emitter.onError(e);
            }

        });
    }

}