/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.inbitcoin.globular.utils.PrefDefaults;

@Singleton
public class PreferencesHelper {

    private static final String ACCESS_TOKEN = "access_token";
    private static final String SCRYPT_PARAMS = "scrypt_params";
    private static final String PIN_FAILED_ATTEMPTS = "pin_failed_attempts";
    public static final String HAPTIC_PIN = "haptic_pin";
    public static final String SCRAMBLE_PIN = "scramble_pin";
    public static final String NET_SERVICE_ENABLE = "network_service_enable";
    public static final String NET_SERVICE_INTERVAL = "network_service_interval";

    public static final String LIGHTER_HOST = "lighter_host";
    public static final String LIGHTER_PORT = "lighter_port";
    public static final String LIGHTER_SECURE_CONN = "enable_secure_connection";
    public static final String LIGHTER_MACAROON = "macaroon";
    public static final String LIGHTER_TLS_CERTIFICATE = "TLS_certificate";
    public static final String LIGHTER_DEMO_MODE = "lighter_demo_mode";

    private final SharedPreferences sharedPreferences;


    @Inject
    public PreferencesHelper(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public void deleteAllPreferences() {
        sharedPreferences.edit().clear().apply();
    }

    public void putStringKeyValue(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public String getAccessToken() {
        return sharedPreferences.getString(ACCESS_TOKEN, PrefDefaults.accessToken);
    }

    public void setAccessToken(String accessToken) {
        sharedPreferences.edit().putString(ACCESS_TOKEN, accessToken).apply();
    }

    public String getScryptParams() {
        return sharedPreferences.getString(SCRYPT_PARAMS, PrefDefaults.scryptParams);
    }

    public void setScryptParams(String scryptParams) {
        sharedPreferences.edit().putString(SCRYPT_PARAMS, scryptParams).apply();
    }

    public int getPinFailedAttempts() {
        return sharedPreferences.getInt(PIN_FAILED_ATTEMPTS, PrefDefaults.pinFailedAttempts);
    }

    public void setPinFailedAttempts(int pinFailedAttempts) {
        sharedPreferences.edit().putInt(PIN_FAILED_ATTEMPTS, pinFailedAttempts).apply();
    }

    public boolean getHapticPin() {
        return sharedPreferences.getBoolean(HAPTIC_PIN, PrefDefaults.hapticPin);
    }

    public void setHapticPin(boolean hapticPin) {
        sharedPreferences.edit().putBoolean(HAPTIC_PIN, hapticPin).apply();
    }

    public boolean getScramblePin() {
        return sharedPreferences.getBoolean(SCRAMBLE_PIN, PrefDefaults.scramblePin);
    }

    public void setScramblePin(boolean scramblePin) {
        sharedPreferences.edit().putBoolean(SCRAMBLE_PIN, scramblePin).apply();
    }

    public boolean getNetServiceEnable() {
        return sharedPreferences.getBoolean(NET_SERVICE_ENABLE, PrefDefaults.netServiceEnable);
    }

    public void setNetServiceEnable(boolean netServiceEnable) {
        sharedPreferences.edit().putBoolean(NET_SERVICE_ENABLE, netServiceEnable).apply();
    }

    public long getNetServiceInterval() {
        long interval = PrefDefaults.netServiceBackgroundInterval;
        try {
            interval = Long.parseLong(sharedPreferences.getString(
                    NET_SERVICE_INTERVAL, String.valueOf(PrefDefaults.netServiceBackgroundInterval)));
        } catch (NumberFormatException | NullPointerException ignored) {}
        return interval;
    }

    public void setNetServiceInterval(long netServiceInterval) {
        sharedPreferences.edit().putString(NET_SERVICE_INTERVAL, String.valueOf(netServiceInterval)).apply();
    }

    public String getLighterHost() {
        return sharedPreferences.getString(LIGHTER_HOST, PrefDefaults.lighterHost);
    }

    public void setLighterHost(String lighterHost) {
        sharedPreferences.edit().putString(LIGHTER_HOST, lighterHost).apply();
    }

    public String getLighterPort() {
        return sharedPreferences.getString(LIGHTER_PORT, String.valueOf(PrefDefaults.lighterPort));
    }

    public void setLighterPort(String lighterPort) {
        sharedPreferences.edit().putString(LIGHTER_PORT, lighterPort).apply();
    }

    public boolean getLighterSecureConn() {
        return sharedPreferences.getBoolean(
                LIGHTER_SECURE_CONN, PrefDefaults.lighterSecureConnection);
    }

    public void setLighterSecureConn(boolean lighterSecureConn) {
        sharedPreferences.edit().putBoolean(LIGHTER_SECURE_CONN, lighterSecureConn).apply();
    }

    public String getLighterMacaroon() {
        return sharedPreferences.getString(LIGHTER_MACAROON, PrefDefaults.lighterMacaroon);
    }

    public void setLighterMacaroon(String lighterMacaroon) {
        sharedPreferences.edit().putString(LIGHTER_MACAROON, lighterMacaroon).apply();
    }

    public String getLighterTlsCertificate() {
        return sharedPreferences.getString(
                LIGHTER_TLS_CERTIFICATE, PrefDefaults.lighterTLSCertificate);
    }

    public void setLighterTlsCertificate(String lighterTlsCertificate) {
        sharedPreferences.edit().putString(LIGHTER_TLS_CERTIFICATE, lighterTlsCertificate).apply();
    }

    public boolean getLighterDemoMode() {
        return sharedPreferences.getBoolean(LIGHTER_DEMO_MODE, PrefDefaults.lighterDemoMode);
    }

    public void setLighterDemoMode(boolean lighterDemoMode) {
        sharedPreferences.edit().putBoolean(LIGHTER_DEMO_MODE, lighterDemoMode).apply();
    }

}
