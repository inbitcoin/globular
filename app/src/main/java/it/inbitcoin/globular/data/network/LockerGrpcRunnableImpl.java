/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.data.network;

import android.util.Log;

import it.inbitcoin.globular.LockLighterRequest;
import it.inbitcoin.globular.LockLighterResponse;
import it.inbitcoin.globular.LockerGrpc;
import it.inbitcoin.globular.data.model.LockerGrpcRunnable;

public class LockerGrpcRunnableImpl {

    public static class LockLighterRunnable implements LockerGrpcRunnable {

        private static final String TAG = "LockLighterRunnable";

        private String password;

        public LockLighterRunnable(String password) {
            this.password = password;
        }

        @Override
        public LockLighterResponse run(LockerGrpc.LockerBlockingStub blockingStub,
                                       LockerGrpc.LockerStub asyncStub) {
            Log.d(TAG, "Start");
            LockLighterRequest request = LockLighterRequest.newBuilder()
                    .setPassword(password).build();
            return blockingStub.lockLighter(request);
        }
    }

}
