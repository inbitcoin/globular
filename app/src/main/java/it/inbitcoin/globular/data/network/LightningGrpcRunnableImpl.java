/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.data.network;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.protobuf.GeneratedMessageLite;

import it.inbitcoin.globular.AddressType;
import it.inbitcoin.globular.ChannelBalanceRequest;
import it.inbitcoin.globular.ChannelBalanceResponse;
import it.inbitcoin.globular.CloseChannelRequest;
import it.inbitcoin.globular.CloseChannelResponse;
import it.inbitcoin.globular.CreateInvoiceRequest;
import it.inbitcoin.globular.CreateInvoiceResponse;
import it.inbitcoin.globular.DecodeInvoiceRequest;
import it.inbitcoin.globular.DecodeInvoiceResponse;
import it.inbitcoin.globular.GetInfoRequest;
import it.inbitcoin.globular.GetInfoResponse;
import it.inbitcoin.globular.LightningGrpc.LightningBlockingStub;
import it.inbitcoin.globular.LightningGrpc.LightningStub;
import it.inbitcoin.globular.ListChannelsRequest;
import it.inbitcoin.globular.ListInvoicesRequest;
import it.inbitcoin.globular.ListInvoicesResponse;
import it.inbitcoin.globular.ListPaymentsRequest;
import it.inbitcoin.globular.ListPaymentsResponse;
import it.inbitcoin.globular.ListPeersRequest;
import it.inbitcoin.globular.ListTransactionsRequest;
import it.inbitcoin.globular.ListTransactionsResponse;
import it.inbitcoin.globular.NewAddressRequest;
import it.inbitcoin.globular.OpenChannelRequest;
import it.inbitcoin.globular.Order;
import it.inbitcoin.globular.PayInvoiceRequest;
import it.inbitcoin.globular.PayInvoiceResponse;
import it.inbitcoin.globular.PayOnChainRequest;
import it.inbitcoin.globular.PayOnChainResponse;
import it.inbitcoin.globular.UnlockNodeRequest;
import it.inbitcoin.globular.WalletBalanceRequest;
import it.inbitcoin.globular.WalletBalanceResponse;
import it.inbitcoin.globular.data.model.LightningGrpcRunnable;

public class LightningGrpcRunnableImpl {

    public static class ChannelBalanceRunnable implements LightningGrpcRunnable {

        private static final String TAG = "ChannelBalanceRunnable";

        @Override
        public ChannelBalanceResponse run(LightningBlockingStub blockingStub,
                                          LightningStub asyncStub) {
            Log.d(TAG, "Start");
            ChannelBalanceRequest request = ChannelBalanceRequest.newBuilder().build();
            return blockingStub.channelBalance(request);
        }
    }

    public static class CloseChannelRunnable implements LightningGrpcRunnable {

        private static final String TAG = "CloseChannelRunnable";

        private String channelId;
        private boolean forceClose;

        public CloseChannelRunnable(String channelId, boolean forceClose) {
            this.channelId = channelId;
            this.forceClose = forceClose;
        }

        @Override
        public CloseChannelResponse run(LightningBlockingStub blockingStub,
                                        LightningStub asyncStub) {
            Log.d(TAG, "Start");
            CloseChannelRequest request = CloseChannelRequest.newBuilder()
                    .setChannelId(channelId).setForce(forceClose).build();
            return blockingStub.closeChannel(request);
        }
    }

    public static class WalletBalanceRunnable implements LightningGrpcRunnable {

        private static final String TAG = "WalletBalanceRunnable";

        @Override
        public WalletBalanceResponse run(LightningBlockingStub blockingStub,
                                         LightningStub asyncStub) {
            Log.d(TAG, "Start");
            WalletBalanceRequest request = WalletBalanceRequest.newBuilder().build();
            return blockingStub.walletBalance(request);
        }
    }

    public static class GetInfoRunnable implements LightningGrpcRunnable {

        private static final String TAG = "GetInfoRunnable";

        @Override
        public GetInfoResponse run(LightningBlockingStub blockingStub,
                                   LightningStub asyncStub) {
            Log.d(TAG, "Start");
            GetInfoRequest request = GetInfoRequest.newBuilder().build();
            return blockingStub.getInfo(request);
        }
    }

    public static class ListChannelsRunnable implements LightningGrpcRunnable {

        private static final String TAG = "ListChannelsRunnable";


        @Override
        public GeneratedMessageLite run(LightningBlockingStub blockingStub,
                                        LightningStub asyncStub) {
            Log.d(TAG, "Start");
            ListChannelsRequest request = ListChannelsRequest.newBuilder().build();
            return blockingStub.listChannels(request);
        }
    }

    public static class ListPeersRunnable implements LightningGrpcRunnable {

        private static final String TAG = "ListPeersRunnable";


        @Override
        public GeneratedMessageLite run(LightningBlockingStub blockingStub,
                                        LightningStub asyncStub) {
            Log.d(TAG, "Start");
            ListPeersRequest request = ListPeersRequest.newBuilder().build();
            return blockingStub.listPeers(request);
        }
    }

    public static class ListInvoicesRunnable implements LightningGrpcRunnable {

        private static final String TAG = "ListInvoicesRunnable";

        private final int maxInvoices = 100;

        @Override
        public ListInvoicesResponse run(LightningBlockingStub blockingStub,
                                        LightningStub asyncStub) {
            Log.d(TAG, "Start");
            ListInvoicesResponse response;
            ListInvoicesResponse.Builder finalResponse = ListInvoicesResponse.newBuilder();
            long timestamp = 0;
            do {
                ListInvoicesRequest request = ListInvoicesRequest.newBuilder()
                        .setMaxItems(maxInvoices)
                        .setPaid(true).setPending(true).setExpired(false)
                        .setListOrder(Order.DESCENDING)
                        .setSearchTimestamp(timestamp)
                        .setSearchOrder(Order.DESCENDING).build();
                response = blockingStub.listInvoices(request);
                finalResponse.mergeFrom(response);
                if (response != null) {
                    int lastIndex = response.getInvoicesCount() - 1;
                    if (lastIndex >= maxInvoices - 1) {
                        timestamp = response.getInvoices(lastIndex).getTimestamp();
                    } else {
                        timestamp = 0;
                    }
                }
            } while (timestamp > 0);
            return finalResponse.build();
        }
    }

    public static class ListPaymentsRunnable implements LightningGrpcRunnable {

        private static final String TAG = "ListPaymentsRunnable";

        @Override
        public ListPaymentsResponse run(LightningBlockingStub blockingStub,
                                        LightningStub asyncStub) {
            return listPayments(blockingStub);
        }

        private ListPaymentsResponse listPayments(LightningBlockingStub blockingStub) {
            Log.d(TAG, "Start");
            ListPaymentsRequest request = ListPaymentsRequest.newBuilder().build();
            return blockingStub.listPayments(request);
        }
    }

    public static class ListTransactionsRunnable implements LightningGrpcRunnable {

        private static final String TAG = "ListTransactionsRunnable";

        @Override
        public ListTransactionsResponse run(LightningBlockingStub blockingStub,
                                            LightningStub asyncStub) {
            Log.d(TAG, "Start");
            ListTransactionsRequest request = ListTransactionsRequest.newBuilder().build();
            return blockingStub.listTransactions(request);
        }
    }

    public static class OpenChannelRunnable implements LightningGrpcRunnable {

        private static final String TAG = "OpenChannelRunnable";

        String nodeUri;
        double fundingBits;
        double pushBits;
        boolean privateChannel;

        public OpenChannelRunnable(@NonNull String nodeUri, @NonNull double fundingBits,
                                   double pushBits, boolean privateChannel) {
            this.nodeUri = nodeUri;
            this.fundingBits = fundingBits;
            this.pushBits = pushBits;
            this.privateChannel = privateChannel;
        }

        @Override
        public GeneratedMessageLite run(LightningBlockingStub blockingStub, LightningStub asyncStub) {
            Log.d(TAG, "Start");
            OpenChannelRequest.Builder builder = OpenChannelRequest.newBuilder()
                    .setNodeUri(nodeUri).setFundingBits(fundingBits)
                    .setPrivate(privateChannel);
            if (pushBits > 0)
                builder.setPushBits(pushBits);
            OpenChannelRequest request = builder.build();
            return blockingStub.openChannel(request);
        }
    }

    public static class NewAddressRunnable implements LightningGrpcRunnable {

        private static final String TAG = "NewAddressRunnable";

        private final AddressType addressType;

        public NewAddressRunnable(AddressType addressType) {
            this.addressType = addressType;
        }

        @Override
        public GeneratedMessageLite run(LightningBlockingStub blockingStub,
                                        LightningStub asyncStub) {
            Log.d(TAG, "Start");
            NewAddressRequest request = NewAddressRequest.newBuilder()
                    .setType(addressType).build();
            return blockingStub.newAddress(request);
        }
    }

    public static class PayInvoiceRunnable implements LightningGrpcRunnable {

        private static final String TAG = "PayInvoiceRunnable";

        private final String paymentRequest;
        private final double amount;

        public PayInvoiceRunnable(String paymentRequest, double amount) {
            this.paymentRequest = paymentRequest;
            this.amount = amount;
        }

        @Override
        public PayInvoiceResponse run(LightningBlockingStub blockingStub,
                                      LightningStub asyncStub) {
            Log.d(TAG, "Start");
            PayInvoiceRequest request = PayInvoiceRequest.newBuilder()
                    .setPaymentRequest(this.paymentRequest).setAmountBits(amount).build();
            return blockingStub.payInvoice(request);
        }
    }

    public static class PayOnChainRunnable implements LightningGrpcRunnable {

        private static final String TAG = "PayOnChainRunnable";

        private final String paymentRequest;
        private final double amount;
        private final long feeSat;

        public PayOnChainRunnable(String paymentRequest, double amount, long feeSat) {
            this.paymentRequest = paymentRequest;
            this.amount = amount;
            this.feeSat = feeSat;
        }

        @Override
        public PayOnChainResponse run(LightningBlockingStub blockingStub,
                                      LightningStub asyncStub) {
            Log.d(TAG, "Start");
            Log.d(TAG, "pay req " + paymentRequest + " amount " + amount + " fee " + feeSat);
            PayOnChainRequest request = PayOnChainRequest.newBuilder()
                    .setAddress(paymentRequest).setAmountBits(amount).setFeeSatByte(feeSat).build();
            return blockingStub.payOnChain(request);
        }
    }

    public static class CreateInvoiceRunnable implements LightningGrpcRunnable {

        private static final String TAG = "CreateInvoiceRunnable";

        private final double amount;
        private final String description;

        public CreateInvoiceRunnable(double amount, String description) {
            this.amount = amount;
            this.description = description;
        }

        @Override
        public CreateInvoiceResponse run(LightningBlockingStub blockingStub,
                                         LightningStub asyncStub) {
            Log.d(TAG, "Start");
            CreateInvoiceRequest request = CreateInvoiceRequest.newBuilder()
                    .setAmountBits(this.amount).setDescription(description).build();
            return blockingStub.createInvoice(request);
        }
    }

    public static class DecodeInvoiceRunnable implements LightningGrpcRunnable {

        private static final String TAG = "DecodeInvoiceRunnable";

        private final String paymentRequest;

        public DecodeInvoiceRunnable(String paymentRequest) {
            this.paymentRequest = paymentRequest;
        }

        @Override
        public DecodeInvoiceResponse run(LightningBlockingStub blockingStub,
                                         LightningStub asyncStub) {
            Log.d(TAG, "Start");
            DecodeInvoiceRequest request = DecodeInvoiceRequest.newBuilder()
                    .setPaymentRequest(paymentRequest).build();
            return blockingStub.decodeInvoice(request);
        }
    }

    public static class UnlockNodeRunnable implements LightningGrpcRunnable {

        private static final String TAG = "UnlockNodeRunnable";

        private final String password;

        public UnlockNodeRunnable(String password) {
            this.password = password;
        }

        @Override
        public GeneratedMessageLite run(LightningBlockingStub blockingStub, LightningStub asyncStub) {
            Log.d(TAG, "Start");
            UnlockNodeRequest request = UnlockNodeRequest.newBuilder()
                    .setPassword(password).build();
            return blockingStub.unlockNode(request);
        }
    }

}
