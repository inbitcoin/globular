/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.data.model;

import android.util.Log;

import com.google.protobuf.GeneratedMessageLite;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import io.grpc.ManagedChannel;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.plugins.RxJavaPlugins;
import it.inbitcoin.globular.LockerGrpc;
import it.inbitcoin.globular.utils.ApiEndPoint;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.NetworkUtils;

public class LockerGrpcCall implements SingleOnSubscribe<GrpcResponse> {

    private static final String TAG = "LockerGrpcCall";

    private final ApiEndPoint apiEndPoint;
    private final LockerGrpcRunnable lockerGrpcRunnable;


    public LockerGrpcCall(ApiEndPoint apiEndPoint, LockerGrpcRunnable lockerGrpcRunnable) {
        this.apiEndPoint = apiEndPoint;
        this.lockerGrpcRunnable = lockerGrpcRunnable;
    }

    @Override
    public void subscribe(SingleEmitter<GrpcResponse> emitter) {

        RxJavaPlugins.setErrorHandler(e -> {
            if (e instanceof RuntimeException)
                return;
            if (e instanceof CertificateException)
                return;
            Log.w("Undeliverable exception received: ", e);
        });

        ManagedChannel mChannel;
        try {
            mChannel = NetworkUtils.buildChannel(apiEndPoint);
        } catch (CertificateException e) {
            emitter.onError(e);
            return;
        }
        if (mChannel == null) {
            emitter.onError(new RuntimeException("Error building gRPC channel"));
            return;
        }
        Log.d(TAG, "gRPC channel created");
        GrpcResponse grpcResponse;
        GeneratedMessageLite response;
        try {

            LockerGrpc.LockerBlockingStub U_blockingStub = LockerGrpc
                    .newBlockingStub(mChannel)
                    .withDeadlineAfter(AppConstants.grpcBlockingCallTimeout, TimeUnit.SECONDS);
            LockerGrpc.LockerStub U_asyncStub = LockerGrpc.newStub(mChannel)
                    .withDeadlineAfter(AppConstants.grpcAsyncCallTimeout, TimeUnit.SECONDS);

            response = lockerGrpcRunnable.run(U_blockingStub, U_asyncStub);

            Log.d(TAG, "gRPC response received");
            grpcResponse = new GrpcResponse(response, null);
            emitter.onSuccess(grpcResponse);
        } catch (StatusRuntimeException e) {
            Log.d(TAG, "gRPC error received " + e);
            Status.Code errorCode = e.getStatus().getCode();
            if (errorCode.equals(Status.Code.UNAVAILABLE)) {
                String description = e.getStatus().getDescription();
                if (description != null && description.equals("End of stream or IOException"))
                    emitter.onError(new CertificateException("gRPC certificate"));
                else
                    emitter.onError(new RuntimeException("server unavailable"));
            } else if (errorCode.equals(Status.Code.UNAUTHENTICATED))
                emitter.onError(new RuntimeException("unauthenticated"));
            else if (errorCode.equals(Status.Code.DEADLINE_EXCEEDED))
                emitter.onError(new RuntimeException("deadline exceeded"));
            else {
                grpcResponse = new GrpcResponse(null, e);
                emitter.onSuccess(grpcResponse);
            }
        }
        Log.d(TAG, "gRPC channel shutdown");
        try {
            mChannel.shutdown().awaitTermination(
                    AppConstants.grpcChannelCloseTimeout, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            mChannel.shutdownNow();
        }
    }

}
