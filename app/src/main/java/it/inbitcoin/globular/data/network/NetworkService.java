/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.data.network;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;

import dagger.android.DaggerService;
import io.grpc.Status;
import io.reactivex.Completable;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.BooleanSupplier;
import io.reactivex.schedulers.Schedulers;
import it.inbitcoin.globular.GetInfoResponse;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.AppRepository;
import it.inbitcoin.globular.data.model.GrpcResponse;
import it.inbitcoin.globular.data.prefs.PreferencesHelper;
import it.inbitcoin.globular.utils.ApiEndPoint;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.HandleNotifications;
import it.inbitcoin.globular.utils.NetworkUtils;

import static android.net.wifi.WifiManager.NETWORK_STATE_CHANGED_ACTION;
import static android.net.wifi.WifiManager.WIFI_STATE_CHANGED_ACTION;
import static android.os.Process.THREAD_PRIORITY_BACKGROUND;
import static it.inbitcoin.globular.Globular.IS_APP_IN_FOREGROUND;
import static it.inbitcoin.globular.utils.HandleNotifications.ONGOING_NOTIFICATION_ID;

@Singleton
public class NetworkService extends DaggerService {

    private static final String TAG = "NetworkService";

    @Inject
    ApiEndPoint mApiEndPoint;

    @Inject
    PreferencesHelper mPreferencesHelper;

    @Inject
    AppRepository appRepo;

    private BroadcastReceiver receiver;

    private Completable foregroundCheck;
    Disposable foregroundDisposable;
    private Completable backgroundCheck;
    public Disposable backgroundDisposable;

    BooleanSupplier appInBackground = () -> !IS_APP_IN_FOREGROUND;
    BooleanSupplier appInForeground = () -> IS_APP_IN_FOREGROUND;

    private final BroadcastReceiver dismissedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "notification dismissed");
            String action = intent.getAction();

            if (action == null || !action.equals(AppConstants.notificationDelete))
                return;

            LAST_NOTIFICATION_TIMESTAMP = System.currentTimeMillis();
        }
    };

    private Context mContext;

    private boolean LAST_WAS_OK = true;
    public static final int STATE_INVALID = 0;
    private int LAST_STATE = STATE_INVALID;
    private boolean CHECKS_RUNNING = false;
    private long LAST_OK_TIMESTAMP;
    private long LAST_NOTIFICATION_TIMESTAMP;

    private NotificationManager notificationManager;

    private final ArrayList<Messenger> mClients = new ArrayList<>();
    final int mValue = 0;

    Messenger mMessenger;

    public static final int FOREGROUND = 0;
    public static final int BACKGROUND = 1;

    public static final int MSG_REGISTER_CLIENT = 1;
    public static final int MSG_UNREGISTER_CLIENT = 2;
    public static final int MSG_LAST_STATE = 3;
    public static final int MSG_DISPOSE_BACKGROUND = 4;

    public static final int CONNECTION_OK = 5;

    public static final int HOST_ERROR = 8;
    public static final int CERTIFICATE_ERROR = 9;
    public static final int INTERNET_ERROR = 10;
    public static final int MACAROON_ERROR = 11;
    public static final int LOCKED_ERROR = 12;


    @Inject
    public NetworkService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();

        receiver = new MyBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(WIFI_STATE_CHANGED_ACTION);
        registerReceiver(receiver, filter);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppConstants.notificationDelete);
        registerReceiver(dismissedReceiver, intentFilter);

        HandlerThread thread = new HandlerThread(
                AppConstants.serviceHandler, THREAD_PRIORITY_BACKGROUND);
        thread.start();

        Looper serviceLooper = thread.getLooper();
        ServiceIncomingHandler serviceHandler = new ServiceIncomingHandler(serviceLooper);

        mMessenger = new Messenger(serviceHandler);

        mApiEndPoint.addApiEndPointListener(new ApiEndPoint.ApiEndPointListener() {
            @Override
            public void OnApiEndPointReady() {
                Log.d(TAG, "ApiEndPoint ready");
                if (!CHECKS_RUNNING)
                    repeatedConnectionCheck(FOREGROUND);
            }

            @Override
            public void OnApiEndPointChanged() {
                Log.d(TAG, "ApiEndPoint changed");
                boolean disposed = false;
                if (foregroundDisposable != null && !foregroundDisposable.isDisposed()) {
                    foregroundDisposable.dispose();
                    disposed = true;
                }
                LAST_STATE = STATE_INVALID;
                LAST_OK_TIMESTAMP = 0;
                LAST_NOTIFICATION_TIMESTAMP = System.currentTimeMillis();
                if (disposed)
                    repeatedConnectionCheck(FOREGROUND);
            }
        });

        Log.d(TAG, "service created " + this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(1);
        unregisterReceiver(receiver);

        unregisterReceiver(dismissedReceiver);
    }

    private final class ServiceIncomingHandler extends Handler {

        ServiceIncomingHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    Log.d(TAG, "registered new client");
                    mClients.add(msg.replyTo);
                    if (mApiEndPoint.isReady() && !CHECKS_RUNNING)
                        repeatedConnectionCheck(FOREGROUND);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_LAST_STATE:
                    sendToClients(MSG_LAST_STATE, LAST_STATE, null);
                    break;
                case MSG_DISPOSE_BACKGROUND:
                    if (backgroundDisposable != null && !backgroundDisposable.isDisposed())
                        backgroundDisposable.dispose();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }

    }

    private void sendToClients(int what, int arg2, Object obj) {
        Log.d(TAG, "sending msg to " + mClients.size() + " service clients");
        for (int i = mClients.size() - 1; i >= 0; i--) {
            try {
                mClients.get(i).send(Message.obtain(null, what, mValue, arg2, obj));
            } catch (RemoteException ex) {
                mClients.remove(i);
            } catch (NullPointerException e) {
                Log.d(TAG, "null client");
            }
        }
    }

    private void showNotification(String message) {
        notificationManager = (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);

        if (notificationManager.getActiveNotifications().length > 0)
            return;
        Log.d(TAG, "showing notification");

        LAST_NOTIFICATION_TIMESTAMP = System.currentTimeMillis();

        String subText = "";
        if (LAST_OK_TIMESTAMP > 0) {
            String lastOkTimestamp = AppUtils.getIsoDate(LAST_OK_TIMESTAMP / 1000);
            subText = (String.format(getString(R.string.last_ok_subtext), lastOkTimestamp));
        }

        Notification notification;
        if (AppUtils.isPreAndroidO()) {
            notification = HandleNotifications.PreO.createNotification(mContext, message, subText);
            notificationManager.notify(ONGOING_NOTIFICATION_ID, notification);
        } else {
            notification = HandleNotifications.O.createNotification(mContext, message, subText);
            notificationManager.notify(ONGOING_NOTIFICATION_ID, notification);
        }
    }

    private void hideNotification() {
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    private void sendError(int checkType, int errorId) {
        Log.d(TAG, "connection not ok");
        LAST_WAS_OK = false;
        if (checkType == FOREGROUND) {
            if (LAST_STATE == errorId)
                return;
            LAST_STATE = errorId;
            sendToClients(MSG_LAST_STATE, errorId, null);
        } else {
            LAST_STATE = errorId;
            long now = System.currentTimeMillis();
            Log.d(TAG, "last ok " + LAST_OK_TIMESTAMP + " last notification " + LAST_NOTIFICATION_TIMESTAMP);
            long interval = TimeUnit.MINUTES.toMillis(mPreferencesHelper.getNetServiceInterval());
            if (now - LAST_OK_TIMESTAMP >= interval && now - LAST_NOTIFICATION_TIMESTAMP >= interval)
                showNotification(AppUtils.getErrorString(mContext, errorId));
        }
    }

    private void sendOk() {
        LAST_STATE = CONNECTION_OK;
        if (!LAST_WAS_OK || LAST_OK_TIMESTAMP == 0) {
            sendToClients(MSG_LAST_STATE, CONNECTION_OK, null);
            hideNotification();
        }
        LAST_WAS_OK = true;
        LAST_OK_TIMESTAMP = System.currentTimeMillis();
        Log.d(TAG, "setting last ok " + LAST_OK_TIMESTAMP);
    }

    private void repeatedConnectionCheck(int checkType) {
        CHECKS_RUNNING = true;
        if (checkType == FOREGROUND) {
            foregroundCheck = Completable.create(emitter -> {
                Log.d(TAG, "repeating foreground check");
                try {
                    checkConnection(FOREGROUND);
                } finally {
                    try {
                        Thread.sleep(AppConstants.serviceForegroundInterval);
                    } catch (InterruptedException ignored) {}
                    emitter.onComplete();
                }
            });
            foregroundDisposable = foregroundCheck.observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io()).onTerminateDetach()
                    .repeatUntil(appInBackground)
                    .doOnComplete(() -> {
                        Log.d(TAG, "completed foreground");
                        if (mPreferencesHelper.getNetServiceEnable())
                            repeatedConnectionCheck(BACKGROUND);
                        else
                            CHECKS_RUNNING = false;
                    }).subscribe();
        }

        if (checkType == BACKGROUND) {
            LAST_NOTIFICATION_TIMESTAMP = System.currentTimeMillis();
            backgroundCheck = Completable.create(emitter -> {
                Log.d(TAG, "repeating background check");
                try {
                    checkConnection(BACKGROUND);
                } finally {
                    long checkInterval = mPreferencesHelper.getNetServiceInterval();
                    try {
                        Thread.sleep(TimeUnit.MINUTES.toMillis(
                                checkInterval / AppConstants.serviceChecksInInterval));
                    } catch (InterruptedException ignored) {}
                    emitter.onComplete();
                }
            });
            Action switchCheck = () -> {
                Log.d(TAG, "completed background check");
                repeatedConnectionCheck(FOREGROUND);
            };
            backgroundDisposable = backgroundCheck.observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .repeatUntil(appInForeground)
                    .doOnDispose(switchCheck).doOnComplete(switchCheck)
                    .onTerminateDetach()
                    .subscribe();
        }

    }

    public void checkConnection(int checkType) {
        new Thread(new ConnectionCheck(mApiEndPoint, checkType)).start();
    }

    class ConnectionCheck implements Runnable {

        final String host;
        final int port;
        final String TLSCertificate;
        final boolean isSecureEnabled;
        final int checkType;

        ConnectionCheck(ApiEndPoint apiEndPoint, int checkType) {
            this.host = apiEndPoint.getHost();
            this.port = apiEndPoint.getPort();
            this.TLSCertificate = apiEndPoint.getTLSCertificate();
            this.isSecureEnabled = apiEndPoint.isSecureEnabled();
            this.checkType = checkType;
        }

        private boolean haveNetworkConnection() {
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED))
                    return true;
            }
            return false;
        }

        @Override
        public void run() {
            String spec = "http://" + host + ":" + port;
            if (isSecureEnabled)
                spec = "https://" + host + ":" + port;
            Log.d(TAG, "checking connection to: " + spec);

            boolean ok = false;
            int message = HOST_ERROR;
            int CONNECTION_TRIES = AppConstants.serviceConnectionTries;

            if (mApiEndPoint.getHost().isEmpty()) {
                sendError(checkType, HOST_ERROR);
                return;
            }

            for (int i = 0; i < CONNECTION_TRIES; i++) {

                if (!haveNetworkConnection()) {
                    message = INTERNET_ERROR;
                } else {
                    URLConnection urlConnection = null;
                    try {
                        URL url = new URL(spec);
                        urlConnection = url.openConnection();
                        if (isSecureEnabled) {
                            ((HttpsURLConnection) urlConnection).setSSLSocketFactory(
                                    NetworkUtils.getSslSocketFactory(TLSCertificate));
                        }
                        urlConnection.setConnectTimeout(AppConstants.serviceCheckTimeoot);
                        urlConnection.connect();
                        Log.d(TAG, "connection ok");
                        ok = true;
                        break;
                    } catch (UnknownHostException e) {
                        Log.d(TAG, "UnknownHostException");
                    } catch (ConnectException e) {
                        Log.d(TAG, "ConnectException");
                    } catch (SSLHandshakeException e) {
                        Log.d(TAG, "SSLHandshakeException");
                        message = CERTIFICATE_ERROR;
                    } catch (SSLPeerUnverifiedException e) {
                        Log.d(TAG, "SSLPeerUnverifiedException");
                    } catch (MalformedURLException e) {
                        Log.d(TAG, "MalformedURLException");
                        // MalformedURLException is included in IOException
                    } catch (SocketTimeoutException e) {
                        Log.d(TAG, "SocketTimeoutException");
                    } catch (CertificateException e) {
                        Log.d(TAG, "CertificateException");
                        message = CERTIFICATE_ERROR;
                    } catch (IOException e) {
                        Log.d(TAG, "IOException");
                        Log.d(TAG, e.toString());
                    } finally {
                        if (urlConnection != null) {
                            if (isSecureEnabled)
                                ((HttpsURLConnection) urlConnection).disconnect();
                            else
                                ((HttpURLConnection) urlConnection).disconnect();
                        }
                    }
                }

                try {
                    Thread.sleep(AppConstants.servicePauseTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (!ok) {
                sendError(checkType, message);
                return;
            }

            appRepo.getInfo().observeOn(Schedulers.io())
                    .subscribe(new SingleObserver<GrpcResponse>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            Log.d(TAG, "checking macaroon");
                        }

                        @Override
                        public void onSuccess(GrpcResponse grpcResponse) {
                            if (grpcResponse.getError() != null) {
                                Log.d(TAG, "macaroon error: " + grpcResponse.getErrorDescription());
                                if (grpcResponse.getErrorCode().equals(Status.Code.UNIMPLEMENTED))
                                    sendError(checkType, LOCKED_ERROR);
                                else
                                    sendError(checkType, MACAROON_ERROR);
                            } else {
                                Log.d(TAG, "macaroon success");
                                GetInfoResponse response = (GetInfoResponse) grpcResponse.getMessage();
                                Log.d(TAG, response.getAlias());
                                sendOk();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "gRPC error: " + e);
                            sendError(checkType, HOST_ERROR);
                        }
                    });
        }
    }

}
