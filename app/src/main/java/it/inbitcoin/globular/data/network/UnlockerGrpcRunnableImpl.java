/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.data.network;

import android.util.Log;

import it.inbitcoin.globular.UnlockLighterRequest;
import it.inbitcoin.globular.UnlockLighterResponse;
import it.inbitcoin.globular.UnlockerGrpc;
import it.inbitcoin.globular.data.model.UnlockerGrpcRunnable;

public class UnlockerGrpcRunnableImpl {

    public static class UnlockLighterRunnable implements UnlockerGrpcRunnable {

        private static final String TAG = "UnlockLighterRunnable";

        private String password;
        private boolean unlockNode;

        public UnlockLighterRunnable(String password, boolean unlockNode) {
            this.password = password;
            this.unlockNode = unlockNode;
        }

        @Override
        public UnlockLighterResponse run(UnlockerGrpc.UnlockerBlockingStub blockingStub,
                                         UnlockerGrpc.UnlockerStub asyncStub) {
            Log.d(TAG, "Start");
            UnlockLighterRequest request = UnlockLighterRequest.newBuilder()
                    .setPassword(password).setUnlockNode(unlockNode).build();
            return blockingStub.unlockLighter(request);
        }
    }

}
