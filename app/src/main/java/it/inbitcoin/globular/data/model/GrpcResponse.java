/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.data.model;

import com.google.protobuf.GeneratedMessageLite;

import io.grpc.Status;
import io.grpc.StatusRuntimeException;

public class GrpcResponse {

    /**
     * GeneratedMessageLite is the parent class of all grpc responses
     */
    private GeneratedMessageLite message;
    private Class<?> messageType;
    private final StatusRuntimeException error;

    public GrpcResponse(GeneratedMessageLite message, StatusRuntimeException error, Class<?> cls) {
        this.message = message;
        this.messageType = cls;
        this.error = error;
    }

    public GrpcResponse(GeneratedMessageLite message, StatusRuntimeException error) {
        this.message = message;
        this.error = error;
    }

    public GeneratedMessageLite getMessage() {
        return this.message;
    }

    public Class<?> getMessageType() {
        return this.messageType;
    }

    public StatusRuntimeException getError() {
        return this.error;
    }

    public String getErrorDescription() {
        if (this.error != null) return this.error.getStatus().getDescription();
        return null;
    }

    public Status.Code getErrorCode() {
        return this.error.getStatus().getCode();
    }

    public void setMessage(GeneratedMessageLite message) {
        this.message = message;
    }

    public void setMessageType(Class<?> messageType) {
        this.messageType = messageType;
    }


}
