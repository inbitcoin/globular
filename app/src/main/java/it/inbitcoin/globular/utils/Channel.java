/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

import android.os.Parcel;
import android.os.Parcelable;

public class Channel implements Parcelable {

    private String remotePubkey;
    private String shortChannelId;
    private String channelId;
    private String fundingTxid;
    private double capacity;
    private double localBalance;
    private double remoteBalance;
    private long toSelfDelay;
    private int channelState;
    private String alias;
    private String color;


    public Channel() {

    }

    protected Channel(Parcel in) {
        channelId = in.readString();
    }

    public static final Creator<Channel> CREATOR = new Creator<Channel>() {
        @Override
        public Channel createFromParcel(Parcel in) {
            return readFromParcel(in);
        }

        @Override
        public Channel[] newArray(int size) {
            return new Channel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(remotePubkey);
        dest.writeString(shortChannelId);
        dest.writeString(channelId);
        dest.writeString(fundingTxid);
        dest.writeDouble(capacity);
        dest.writeDouble(localBalance);
        dest.writeDouble(remoteBalance);
        dest.writeLong(toSelfDelay);
        dest.writeInt(channelState);
        dest.writeString(alias);
        dest.writeString(color);
    }

    private static Channel readFromParcel(Parcel in) {
        Channel channel = new Channel();
        channel.setRemotePubkey(in.readString());
        channel.setShortChannelId(in.readString());
        channel.setChannelId(in.readString());
        channel.setFundingTxid(in.readString());
        channel.setCapacity(in.readDouble());
        channel.setLocalBalance(in.readDouble());
        channel.setRemoteBalance(in.readDouble());
        channel.setToSelfDelay(in.readLong());
        channel.setChannelState(in.readInt());
        channel.setAlias(in.readString());
        channel.setColor(in.readString());
        return channel;
    }

    public String getRemotePubkey() {
        return remotePubkey;
    }

    public void setRemotePubkey(String remotePubkey) {
        this.remotePubkey = remotePubkey;
    }

    public String getShortChannelId() {
        return shortChannelId;
    }

    public void setShortChannelId(String shortChannelId) {
        this.shortChannelId = shortChannelId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getFundingTxid() {
        return fundingTxid;
    }

    public void setFundingTxid(String fundingTxid) {
        this.fundingTxid = fundingTxid;
    }

    public double getLocalBalance() {
        return localBalance;
    }

    public void setLocalBalance(double localBalance) {
        this.localBalance = localBalance;
    }

    public double getRemoteBalance() {
        return remoteBalance;
    }

    public void setRemoteBalance(double remoteBalance) {
        this.remoteBalance = remoteBalance;
    }

    public long getToSelfDelay() {
        return toSelfDelay;
    }

    public void setToSelfDelay(long toSelfDelay) {
        this.toSelfDelay = toSelfDelay;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public int getChannelState() {
        return channelState;
    }

    public void setChannelState(int channelState) {
        this.channelState = channelState;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
