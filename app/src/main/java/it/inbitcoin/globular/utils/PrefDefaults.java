/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

public final class PrefDefaults {

    public static final String accessToken = "";
    public static final String scryptParams = "";

    public static final int pinFailedAttempts = 0;
    public static final boolean hapticPin = true;
    public static final boolean scramblePin = true;
    public static final boolean netServiceEnable = true;
    public static final int netServiceBackgroundInterval = 10;

    public static final String lighterHost = "";
    public static final int lighterPort = 1708;
    public static final boolean lighterSecureConnection = true;
    public static final String lighterTLSCertificate = "";
    public static final String lighterMacaroon = "";
    public static final boolean lighterDemoMode = false;

}
