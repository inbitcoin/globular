/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

public class SCryptParams {

    private final byte[] salt;
    private final int costFactor;
    private final int blockSizeFactor;
    private final int parallelizationFactor;
    private final int outKeyLen;


    public SCryptParams(byte[] salt, int costFactor, int blockSizeFactor, int parallelizationFactor,
                 int outKeyLen) {
        this.salt = salt;
        this.costFactor = costFactor;
        this.blockSizeFactor = blockSizeFactor;
        this.parallelizationFactor = parallelizationFactor;
        this.outKeyLen = outKeyLen;
    }

    public byte[] getSalt() {
        return salt;
    }

    public int getCostFactor() {
        return costFactor;
    }

    public int getBlockSizeFactor() {
        return blockSizeFactor;
    }

    public int getParallelizationFactor() {
        return parallelizationFactor;
    }

    public int getOutKeyLen() {
        return outKeyLen;
    }

}
