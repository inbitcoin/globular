/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

import android.util.Log;

import com.google.gson.Gson;
import com.goterl.lazycode.lazysodium.exceptions.SodiumException;

import org.bouncycastle.crypto.generators.SCrypt;

import javax.inject.Singleton;


@Singleton
public class AccessFactory	{

    private static final String TAG = "AccessFactory";

    public static final int PIN_LENGTH = 6;

    private byte[] accessKey;

    private byte[] oldAccessKey;

    private static boolean isLoggedIn = false;


    public AccessFactory() {

    }

    public byte[] getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(byte[] accessKey) {
        this.accessKey = accessKey;
    }

    public void resetAccessKey() {
        accessKey = null;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setIsLoggedIn(boolean logged) {
        isLoggedIn = logged;
    }

    public byte[] getAccessKey(String pin, String scryptParamsJson) {
        SCryptParams scryptParams = new Gson().fromJson(scryptParamsJson, SCryptParams.class);
        return SCrypt.generate(pin.getBytes(), scryptParams.getSalt(),
                scryptParams.getCostFactor(), scryptParams.getBlockSizeFactor(),
                scryptParams.getParallelizationFactor(), scryptParams.getOutKeyLen());
    }

    public boolean validateToken(String cryptedAccessToken, byte[] accessKey) {
        String clearAccessToken;
        try {
            clearAccessToken = CipherUtil.decrypt(cryptedAccessToken, accessKey);
            Log.d(TAG, "clearAccessToken " + clearAccessToken);
        } catch (SodiumException e) {
            Log.d(TAG, "decryption failed");
            return false;
        }
        return clearAccessToken.equals(AppConstants.accessToken);
    }

    public String createScryptParams() {
        byte[] salt = AppUtils.genSalt();
        SCryptParams scryptParams = new SCryptParams(salt, AppConstants.costFactor,
                AppConstants.blockSizeFactor, AppConstants.parallelizationFactor,
                AppConstants.accessKeyLen);
        Gson gson = new Gson();
        return gson.toJson(scryptParams);
    }

    public byte[] getOldAccessKey() {
        return oldAccessKey;
    }

    public void setOldAccessKey(byte[] oldAccessKey) {
        this.oldAccessKey = oldAccessKey;
    }

    public void resetOldAccessKey() {
        oldAccessKey = null;
    }

}
