/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

import android.util.Log;

import org.conscrypt.Conscrypt;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.Provider;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.security.auth.x500.X500Principal;

import io.grpc.CallCredentials;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.android.AndroidChannelBuilder;
import io.grpc.okhttp.OkHttpChannelBuilder;

public class NetworkUtils {

    private static final String TAG = "NetworkUtils";

    private static String providerName;

    static {
        Provider provider = Conscrypt.newProvider();
        providerName = provider.getName();
        Security.insertProviderAt(provider, 1);
        Log.d(TAG,"add security provider");
    }


    /**
     * Builds a gRPC channel using the endpoint defined in class 'ApiEndPoint'
     * @return a gRPC channel
     */
    public static ManagedChannel buildChannel(ApiEndPoint apiEndPoint)
            throws CertificateException {

        if (apiEndPoint.getHost().isEmpty() || apiEndPoint.getPort() == 0)
            return null;

        try {
            ManagedChannelBuilder<?> channelBuilder;

            if (apiEndPoint.isSecureEnabled()) {

                channelBuilder = ManagedChannelBuilder
                        .forAddress(apiEndPoint.getHost(), apiEndPoint.getPort());

                String TLSCertificate = apiEndPoint.getTLSCertificate();

                SSLSocketFactory factory;

                factory = getSslSocketFactory(TLSCertificate);
                ((OkHttpChannelBuilder) channelBuilder).sslSocketFactory(factory);
            } else {
                channelBuilder = ManagedChannelBuilder
                        .forAddress(apiEndPoint.getHost(), apiEndPoint.getPort())
                        .usePlaintext();
            }
            channelBuilder.idleTimeout(AppConstants.grpcChannelTimeout, TimeUnit.SECONDS);
            return AndroidChannelBuilder.fromBuilder(channelBuilder).build();
        } catch (IllegalArgumentException ignored) {}
        return null;
    }

    public static SSLSocketFactory getSslSocketFactory(String TLSCertificate)
            throws CertificateException {
        // Create an SSLContext that uses our TrustManager
        X509Certificate cert = parseCertificate(TLSCertificate);

        SSLContext context;
        TrustManager[] tmf;
        try {
            context = SSLContext.getInstance("TLSv1.2", providerName);
            tmf = createTrustManagers(cert);
            context.init(null, tmf, null);
        } catch (Exception e) {
            return null;
        }
        return context.getSocketFactory();
    }

    private static X509Certificate parseCertificate(String TLSCertificate)
            throws CertificateException {
        ByteArrayInputStream certificateStream = new ByteArrayInputStream(
                TLSCertificate.getBytes());

        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509Certificate x509Cert = (X509Certificate) cf.generateCertificate(certificateStream);
        try {
            certificateStream.close();
        } catch (IOException ignored) {}
        return x509Cert;
    }

    /**
     * Create a TrustManager that trusts the CAs in our KeyStore.
     * Set up trust manager factory to use our key store.
     */
    private static TrustManager[] createTrustManagers(X509Certificate cert) throws Exception {
        // Create a KeyStore containing our trusted CAs
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", cert);

        X500Principal principal = cert.getSubjectX500Principal();
        keyStore.setCertificateEntry(principal.getName("RFC2253"), cert);

        // Create a TrustManager that trusts the CAs in our KeyStore
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

        return tmf.getTrustManagers();
    }

    public static class MacaroonCallCredential extends CallCredentials {
        private final String macaroon;

        public MacaroonCallCredential(String macaroon) {
            this.macaroon = macaroon;
        }

        @Override
        public void applyRequestMetadata(RequestInfo requestInfo, Executor appExecutor,
                                         MetadataApplier applier) {
            appExecutor.execute(() -> {
                try {
                    Metadata headers = new Metadata();
                    Metadata.Key < String > macaroonKey = Metadata.Key.of(
                            AppConstants.macaroonKey, Metadata.ASCII_STRING_MARSHALLER);
                    headers.put(macaroonKey, macaroon);
                    applier.apply(headers);
                } catch (Throwable e) {
                    applier.fail(Status.UNAUTHENTICATED.withCause(e));
                }
            });
        }

        public void thisUsesUnstableApi() {}

    }

}
