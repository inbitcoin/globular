/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import it.inbitcoin.globular.data.network.NetworkService;

public class NetworkServiceConnection implements ServiceConnection {

    public Messenger mMessengerService = null;
    public boolean mIsBound = false;

    public Messenger mMessenger;


    public NetworkServiceConnection() {

    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mMessengerService = new Messenger(service);
        try {
            Message msg = Message.obtain(null, NetworkService.MSG_REGISTER_CLIENT);
            msg.replyTo = mMessenger;
            mMessengerService.send(msg);
        } catch (RemoteException ignored) {}
        AppUtils.sendMessageToService(mMessenger, mMessengerService, NetworkService.MSG_LAST_STATE);
        AppUtils.sendMessageToService(mMessenger, mMessengerService,
                NetworkService.MSG_DISPOSE_BACKGROUND);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mIsBound = false;
        mMessengerService = null;
    }

}
