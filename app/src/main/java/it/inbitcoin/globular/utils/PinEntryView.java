/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

import android.content.Context;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import it.inbitcoin.globular.R;

import static android.content.Context.VIBRATOR_SERVICE;


/**
 * Re-Usable custom KeypadView for pin entry
 */
public class PinEntryView extends FrameLayout implements View.OnClickListener {

    private static final String TAG = "PinEntryView";

    public enum KeyClearTypes {
        CLEAR_ALL,
        CLEAR
    }

    private Button ta = null;
    private Button tb = null;
    private Button tc = null;
    private Button td = null;
    private Button te = null;
    private Button tf = null;
    private Button tg = null;
    private Button th = null;
    private Button ti = null;
    private Button tj = null;
    private ImageButton tsend = null;
    private ImageButton tback = null;
    private ScrambledPin keypad = null;
    private int pinLen = 0;
    private boolean scramble = false;
    private pinEntryListener entryListener = null;
    private pinClearListener clearListener = null;
    private Vibrator vibrator;
    private final boolean enableHaptic = true;

    public PinEntryView(Context context) {
        super(context);
        initView();
    }

    public PinEntryView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public PinEntryView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    public void setEntryListener(pinEntryListener entryListener) {
        this.entryListener = entryListener;
    }

    public void setClearListener(pinClearListener clearListener) {
        this.clearListener = clearListener;
    }

    public void setScramble(boolean scramble) {
        this.scramble = scramble;
        setButtonLabels();
    }

    private void initView() {
        vibrator = (Vibrator) getContext().getSystemService(VIBRATOR_SERVICE);
        View view = inflate(getContext(), R.layout.view_keypad, null);
        ta = view.findViewById(R.id.ta);
        tb = view.findViewById(R.id.tb);
        tc = view.findViewById(R.id.tc);
        td = view.findViewById(R.id.td);
        te = view.findViewById(R.id.te);
        tf = view.findViewById(R.id.tf);
        tg = view.findViewById(R.id.tg);
        th = view.findViewById(R.id.th);
        ti = view.findViewById(R.id.ti);
        tj = view.findViewById(R.id.tj);
        tsend = view.findViewById(R.id.tsend);
        tback = view.findViewById(R.id.tback);
        tback.setOnClickListener(view1 -> {
            hapticFeedBack();
            pinLen--;

            if (clearListener != null) {
                clearListener.onPinClear(KeyClearTypes.CLEAR);
            }
        });
        tback.setOnLongClickListener(view12 -> {
            hapticFeedBack();
            pinLen = 0;
            if (clearListener != null) {
                clearListener.onPinClear(KeyClearTypes.CLEAR_ALL);
            }
            return false;
        });
        setButtonLabels();
        addView(view);
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "pinLen " + pinLen);
        if (pinLen < AccessFactory.PIN_LENGTH) {
            if (this.entryListener != null) {
                if (((Button) view).getText().toString().length() < AccessFactory.PIN_LENGTH) {
                    hapticFeedBack();
                    entryListener.onPinEntered(((Button) view).getText().toString(), view);
                    pinLen++;
                }
            }
        }
    }

    private void hapticFeedBack() {
        if (this.enableHaptic) {
            vibrator.vibrate(44);
        }
    }

    private void setButtonLabels() {
        keypad = new ScrambledPin();
        ta.setText(this.scramble ? Integer.toString(keypad.getMatrix().get(0).getValue()) : "1");
        ta.setOnClickListener(this);
        tb.setText(this.scramble ? Integer.toString(keypad.getMatrix().get(1).getValue()) : "2");
        tb.setOnClickListener(this);
        tc.setText(this.scramble ? Integer.toString(keypad.getMatrix().get(2).getValue()) : "3");
        tc.setOnClickListener(this);
        td.setText(this.scramble ? Integer.toString(keypad.getMatrix().get(3).getValue()) : "4");
        td.setOnClickListener(this);
        te.setText(this.scramble ? Integer.toString(keypad.getMatrix().get(4).getValue()) : "5");
        te.setOnClickListener(this);
        tf.setText(this.scramble ? Integer.toString(keypad.getMatrix().get(5).getValue()) : "6");
        tf.setOnClickListener(this);
        tg.setText(this.scramble ? Integer.toString(keypad.getMatrix().get(6).getValue()) : "7");
        tg.setOnClickListener(this);
        th.setText(this.scramble ? Integer.toString(keypad.getMatrix().get(7).getValue()) : "8");
        th.setOnClickListener(this);
        ti.setText(this.scramble ? Integer.toString(keypad.getMatrix().get(8).getValue()) : "9");
        ti.setOnClickListener(this);
        tj.setText(this.scramble ? Integer.toString(keypad.getMatrix().get(9).getValue()) : "0");
        tj.setOnClickListener(this);
    }

    public interface pinEntryListener {
        void onPinEntered(String key, View view);
    }

    public interface pinClearListener {
        void onPinClear(KeyClearTypes clearType);
    }

    public void resetPin() {
        Log.d(TAG, "pin reset");
        this.pinLen = -1;
    }

}
