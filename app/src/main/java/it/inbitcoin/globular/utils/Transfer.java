/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.List;

import it.inbitcoin.globular.R;

public class Transfer implements Comparable<Transfer>, Parcelable {

    private int id;
    private int transferType;
    private int status;
    private String paymentRequest;
    private String description;
    private double amount;
    private long timestamp;
    private long expiryTime;
    private long feeBaseMsat;
    private List<String> destAddresses;
    private int numConfirmations;
    private String blockHash;
    private int blockHeight;
    private long feeSat;

    public static final int INVOICES = 0;
    public static final int PAYMENTS = 1;
    public static final int TRANSACTIONS = 2;

    public static final int PAID = 0;
    private static final int PENDING = 1;
    private static final int EXPIRED = 2;


    public Transfer() {

    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Transfer createFromParcel(Parcel in) {
            return readFromParcel(in);
        }

        public Transfer[] newArray(int size) {
            return new Transfer[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(transferType);
        dest.writeInt(status);
        dest.writeString(paymentRequest);
        dest.writeString(description);
        dest.writeDouble(amount);
        dest.writeLong(timestamp);
        dest.writeLong(expiryTime);
        dest.writeLong(feeBaseMsat);
        dest.writeStringList(destAddresses);
        dest.writeInt(numConfirmations);
        dest.writeString(blockHash);
        dest.writeInt(blockHeight);
        dest.writeLong(feeSat);
    }

    private static Transfer readFromParcel(Parcel in) {
        Transfer transfer = new Transfer();
        transfer.setId(in.readInt());
        transfer.setTransferType(in.readInt());
        transfer.setStatus(in.readInt());
        transfer.setPaymentRequest(in.readString());
        transfer.setDescription(in.readString());
        transfer.setAmount(in.readDouble());
        transfer.setTimestamp(in.readLong());
        transfer.setExpiryTime(in.readLong());
        transfer.setFeeBaseMsat(in.readLong());
        List<String> newList = in.createStringArrayList();
        transfer.setDestAddresses(newList);
        transfer.setNumConfirmations(in.readInt());
        transfer.setBlockHash(in.readString());
        transfer.setBlockHeight(in.readInt());
        transfer.setFeeSat(in.readLong());
        return transfer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTransferType() {
        return transferType;
    }

    public void setTransferType(int transferType) {
        this.transferType = transferType;
    }

    public int getStatus() {
        return status;
    }

    public String getStatusString(Context context) {
        switch (this.getStatus()){
            case PAID:
                return context.getResources().getString(R.string.status_paid);
            case PENDING:
                return context.getResources().getString(R.string.status_pending);
            case EXPIRED:
                return context.getResources().getString(R.string.status_expired);
            default:
                return context.getResources().getString(R.string.none);
        }
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPaymentRequest() {
        return paymentRequest;
    }

    public void setPaymentRequest(String paymentRequest) {
        this.paymentRequest = paymentRequest;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(long expiryTime) {
        this.expiryTime = expiryTime;
    }

    public long getFeeBaseMsat() {
        return feeBaseMsat;
    }

    public void setFeeBaseMsat(long feeBaseMsat) {
        this.feeBaseMsat = feeBaseMsat;
    }

    public List<String> getDestAddresses() {
        return destAddresses;
    }

    public void setDestAddresses(List<String> destAddresses) {
        this.destAddresses = destAddresses;
    }

    public int getNumConfirmations() {
        return numConfirmations;
    }

    public void setNumConfirmations(int numConfirmations) {
        this.numConfirmations = numConfirmations;
    }

    public String getBlockHash() {
        return blockHash;
    }

    public void setBlockHash(String blockHash) {
        this.blockHash = blockHash;
    }

    public int getBlockHeight() {
        return blockHeight;
    }

    public void setBlockHeight(int blockHeight) {
        this.blockHeight = blockHeight;
    }

    public long getFeeSat() {
        return feeSat;
    }

    public void setFeeSat(long feeSat) {
        this.feeSat = feeSat;
    }

    @NonNull
    @Override
    public String toString() {
        return "id: " + id + ", transferType: " + transferType;
    }

    @Override
    public int compareTo(Transfer o) {
        return Long.compare(o.timestamp, timestamp);
    }

}
