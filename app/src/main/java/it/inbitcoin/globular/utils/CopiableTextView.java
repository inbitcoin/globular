/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;

import it.inbitcoin.globular.R;

public class CopiableTextView extends AppCompatTextView {

    private static final String TAG = "CopiableTextView";

    private Context mContext;


    public CopiableTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public CopiableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CopiableTextView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        this.setTextIsSelectable(true);
        this.setOnClickListener(view -> copyToClipboard(CopiableTextView.this.getText().toString()));
    }

    private void copyToClipboard(String text) {
        Log.d(TAG, "copied to clipboard " + text);
        AppUtils.copyToClipboard(mContext, text);
        String message = String.format(getResources().getString(R.string.clipboard_copy_message), text);
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

}