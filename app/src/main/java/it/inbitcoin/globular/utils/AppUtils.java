/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.util.Preconditions;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.network.NetworkService;
import it.inbitcoin.globular.data.prefs.PreferencesHelper;
import it.inbitcoin.globular.ui.login.LoginActivity;
import it.inbitcoin.globular.ui.scan.ScanActivity;

import static android.content.Context.CLIPBOARD_SERVICE;
import static androidx.core.content.FileProvider.getUriForFile;

public class AppUtils {

    private static final String TAG = "AppUtils";


    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment, int frameId,
                                             boolean addToBackStack,
                                             boolean replace) {
        Preconditions.checkNotNull(fragmentManager);
        Preconditions.checkNotNull(fragment);

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (replace)
            transaction.replace(frameId, fragment);
        else
            transaction.add(frameId, fragment);
        if (addToBackStack)
            transaction.addToBackStack(null);
        transaction.commit();
    }

    public static String getIsoDate(long timestamp) {
        SimpleDateFormat formatter = new SimpleDateFormat(
                AppConstants.dateFormat, Locale.getDefault());
        return formatter.format(new Date(timestamp * 1000));
    }

    public static class EditorDoneListener implements TextView.OnEditorActionListener {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard(v);
                return true;
            }
            return false;
        }
    }

    public static void hideKeyboard(View v) {
        try {
            InputMethodManager imm = (InputMethodManager) v.getContext()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        } catch (Exception ignored) {}
    }


    public static boolean isTimedOut(AccessFactory accessFactory, TimeoutUtil timeoutUtil,
                                     Context context) {
        if (timeoutUtil.isTimedOut()) {
            Log.d(TAG, "timedOut");
            accessFactory.resetAccessKey();
            accessFactory.setIsLoggedIn(false);
            Intent intent = new Intent(context, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(intent);
            return true;
        } else {
            Log.d(TAG, "not timedOut");
            timeoutUtil.updatePin();
            return false;
        }
    }

    private static String encodeHexString(byte[] byteArray) {
        StringBuilder hexStringBuffer = new StringBuilder();
        for (byte b : byteArray) {
            hexStringBuffer.append(byteToHex(b));
        }
        return hexStringBuffer.toString();
    }

    private static String byteToHex(byte num) {
        char[] hexDigits = new char[2];
        hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
        hexDigits[1] = Character.forDigit((num & 0xF), 16);
        return new String(hexDigits);
    }

    public static void saveImage(Context context, Bitmap bitmap, String imageName) {
        File imageDir = new File(context.getCacheDir(), AppConstants.imagesDir);
        File newFile = new File(imageDir, imageName);
        Log.d(TAG, "file path " + newFile.getAbsolutePath());
        FileOutputStream outputStream;
        try {
            boolean createdDir = newFile.getParentFile().mkdirs();
            Log.d(TAG, "created image dir " + createdDir);
            boolean success = newFile.createNewFile();
            if (success) {
                outputStream = new FileOutputStream(newFile, false);
                bitmap.compress(Bitmap.CompressFormat.PNG, AppConstants.imageQuality, outputStream);
                outputStream.flush();
                outputStream.close();
                Log.d(TAG, "image saved");
            }
        } catch (Exception e) {
            Log.d(TAG, "error saving image " + e);
        }
    }

    public static Intent getShareIntent(Context context, String sharedText, String imageName) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);

        File imagePath = new File(context.getCacheDir(), AppConstants.imagesDir);
        File newFile = new File(imagePath, imageName);
        Uri contentUri = getUriForFile(
                context, context.getPackageName() + AppConstants.fileAuthority, newFile);

        sharingIntent.setType("*/*");
        String[] mimeTypes = {"image/png", "text/plain"};
        sharingIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

        sharingIntent.putExtra(
                android.content.Intent.EXTRA_TEXT, sharedText);
        sharingIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
        return sharingIntent;
    }

    static byte[] genSalt() {
        byte[] salt = new byte[AppConstants.nonceLen];
        new Random().nextBytes(salt);
        return salt;
    }

    public static String getAppVersion(Context context) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return "v".concat(packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException ignored) {}
        return "";
    }

    public static String parseMacaroon(String macaroon) {
        byte[] macaroonBytes = macaroon.getBytes(StandardCharsets.UTF_8);
        return encodeHexString(macaroonBytes);
    }

    static void sendMessageToService(Messenger messenger, Messenger messengerService,
                                     int message) {
        if (messengerService != null) {
            try {
                Message msg = Message.obtain(null, message);
                msg.replyTo = messenger;
                messengerService.send(msg);
            } catch (RemoteException ignored) {}
        }
    }

    public static boolean isPreAndroidO() {
        return Build.VERSION.SDK_INT <= Build.VERSION_CODES.N_MR1;
    }

    public static String getErrorString(Context context, int errorId) {
        switch (errorId) {
            case NetworkService.HOST_ERROR:
                return context.getString(R.string.connection_problems);
            case NetworkService.CERTIFICATE_ERROR:
                return context.getString(R.string.certificate_problems);
            case NetworkService.INTERNET_ERROR:
                return context.getString(R.string.internet_problems);
            case NetworkService.MACAROON_ERROR:
                return context.getString(R.string.macaroon_error);
            case NetworkService.LOCKED_ERROR:
                return context.getString(R.string.lighter_locked);
            default:
                return "";
        }
    }

    public static String formatNumber(String number, int decimalDigits) {
        NumberFormat numFormat = NumberFormat.getNumberInstance(Locale.getDefault());
        numFormat.setMinimumFractionDigits(decimalDigits);
        DecimalFormat decFormat = (DecimalFormat) numFormat;
        try {
            return decFormat.format(Double.parseDouble(number));
        } catch (NumberFormatException e) {
            return number;
        }
    }

    public static String formatRemotePubKeyShort(String remotePubkey) {
        return remotePubkey.substring(0,6) + "..." + remotePubkey.substring(57, 63);
    }

    private static String copyFromClipboard(Context context) {
        Log.d(TAG, "copied from clipboard");
        try {
            final android.content.ClipboardManager clipboardManager =
                    (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
            ClipData clipData = clipboardManager.getPrimaryClip();
            if (clipData != null)
                return clipData.getItemAt(0).getText().toString();
        } catch (NullPointerException e) {
            Toast.makeText(context, context.getString(R.string.empty_clipboard),
                    Toast.LENGTH_SHORT).show();
        }
        return "";
    }

    public static void copyToClipboard(Context context, String text) {
        final ClipboardManager clipboardManager = (ClipboardManager) context
                .getSystemService(Context.CLIPBOARD_SERVICE);
        clipboardManager.setPrimaryClip(ClipData.newPlainText("", text));
    }

    public static void cryptPref(AccessFactory accessFactory, PreferencesHelper preferencesHelper,
                                 String preferenceKey, String data) {
        String cryptedData = CipherUtil.cryptData(data, accessFactory);
        preferencesHelper.putStringKeyValue(preferenceKey, cryptedData);
    }

    public static void cancelRequest(Activity activity) {
        Log.d(TAG, "request cancelled");
        Toast.makeText(activity, activity.getString(R.string.cancelled), Toast.LENGTH_SHORT).show();
    }

    public static String getPreferenceKey(int requestCode) {
        String preferenceKey = "";
        switch (requestCode) {
            case ScanActivity.REQ_CERTIFICATE:
                return PreferencesHelper.LIGHTER_TLS_CERTIFICATE;
            case ScanActivity.REQ_MACAROON:
                return PreferencesHelper.LIGHTER_MACAROON;
        }
        return preferenceKey;
    }

    public static String getDataFromScan(int requestCode, int resultCode, Intent data,
                                         Activity activity) {
        String clearData = "";

        IntentResult result = IntentIntegrator.parseActivityResult(
                IntentIntegrator.REQUEST_CODE, resultCode, data);
        Log.d(TAG, "result " + result);
        if (result == null) {
            Log.d(TAG, "null result");
            return clearData;
        }

        if (result.getContents() == null) {
            if (resultCode == ScanActivity.COPY) {
                clearData = AppUtils.copyFromClipboard(activity);
            } else {
                AppUtils.cancelRequest(activity);
            }
        } else {
            clearData = result.getContents();
            if (requestCode == ScanActivity.REQ_CERTIFICATE) {
                clearData = clearData.replaceAll(" (?!CERTIFICATE)", "\n");
            }
            if (requestCode == ScanActivity.REQ_MACAROON) {
                clearData = clearData.replace("macaroon:", "");
            }
        }
        return clearData;
    }

    private static String EnforcedDecimal(String str, int maxDigitsBeforeDecimalPoint,
                                          int maxDigitsAfterDecimalPoint, double minValue,
                                          double maxValue){
        DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getInstance();
        DecimalFormatSymbols symbols = decimalFormat.getDecimalFormatSymbols();
        char decimalSeparator = symbols.getDecimalSeparator();

        StringBuilder stringBuilder = new StringBuilder();

        if (str.charAt(0) == decimalSeparator) str = "0" + str;

        int strLength = str.length();
        boolean after = false, foundNonzeroDigit = false;
        int i = 0, up = 0, decimal = 0;
        char t;
        while (i < strLength) {
            t = str.charAt(i);
            if (!foundNonzeroDigit && t == '0') {
                i++;
                continue;
            } else
                foundNonzeroDigit = true;

            if (t != decimalSeparator && !after) {
                up++;
                if (up > maxDigitsBeforeDecimalPoint) {
                    i++;
                    continue;
                }
            } else if (t == decimalSeparator) {
                after = true;
            } else {
                decimal++;
                if (decimal > maxDigitsAfterDecimalPoint) break;
            }

            stringBuilder.append(t);
            i++;
        }

        decimalFormat.setMinimumFractionDigits(maxDigitsAfterDecimalPoint);
        decimalFormat.setGroupingUsed(false);

        String finalString = stringBuilder.toString();
        if (finalString.isEmpty())
            finalString = "0";
        if (finalString.charAt(0) == decimalSeparator) finalString = "0" + finalString;

        double finalValue = Double.valueOf(finalString);
        if (finalValue < minValue) {
            finalString = decimalFormat.format(minValue);
        } else if (finalValue > maxValue) {
            finalString = decimalFormat.format(maxValue);
        }

        return finalString;
    }

    public static String addNewlineEveryNChars(String text, int n) {
        StringBuilder finalText = new StringBuilder();
        int textLength = text.length();
        for (int i = 0; i < textLength; i += n) {
            finalText.append(text.substring(i, Math.min(textLength, i + n))).append('\n');
        }
        return finalText.toString();
    }

    public static class DecimalInputWatcher implements TextWatcher {

        EditText editText;
        int maxDigitsBeforeDecimalPoint;
        int maxDigitsAfterDecimalPoint;
        double minValue;
        double maxValue;

        public DecimalInputWatcher(EditText editText, int maxDigitsBeforeDecimalPoint,
                                   int maxDigitsAfterDecimalPoint, double minValue,
                                   double maxValue) {
            this.editText = editText;
            this.maxDigitsBeforeDecimalPoint = maxDigitsBeforeDecimalPoint;
            this.maxDigitsAfterDecimalPoint = maxDigitsAfterDecimalPoint;
            this.minValue = minValue;
            this.maxValue = maxValue;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { }

        @Override
        public void afterTextChanged(Editable s) {
            String str = editText.getText().toString();
            if (str.isEmpty()) return;
            String str2 = EnforcedDecimal(
                    str, maxDigitsBeforeDecimalPoint, maxDigitsAfterDecimalPoint, minValue,
                    maxValue);
            if (!str2.equals(str)) {
                editText.setText(str2);
                int pos = editText.getText().length();
                editText.setSelection(pos);
            }
        }
    }

}
