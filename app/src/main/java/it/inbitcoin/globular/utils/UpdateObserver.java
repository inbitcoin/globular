/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

import android.view.View;
import android.widget.ProgressBar;

import io.reactivex.CompletableObserver;
import io.reactivex.disposables.Disposable;

public class UpdateObserver implements CompletableObserver {

    final long time;
    final int uxTime = 1600;

    ProgressBar progressBar;

    public UpdateObserver(ProgressBar progressBar) {
        this.time = System.currentTimeMillis();
        this.progressBar = progressBar;
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onComplete() {
        while (System.currentTimeMillis() - time < uxTime) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ignored) {}
        }
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onError(Throwable e) {
        progressBar.setVisibility(View.INVISIBLE);
    }
}