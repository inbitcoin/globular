/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ApiEndPoint {

    private String host = PrefDefaults.lighterHost;
    private int port = PrefDefaults.lighterPort;
    private String TLSCertificate = PrefDefaults.lighterTLSCertificate;
    private String macaroon = PrefDefaults.lighterMacaroon;
    private boolean secureEnabled = PrefDefaults.lighterSecureConnection;
    private boolean enableDemoMode = PrefDefaults.lighterDemoMode;

    private final List<ApiEndPointListener> listeners = new ArrayList<>();
    private boolean ready = false;
    private boolean changed = false;


    @Inject
    public ApiEndPoint() {
    }

    public String getHost() {
        if (enableDemoMode)
            return AppConstants.demoHost;
        return this.host;
    }

    public int getPort() {
        if (enableDemoMode)
            return AppConstants.demoPort;
        return this.port;
    }

    public String getTLSCertificate() {
        if (enableDemoMode)
            return AppConstants.demoCertificate;
        return this.TLSCertificate;
    }

    public String getMacaroon() {
        if (enableDemoMode)
            return AppConstants.demoMacaroon;
        return this.macaroon;
    }

    public boolean isSecureEnabled() {
        if (enableDemoMode)
            return AppConstants.demoSecure;
        return this.secureEnabled;
    }

    public boolean isReady() {
        return ready;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setTLSCertificate(String TLSCertificate) {
        this.TLSCertificate = TLSCertificate;
    }

    public void setMacaroon(String macaroon) {
        this.macaroon = macaroon;
    }

    public void setSecureEnabled(boolean secureEnabled) {
        this.secureEnabled = secureEnabled;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
        for (ApiEndPointListener listener : listeners) {
            listener.OnApiEndPointReady();
        }
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
        for (ApiEndPointListener listener : listeners)
            listener.OnApiEndPointChanged();
    }

    public void setLighterDemoMode(boolean enableDemoMode) {
        this.enableDemoMode = enableDemoMode;
    }

    public void addApiEndPointListener(ApiEndPointListener listener) {
        listeners.add(listener);
    }

    public interface ApiEndPointListener {
        void OnApiEndPointReady();
        void OnApiEndPointChanged();
    }

}
