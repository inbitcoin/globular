/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import it.inbitcoin.globular.BuildConfig;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.ui.login.LoginActivity;

public class HandleNotifications {

    private static final String TAG = "HandleNotifications";

    private static final String CHANNEL_ID = BuildConfig.APPLICATION_ID + AppConstants.channelId;

    private static final int SMALL_ICON = R.drawable.ic_logo_small;
    private static final int REQUEST_LAUNCH = 1;
    public static final int ONGOING_NOTIFICATION_ID = 7;


    private static PendingIntent getLaunchActivityPI(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        return PendingIntent.getActivity(context, REQUEST_LAUNCH, intent, 0);
    }

    private static PendingIntent getOnDismissedPI(Context context) {
        Intent intent = new Intent(AppConstants.notificationDelete);
        intent.setAction(AppConstants.notificationDelete);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

    @NonNull
    private static String getNotificationTitle(Context context) {
        return context.getString(R.string.notification_text_title);
    }

    @TargetApi(25)
    public static class PreO {

        public static Notification createNotification(Context context, String contentText,
                                                      String subText) {
            PendingIntent piLaunchActivity = getLaunchActivityPI(context);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setContentTitle(getNotificationTitle(context))
                    .setContentText(contentText)
                    .setTicker(contentText)
                    .setContentIntent(piLaunchActivity)
                    .setSmallIcon(SMALL_ICON)
                    .setAutoCancel(false)
                    .setDeleteIntent(getOnDismissedPI(context))
                    .setWhen(System.currentTimeMillis())
                    .setShowWhen(true);
            if (!subText.isEmpty())
                builder.setSubText(subText);
            return builder.build();
        }

    }

    @TargetApi(26)
    public static class O {

        public static Notification createNotification(Context context, String contentText,
                                                      String subText) {
            String channelId = createChannel(context);
            PendingIntent piLaunchActivity = getLaunchActivityPI(context);
            Notification.Builder builder = new Notification.Builder(context, channelId)
                    .setContentTitle(getNotificationTitle(context))
                    .setContentText(contentText)
                    .setTicker(contentText)
                    .setContentIntent(piLaunchActivity)
                    .setSmallIcon(SMALL_ICON)
                    .setAutoCancel(false)
                    .setDeleteIntent(getOnDismissedPI(context))
                    .setWhen(System.currentTimeMillis())
                    .setShowWhen(true);
            if (!subText.isEmpty())
                builder.setSubText(subText);
            return builder.build();
        }

        @NonNull
        private static String createChannel(Context context) {
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            CharSequence channelName = context.getString(R.string.channel_name);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel notificationChannel = new NotificationChannel(
                    CHANNEL_ID, channelName, importance);
            notificationChannel.setDescription(context.getString(R.string.channel_description));
            notificationManager.createNotificationChannel(notificationChannel);
            return CHANNEL_ID;
        }
    }

}