/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

public class AppConstants {

    // utility string keys
    public static final String requestKey = "requestActivity";
    public static final String viaMenu = "viaMenu";
    public static final String viaPinCreate = "viaPinCreate";
    public static final String transfer = "transfer";
    public static final String channel = "channel";
    static final String macaroonKey = "macaroon";
    public static final String unlockPassword = "unlockPassword";
    public static final String unlockNodeCb = "unlockNodeCheckbox";
    public static final String lockPassword = "lockPassword";
    public static final String unlockLighterDialog = "unlockLighterDialog";
    public static final String lockDialog = "lockDialog";
    public static final String unlockNodeDialog = "unlockNodeDialog";
    public static final String nodeUri = "nodeUri";
    public static final String paymentRequest = "paymentRequest";
    public static final String intervalDialog = "lastNetServiceInterval";
    public static final String lastServiceInterval = "lastNetServiceInterval";
    public static final String notificationDelete = "NOTIFICICATION_DELETE";
    public static final String closeDialog = "closeDialog";

    // bitcoin strings
    public static final String uriSchemeBitcoin = "bitcoin:";
    public static final String uriSchemeLightning = "lightning:";
    public static final String amount = "amount";
    public static final String amountParam = "?amount=";

    // images settings
    static final String imagesDir = "images";
    private static final String imageExtension = ".png";
    public static final String qrLastInvoice = "qr_last_invoice" + imageExtension;
    public static final String qrLastAddress = "qr_last_address" + imageExtension;
    public static final String qrNodeUri = "qr_node_uri" + imageExtension;
    public static final String qrInvoiceDetail = "qr_invoice_detail" + imageExtension;
    static final int imageQuality = 100;
    static final String fileAuthority = ".fileprovider";

    // format settings
    static final String dateFormat = "d MMM y HH:mm";

    // cipher settings
    private static final String appName = "globular";
    private static final String latestAccessVersion = "v0";
    public static final String accessToken = appName + "_" + latestAccessVersion;
    static final int costFactor = 16384;
    static final int blockSizeFactor = 8;
    static final int parallelizationFactor = 1;
    static final int accessKeyLen = 32;
    static final int nonceLen = 32;

    // grpc settings
    public static final int grpcBlockingCallTimeout = 7;
    public static final int grpcAsyncCallTimeout = 7;
    static final int grpcChannelTimeout = 4;
    public static final int grpcChannelCloseTimeout = 4;

    // service settings
    public static final int serviceConnectionTries = 3;
    public static final String serviceHandler = "NetworkServiceHandler";
    static final String channelId = ".connection_channel";
    public static final int serviceCheckTimeoot = 1000;
    public static final int servicePauseTime = 500;
    public static final int serviceForegroundInterval = 7000;
    public static final int serviceChecksInInterval = 4;

    // Lighter demo mode credentials
    static final String demoHost = "globulight.inbitcoin.it";
    static final int demoPort = 1708;
    static final String demoCertificate = "-----BEGIN CERTIFICATE-----\n" +
            "MIIDMDCCAhigAwIBAgIJAKUxLxmLs0oHMA0GCSqGSIb3DQEBCwUAMCIxIDAeBgNV\n" +
            "BAMMF2dsb2J1bGlnaHQuaW5iaXRjb2luLml0MB4XDTE5MDQwNTIwNTAzM1oXDTI5\n" +
            "MDQwMjIwNTAzM1owIjEgMB4GA1UEAwwXZ2xvYnVsaWdodC5pbmJpdGNvaW4uaXQw\n" +
            "ggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDgXtKGPiEtUPV7P0OHjG9a\n" +
            "uPFGHTeoqjovAPk1HehTCG+cGgPto+OMwUZTbMsum7iswsg2F92B8TOqzlyEwVvv\n" +
            "upEJmsRzjze15n2NNcLuDbCYUuapdeDDLuz3yzk4tfK6pPwVc7E7oLtwRS55p7SH\n" +
            "sNO5zH9wHmFvW8vy6kl6Fwa2B7Zzd/tKwTUFZFERy24YTLLxeooULx5/G/LZMLlH\n" +
            "YmkcdrKkbwp/LfviIv3GL7q6mudDXYZxUfZzFxhZqMoEkn6RfboxYAwTRJe8uvO9\n" +
            "1OKqDDVBCyjlD+vIXkt372PLW/z0Bjr65cKIdGVwcMRuXTG4mWtXQ9Ffl+DGZwAp\n" +
            "AgMBAAGjaTBnMGUGA1UdEQReMFyCF2dsb2J1bGlnaHQuaW5iaXRjb2luLml0ggls\n" +
            "b2NhbGhvc3SCB2xpZ2h0ZXKCFWxpZ2h0ZXJwcmlzX2xpZ2h0ZXJfMYcEfwAAAYcQ\n" +
            "AAAAAAAAAAAAAAAAAAAAATANBgkqhkiG9w0BAQsFAAOCAQEArHT5SEj1QXSOkS1W\n" +
            "Y9JaXNEPInVIXyVwC+ofvHAZmfpWZww+AbjyK5eOjDp2dkrDRUTHfKOrmVTCzgmI\n" +
            "vBM81jmovrT0Xn7oPRDLuhA4xFPbCrxkIHFzWV/ORsBScqB5jduUokvkUFnmzoS/\n" +
            "+/ok/R4B3vKGpGgPz9mJsN2inxc4tHlw8wu1mwAcgJnk99Nt5yG8WckawFSp9+FY\n" +
            "ScYDokibc82cLC8zypig1dt4PUqfHgIzTcfUT//w6oTJicdvtWy8eVK+Z/28g5VS\n" +
            "Ub3Rzj4p3udNYOC/XzTzxWIkccrqOKTBey7bNhTILxp+hJx4F7AOWAlWHbZXUdvU\n" +
            "MqV1JA==\n" +
            "-----END CERTIFICATE-----";
    static final String demoMacaroon = AppUtils.parseMacaroon("AgEHbGlnaHRlcgJOAwoQJvng9r2lQgjbq" +
            "d_fix_q_BIBMBo2CjFtdWx0aS1EVlc4cTZEcmhYYWYxSEd0TXByYUJScXlLNE1yNERCSWhGcDBNSXNIdVNV" +
            "EgEqAAIndGltZS1iZWZvcmUgMjAyMC0xMS0wM1QwODo1NzoyMS43MzY4MzRaAAAGID8gY6o60EGlFPpTsiD" +
            "VYmDl86VEhd6DIEqTX8hEtAky");
    static final boolean demoSecure = true;

    // Lighter errors
    public static final String wrongPassword = "Wrong password";

}
