/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

import com.goterl.lazycode.lazysodium.LazySodiumAndroid;
import com.goterl.lazycode.lazysodium.SodiumAndroid;
import com.goterl.lazycode.lazysodium.exceptions.SodiumException;
import com.goterl.lazycode.lazysodium.interfaces.SecretBox;
import com.goterl.lazycode.lazysodium.utils.Key;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class CipherUtil {

    public static String decrypt(String cipherText, byte[] accessKey)
            throws SodiumException {
        LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());
        Key key = Key.fromBytes(accessKey);
        byte[] cipherTextBytes = lazySodium.sodiumHex2Bin(cipherText);
        byte[] salt = Arrays.copyOfRange(cipherTextBytes, 0, AppConstants.nonceLen);
        byte[] cryptedBytes = Arrays.copyOfRange(
                cipherTextBytes, AppConstants.nonceLen, cipherTextBytes.length);
        String crypted = lazySodium.sodiumBin2Hex(cryptedBytes);
        return ((SecretBox.Lazy) lazySodium).cryptoSecretBoxOpenEasy(crypted, salt, key);
    }

    public static String crypt(String clearText, byte[] accessKey) {
        byte[] salt = AppUtils.genSalt();
        LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());
        Key key = Key.fromBytes(accessKey);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            byte[] crypted = lazySodium.sodiumHex2Bin(
                    ((SecretBox.Lazy) lazySodium).cryptoSecretBoxEasy(clearText, salt, key));
            output.write(salt);
            output.write(crypted);
            return lazySodium.sodiumBin2Hex(output.toByteArray());
        } catch (SodiumException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String decryptData(String cipherData, AccessFactory accessFactory)
            throws SodiumException {
        return decrypt(cipherData, accessFactory.getAccessKey());
    }

    public static String cryptData(String clearData, AccessFactory accessFactory) {
        return crypt(clearData, accessFactory.getAccessKey());
    }

}
