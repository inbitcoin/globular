/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

import java.security.SecureRandom;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

public class ScrambledPin {

    private final List<AbstractMap.SimpleImmutableEntry<Integer,Integer>> matrix;
    private final List<Integer> positions;


    public ScrambledPin() {

        positions = new ArrayList<>();
        positions.add(0);
        positions.add(1);
        positions.add(2);
        positions.add(3);
        positions.add(4);
        positions.add(5);
        positions.add(6);
        positions.add(7);
        positions.add(8);
        positions.add(9);

        matrix = new ArrayList<>();

        init();

    }

    private void init() {
        SecureRandom random = new SecureRandom();
        for (int i = 0; i < 10; i++) {
            int ran = random.nextInt(positions.size());
            AbstractMap.SimpleImmutableEntry<Integer,Integer> pair = new AbstractMap.SimpleImmutableEntry<>(i, positions.get(ran));
            positions.remove(ran);
            matrix.add(pair);
        }
    }

    public List<AbstractMap.SimpleImmutableEntry<Integer,Integer>> getMatrix()  {
        return matrix;
    }

}
