/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.network.NetworkService;
import it.inbitcoin.globular.ui.settings.SettingsActivity;

public class IncomingHandler extends Handler {

    private static final String TAG = "IncomingHandler";

    private final Context context;
    private final View view;
    private final IncomingHandlerCallback callback;

    private final Intent intent;
    private Snackbar snackbar;

    private int lastError;


    public IncomingHandler(Looper looper, Context context, View view,
                           IncomingHandlerCallback callback) {
        super(looper);
        Log.d(TAG, "created incoming handler " + this);
        this.context = context;
        this.view = view;
        this.callback = callback;
        intent = new Intent(context, SettingsActivity.class);
    }

    private void createSnackbar(String message, boolean addAction, int error) {
        if (snackbar != null && lastError == error)
            return;
        Log.d(TAG, "creating snackbar");
        snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE);
        int backgroundColor = context.getColor(R.color.primary_dark);
        snackbar.getView().setBackgroundColor(backgroundColor);
        if (!addAction || context.getClass().getSimpleName().equals("SettingsActivity"))
            return;
        snackbar.setActionTextColor(context.getColor(R.color.globular_red))
                .setAction(context.getString(R.string.go_to_settings), v -> context.startActivity(intent));
    }

    private void toggleSnackbar(boolean show, int error) {
        if (snackbar == null)
            return;
        if (show) {
            if (lastError == error && snackbar.isShown())
                return;
            Log.d(TAG, "showing snackbar " + this);
            snackbar.show();
        } else
            if (snackbar.isShown()) {
                Log.d(TAG, "hiding snackbar " + this);
                snackbar.dismiss();
            }

    }

    @Override
    public void handleMessage(Message msg) {
        if (msg.what == NetworkService.MSG_LAST_STATE && msg.arg2 == NetworkService.STATE_INVALID)
            return;
        Log.d(TAG, "Message from service: " + msg.what + ", arg2: " + msg.arg2);
        if (callback != null)
            callback.handleView(msg);
        if (msg.what == NetworkService.MSG_LAST_STATE && msg.arg2 == NetworkService.CONNECTION_OK) {
            toggleSnackbar(false, 0);
            return;
        }

        if (msg.what == NetworkService.MSG_LAST_STATE) {
            String message;
            if (msg.arg2 == NetworkService.INTERNET_ERROR) {
                message = context.getString(R.string.internet_problems);
                createSnackbar(message, false, msg.arg2);
            } else {
                message = AppUtils.getErrorString(context, msg.arg2);
                createSnackbar(message, true, msg.arg2);
            }
            toggleSnackbar(true, msg.arg2);
            lastError = msg.arg2;
        }
    }

}
