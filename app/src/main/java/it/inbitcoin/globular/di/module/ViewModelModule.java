/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.di.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import it.inbitcoin.globular.di.ViewModelKey;
import it.inbitcoin.globular.di.factory.AppViewModelFactory;
import it.inbitcoin.globular.ui.channeldetails.ChannelDetailsViewModel;
import it.inbitcoin.globular.ui.channels.ChannelsViewModel;
import it.inbitcoin.globular.ui.main.MainViewModel;
import it.inbitcoin.globular.ui.nodeinfo.NodeInfoViewModel;
import it.inbitcoin.globular.ui.openchannel.OpenChannelViewModel;
import it.inbitcoin.globular.ui.pairing.PairingViewModel;
import it.inbitcoin.globular.ui.receive.ReceiveViewModel;
import it.inbitcoin.globular.ui.send.SendViewModel;
import it.inbitcoin.globular.ui.settings.SettingsViewModel;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel repoViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SendViewModel.class)
    abstract ViewModel bindSendViewModel(SendViewModel repoViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ReceiveViewModel.class)
    abstract ViewModel bindReceiveViewModel(ReceiveViewModel receiveViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel.class)
    abstract ViewModel bindSettingsViewModel(SettingsViewModel settingsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PairingViewModel.class)
    abstract ViewModel bindPairingViewModel(PairingViewModel pairingViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(NodeInfoViewModel.class)
    abstract ViewModel bindNodeInfoViewModel(NodeInfoViewModel nodeInfoViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ChannelsViewModel.class)
    abstract ViewModel bindChannelsViewModel(ChannelsViewModel channelsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ChannelDetailsViewModel.class)
    abstract ViewModel bindChannelDetailsViewModel(ChannelDetailsViewModel channelDetailsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(OpenChannelViewModel.class)
    abstract ViewModel bindOpenChannelViewModel(OpenChannelViewModel openChannelViewModel);

    @Binds
    @Singleton
    abstract ViewModelProvider.Factory bindViewModelFactory(AppViewModelFactory factory);

}
