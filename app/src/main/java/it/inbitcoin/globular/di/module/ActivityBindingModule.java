/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import it.inbitcoin.globular.ui.about.AboutActivity;
import it.inbitcoin.globular.ui.channeldetails.ChannelDetailsActivity;
import it.inbitcoin.globular.ui.channels.ChannelsActivity;
import it.inbitcoin.globular.ui.landing.LandingActivity;
import it.inbitcoin.globular.ui.login.LoginActivity;
import it.inbitcoin.globular.ui.main.MainActivity;
import it.inbitcoin.globular.ui.nodeinfo.NodeInfoActivity;
import it.inbitcoin.globular.ui.openchannel.OpenChannelActivity;
import it.inbitcoin.globular.ui.pairing.PairingActivity;
import it.inbitcoin.globular.ui.pairing.PairingModule;
import it.inbitcoin.globular.ui.pincreate.PinCreateActivity;
import it.inbitcoin.globular.ui.pincreate.PinCreateModule;
import it.inbitcoin.globular.ui.pinentry.PinEntryActivity;
import it.inbitcoin.globular.ui.receive.ReceiveActivity;
import it.inbitcoin.globular.ui.receive.ReceiveModule;
import it.inbitcoin.globular.ui.send.SendActivity;
import it.inbitcoin.globular.ui.send.SendModule;
import it.inbitcoin.globular.ui.settings.SettingsActivity;
import it.inbitcoin.globular.ui.settings.SettingsModule;
import it.inbitcoin.globular.ui.transferdetails.TransferDetailsActivity;
import it.inbitcoin.globular.ui.transferdetails.TransferDetailsModule;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    abstract LoginActivity bindLoginActivity();

    @ContributesAndroidInjector
    abstract NodeInfoActivity bindNodeInfoActivity();

    @ContributesAndroidInjector
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = {SendModule.class})
    abstract SendActivity bindSendActivity();

    @ContributesAndroidInjector(modules = {ReceiveModule.class})
    abstract ReceiveActivity bindReceiveActivity();

    @ContributesAndroidInjector
    abstract LandingActivity bindLandingActivity();

    @ContributesAndroidInjector
    abstract PinEntryActivity bindPinEntryActivity();

    @ContributesAndroidInjector(modules = {SettingsModule.class})
    abstract SettingsActivity bindSettingsActivity();

    @ContributesAndroidInjector(modules = {PairingModule.class})
    abstract PairingActivity bindPairingActivity();

    @ContributesAndroidInjector
    abstract AboutActivity bindAboutActivity();

    @ContributesAndroidInjector(modules = {TransferDetailsModule.class})
    abstract TransferDetailsActivity bindTransferDetailsActivity();

    @ContributesAndroidInjector(modules = {PinCreateModule.class})
    abstract PinCreateActivity bindPinCreateActivity();

    @ContributesAndroidInjector
    abstract ChannelsActivity bindChannelsActivity();

    @ContributesAndroidInjector
    abstract ChannelDetailsActivity bindChannelDetailsActivity();

    @ContributesAndroidInjector
    abstract OpenChannelActivity bindOpenChannelActivity();

}
