/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.di.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import it.inbitcoin.globular.Globular;
import it.inbitcoin.globular.di.module.ActivityBindingModule;
import it.inbitcoin.globular.di.module.RetrofitModule;
import it.inbitcoin.globular.di.module.ServiceModule;
import it.inbitcoin.globular.di.module.SharedPreferencesModule;
import it.inbitcoin.globular.di.module.UtilModule;
import it.inbitcoin.globular.di.module.ViewModelModule;

@Component(modules={
        AndroidSupportInjectionModule.class,
        ViewModelModule.class,
        ActivityBindingModule.class,
        ServiceModule.class,
        RetrofitModule.class,
        SharedPreferencesModule.class,
        UtilModule.class})
@Singleton
public interface AppComponent extends AndroidInjector<Globular> {

    @Component.Factory
    interface Factory {
        AppComponent create(@BindsInstance Context context);
    }

    void inject(Globular app);

}
