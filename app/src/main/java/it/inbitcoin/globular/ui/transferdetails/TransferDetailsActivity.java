/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.transferdetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.ShareActionProvider;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.network.NetworkService;
import it.inbitcoin.globular.ui.transferdetails.invoice.InvoiceFragment;
import it.inbitcoin.globular.ui.transferdetails.payment.PaymentFragment;
import it.inbitcoin.globular.ui.transferdetails.transaction.TransactionFragment;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.IncomingHandler;
import it.inbitcoin.globular.utils.NetworkServiceConnection;
import it.inbitcoin.globular.utils.TimeoutUtil;
import it.inbitcoin.globular.utils.Transfer;

import static it.inbitcoin.globular.utils.AppUtils.addFragmentToActivity;

public class TransferDetailsActivity extends DaggerAppCompatActivity {

    private static final String TAG = "TransferDetailsActivity";

    @Inject
    InvoiceFragment mInvoiceFragment;

    @Inject
    PaymentFragment mPaymentFragment;

    @Inject
    TransactionFragment mTransactionFragment;

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    TimeoutUtil mTimeoutUtil;

    MenuItem mShareMenuItem;
    public boolean noShare = true;

    private FrameLayout invoiceLayout;
    private FrameLayout paymentLayout;
    private FrameLayout transactionLayout;

    public Transfer mTransfer;

    private Toolbar mToolbar;

    private LinearLayout activityLayout;
    private FrameLayout privateLayout;

    private Looper activityLooper;
    private IncomingHandler incomingHandler;
    private final NetworkServiceConnection mConnection = new NetworkServiceConnection();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_details);

        activityLayout = findViewById(R.id.transfer_details_ll);
        privateLayout = findViewById(R.id.transfer_details_private_layout);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (getIntent().hasExtra(AppConstants.transfer))
                mTransfer = extras.getParcelable(AppConstants.transfer);
        }

        activityLooper = Looper.myLooper();
        incomingHandler = new IncomingHandler(
                activityLooper, this, activityLayout, null);
        mConnection.mMessenger = new Messenger(incomingHandler);

        invoiceLayout = findViewById(R.id.fragment_invoice);
        paymentLayout = findViewById(R.id.fragment_payment);
        transactionLayout = findViewById(R.id.fragment_transaction);

        mToolbar = findViewById(R.id.toolbar_transfer_details);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        switch (mTransfer.getTransferType()) {
            case Transfer.INVOICES:
                setToolbarTitle(R.string.invoice);
                addFragmentToActivity(getSupportFragmentManager(), mInvoiceFragment,
                        R.id.fragment_invoice, false, false);
                invoiceLayout.setVisibility(View.VISIBLE);
                break;
            case Transfer.PAYMENTS:
                setToolbarTitle(R.string.payment);
                addFragmentToActivity(getSupportFragmentManager(), mPaymentFragment,
                        R.id.fragment_payment, false, false);
                paymentLayout.setVisibility(View.VISIBLE);
                break;
            case Transfer.TRANSACTIONS:
                setToolbarTitle(R.string.transaction);
                addFragmentToActivity(getSupportFragmentManager(), mTransactionFragment,
                        R.id.fragment_transaction, false, false);
                transactionLayout.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!AppUtils.isTimedOut(mAccessFactory, mTimeoutUtil, this)) {
            privateLayout.setVisibility(View.VISIBLE);
            doBindService();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mTimeoutUtil.updatePin();
        privateLayout.setVisibility(View.GONE);
        doUnbindService();
    }

    void doBindService() {
        bindService(new Intent(TransferDetailsActivity.this, NetworkService.class),
                mConnection, Context.BIND_AUTO_CREATE);
        mConnection.mIsBound = true;
    }

    void doUnbindService() {
        if (mConnection.mIsBound) {
            if (mConnection.mMessengerService != null) {
                try {
                    Message msg = Message.obtain(null, NetworkService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mConnection.mMessenger;
                    mConnection.mMessengerService.send(msg);
                } catch (RemoteException ignored) { }
            }
            unbindService(mConnection);
            mConnection.mIsBound = false;
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void setToolbarTitle(int title) {
        mToolbar.setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share, menu);
        mShareMenuItem = menu.findItem(R.id.menu_item_share);
        ShareActionProvider shareActionProvider =
                (ShareActionProvider) MenuItemCompat.getActionProvider(mShareMenuItem);
        shareActionProvider.setShareIntent(AppUtils.getShareIntent(
                this, mInvoiceFragment.mPaymentRequest, AppConstants.qrInvoiceDetail));
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        if (noShare)
            mShareMenuItem.setVisible(false);
        else
            mShareMenuItem.setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

}
