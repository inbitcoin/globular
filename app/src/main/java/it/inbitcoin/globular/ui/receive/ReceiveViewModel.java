/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.receive;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.inbitcoin.globular.AddressType;
import it.inbitcoin.globular.data.AppRepository;
import it.inbitcoin.globular.data.model.GrpcResponse;

@Singleton
public class ReceiveViewModel extends ViewModel {

    @NonNull
    private final AppRepository appRepo;

    private MutableLiveData<GrpcResponse> cachedCreateInvoiceResponse;
    private MutableLiveData<GrpcResponse> cachedNewAddressResponse;


    @Inject
    ReceiveViewModel(@NonNull AppRepository appRepo) {
        this.appRepo = appRepo;
    }

    LiveData<GrpcResponse> createInvoice(double amount, String description) {
        if (this.cachedCreateInvoiceResponse == null) {
            this.cachedCreateInvoiceResponse = new MutableLiveData<>();
        }
        appRepo.createInvoice(this.cachedCreateInvoiceResponse, amount, description);
        return this.cachedCreateInvoiceResponse;
    }

    LiveData<GrpcResponse> createAddress(AddressType addressType) {
        if (cachedNewAddressResponse == null)
            cachedNewAddressResponse = new MutableLiveData<>();
        appRepo.newAddress(cachedNewAddressResponse, addressType);
        return cachedNewAddressResponse;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        this.cachedCreateInvoiceResponse = null;
        this.cachedNewAddressResponse = null;
    }

}