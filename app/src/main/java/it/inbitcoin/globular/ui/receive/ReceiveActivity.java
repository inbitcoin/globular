/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.receive;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.network.NetworkService;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.IncomingHandler;
import it.inbitcoin.globular.utils.IncomingHandlerCallback;
import it.inbitcoin.globular.utils.NetworkServiceConnection;
import it.inbitcoin.globular.utils.TimeoutUtil;

public class ReceiveActivity extends DaggerAppCompatActivity {

    private static final String TAG = "ReceiveActivity";

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private ReceiveViewModel mViewModel;

    @Inject
    ReceiveOffChainFragment mReceiveOffChainFragment;

    @Inject
    ReceiveOnChainFragment mReceiveOnChainFragment;

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    TimeoutUtil mTimeoutUtil;

    private CoordinatorLayout activityLayout;

    private Looper activityLooper;
    private IncomingHandler incomingHandler;
    private final NetworkServiceConnection mConnection = new NetworkServiceConnection();

    private Toolbar mToolbar;

    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private PagerAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_receive);

        activityLayout = findViewById(R.id.receive_coordinator_layout);

        mToolbar = findViewById(R.id.toolbar_receive);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mTabLayout = findViewById(R.id.tab_layout_receive);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.off_chain));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.on_chain));
        mViewPager = findViewById(R.id.view_pager_receive);
        adapter = new PagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapter);

        activityLooper = Looper.myLooper();
        incomingHandler = new IncomingHandler(
                activityLooper, this, activityLayout, new ReceiveHandlerCallback());
        mConnection.mMessenger = new Messenger(incomingHandler);

        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(ReceiveViewModel.class);

        setListeners();
    }

    private void setListeners() {
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull String name, @NonNull Context context,
                             @NonNull AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!AppUtils.isTimedOut(mAccessFactory, mTimeoutUtil, this))
            doBindService();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mTimeoutUtil.updatePin();
        doUnbindService();
    }

    void doBindService() {
        bindService(new Intent(ReceiveActivity.this, NetworkService.class),
                mConnection, Context.BIND_AUTO_CREATE);
        mConnection.mIsBound = true;
    }

    void doUnbindService() {
        if (mConnection.mIsBound) {
            if (mConnection.mMessengerService != null) {
                try {
                    Message msg = Message.obtain(null, NetworkService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mConnection.mMessenger;
                    mConnection.mMessengerService.send(msg);
                } catch (RemoteException ignored) { }
            }
            unbindService(mConnection);
            mConnection.mIsBound = false;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    class ReceiveHandlerCallback implements IncomingHandlerCallback {

        @Override
        public void handleView(Message msg) {
            if (msg.what == NetworkService.MSG_LAST_STATE &&
                    msg.arg2 == NetworkService.CONNECTION_OK) {
                mReceiveOffChainFragment.mCreateInvoiceButton.setEnabled(true);
                mReceiveOnChainFragment.mCreateAddressButton.setEnabled(true);
            }
        }
    }

    private class PagerAdapter extends FragmentPagerAdapter {

        PagerAdapter(FragmentManager fm) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return mReceiveOffChainFragment;
            } else {
                return mReceiveOnChainFragment;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

}