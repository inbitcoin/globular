/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.inputmethod.EditorInfoCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.preference.Preference;
import androidx.preference.PreferenceScreen;

import com.google.zxing.integration.android.IntentIntegrator;
import com.takisoft.preferencex.EditTextPreference;
import com.takisoft.preferencex.PreferenceFragmentCompat;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.HasAndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import io.reactivex.schedulers.Schedulers;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.prefs.PreferencesHelper;
import it.inbitcoin.globular.ui.scan.ScanActivity;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.ApiEndPoint;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.UpdateObserver;

import static it.inbitcoin.globular.utils.AppUtils.getPreferenceKey;

public class ConnectionSettingsFragment extends PreferenceFragmentCompat
        implements HasAndroidInjector,
        androidx.preference.PreferenceFragmentCompat.OnPreferenceStartScreenCallback,
        SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "SettingsFragment";

    @Inject
    ApiEndPoint mApiEndPoint;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private SettingsViewModel mViewModel;

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    PreferencesHelper mPreferencesHelper;

    private SettingsActivity parentActivity;

    private EditTextPreference mHostEditTextPref;
    private EditTextPreference mPortEditTextPref;
    private PreferenceScreen mGetTLSCertificateScreen;
    private PreferenceScreen mGetMacaroonScreen;

    private ProgressBar mProgressBar;

    @Inject
    public ConnectionSettingsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        parentActivity = (SettingsActivity) getActivity();
        assert parentActivity != null;
        mProgressBar = parentActivity.mProgressBar;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(SettingsViewModel.class);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPreferencesHelper.getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPreferencesHelper.getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.connection_preferences, rootKey);

        mHostEditTextPref = findPreference(PreferencesHelper.LIGHTER_HOST);
        mHostEditTextPref.setOnBindEditTextListener(editText -> {
            editText.setImeOptions(EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING);
            editText.setHint(R.string.host_edittext_hint);
        });
        mPortEditTextPref = findPreference(PreferencesHelper.LIGHTER_PORT);
        mPortEditTextPref.setOnBindEditTextListener(editText -> {
            editText.setImeOptions(EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING);
            editText.setHint(R.string.lighter_port_default);
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        });
        mGetTLSCertificateScreen = findPreference(PreferencesHelper.LIGHTER_TLS_CERTIFICATE);
        mGetMacaroonScreen = findPreference(PreferencesHelper.LIGHTER_MACAROON);

        setListeners();
    }

    private void setListeners() {
        mGetTLSCertificateScreen.setOnPreferenceClickListener(preference -> {
            openScanner(ScanActivity.REQ_CERTIFICATE);
            return false;
        });
        mGetMacaroonScreen.setOnPreferenceClickListener(preference -> {
            openScanner(ScanActivity.REQ_MACAROON);
            return false;
        });
    }

    @Override
    public boolean onPreferenceStartScreen(androidx.preference.PreferenceFragmentCompat caller, PreferenceScreen pref) {
        return false;
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        return super.onPreferenceTreeClick(preference);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            switch (key) {
                case PreferencesHelper.LIGHTER_HOST:
                    mProgressBar.setVisibility(View.VISIBLE);
                    mViewModel.updateLighterHost().observeOn(Schedulers.io())
                            .subscribe(new UpdateObserver(mProgressBar));
                    mApiEndPoint.setChanged(true);
                    break;
                case PreferencesHelper.LIGHTER_PORT:
                    mProgressBar.setVisibility(View.VISIBLE);
                    mViewModel.updateLighterPort().observeOn(Schedulers.io())
                            .subscribe(new UpdateObserver(mProgressBar));
                    mApiEndPoint.setChanged(true);
                    break;
                case PreferencesHelper.LIGHTER_SECURE_CONN:
                    mProgressBar.setVisibility(View.VISIBLE);
                    mViewModel.updateLighterSecure().observeOn(Schedulers.io())
                            .subscribe(new UpdateObserver(mProgressBar));
                    mApiEndPoint.setChanged(true);
                    break;
                case PreferencesHelper.LIGHTER_DEMO_MODE:
                    mProgressBar.setVisibility(View.VISIBLE);
                    mViewModel.updateLighterDemoMode().observeOn(Schedulers.io())
                            .subscribe(new UpdateObserver(mProgressBar));
                    mApiEndPoint.setChanged(true);
                    break;
                default:
                    break;
        }
    }

    private void openScanner(int preferenceKey) {
        IntentIntegrator.forSupportFragment(this).setBeepEnabled(false)
                .setOrientationLocked(true).setPrompt("")
                .setDesiredBarcodeFormats(IntentIntegrator.QR_CODE).setCameraId(0)
                .setCaptureActivity(ScanActivity.class)
                .setRequestCode(preferenceKey)
                .initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "Request code: " + requestCode + ", result code: " + resultCode);
        mProgressBar.setVisibility(View.VISIBLE);
        String preferenceKey = getPreferenceKey(requestCode);
        String clearPref = AppUtils.getDataFromScan(requestCode, resultCode, data, parentActivity);
        Log.d(TAG, "clear pref: " + clearPref);
        if (clearPref.isEmpty())
            return;
        AppUtils.cryptPref(mAccessFactory, mPreferencesHelper, preferenceKey, clearPref);
        updatePref(preferenceKey);
    }

    private void updatePref(String preferenceKey) {
        if (preferenceKey.equals(PreferencesHelper.LIGHTER_TLS_CERTIFICATE)) {
            mViewModel.updateLighterTLSCertificate().observeOn(Schedulers.io())
                    .subscribe(new UpdateObserver(mProgressBar));
        }
        if (preferenceKey.equals(PreferencesHelper.LIGHTER_MACAROON)) {
            mViewModel.updateLighterMacaroon().observeOn(Schedulers.io())
                    .subscribe(new UpdateObserver(mProgressBar));
        }
        mApiEndPoint.setChanged(true);
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return null;
    }

}
