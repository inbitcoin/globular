/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.send;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.inbitcoin.globular.data.AppRepository;
import it.inbitcoin.globular.data.model.GrpcResponse;

@Singleton
public class SendViewModel extends ViewModel {

    private static final String TAG = "SendViewModel";

    @NonNull
    private final AppRepository appRepo;

    private MutableLiveData<GrpcResponse> cachedDecodeInvoiceResponse;
    private MutableLiveData<GrpcResponse> cachedPayInvoiceResponse;
    private MutableLiveData<GrpcResponse> cachedPayOnChainResponse;
    String cachedPaymentRequest;


    @Inject
    SendViewModel(@NonNull AppRepository appRepo) {
        this.appRepo = appRepo;
    }

    LiveData<GrpcResponse> decodeInvoice(String paymentRequest) {
        if (cachedDecodeInvoiceResponse == null) {
            cachedDecodeInvoiceResponse = new MutableLiveData<>();
        }
        cachedPaymentRequest = paymentRequest;
        appRepo.decodeInvoice(this.cachedDecodeInvoiceResponse, cachedPaymentRequest);
        return cachedDecodeInvoiceResponse;
    }

    LiveData<GrpcResponse> payOnChain(String paymentRequest, double amount, long satByte) {
        if (cachedPayOnChainResponse == null)
            cachedPayOnChainResponse = new MutableLiveData<>();
        cachedPaymentRequest = paymentRequest;
        appRepo.payOnChain(cachedPayOnChainResponse, cachedPaymentRequest, amount, satByte);
        return cachedPayOnChainResponse;
    }

    LiveData<GrpcResponse> payOffChain(double amount) {
        if (cachedPayInvoiceResponse == null) {
            cachedPayInvoiceResponse = new MutableLiveData<>();
        }
        try {
            if (hasAmountEncoded()) {
                Log.d(TAG, "amount is not settable");
                appRepo.payOffchain(cachedPayInvoiceResponse, cachedPaymentRequest, 0);
            } else {
                Log.d(TAG, "amount is settable");
                appRepo.payOffchain(cachedPayInvoiceResponse, cachedPaymentRequest, amount);
            }
        } catch (NullPointerException e) {
            Log.d(TAG, e.toString());
        }
        return cachedPayInvoiceResponse;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        cachedPaymentRequest = null;
        cachedDecodeInvoiceResponse = null;
        cachedPayInvoiceResponse = null;
        cachedPayOnChainResponse = null;
    }

    boolean hasAmountEncoded() {
        int separator = cachedPaymentRequest.lastIndexOf('1');
        String hrp = cachedPaymentRequest.substring(0, separator);
        return hrp.matches(".*\\d.*");
    }

}
