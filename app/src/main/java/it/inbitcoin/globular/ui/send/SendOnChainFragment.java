/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.send;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.protobuf.GeneratedMessageLite;

import net.i2p.android.ext.floatingactionbutton.FloatingActionButton;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import it.inbitcoin.globular.PayOnChainResponse;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.model.GrpcResponse;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;

public class SendOnChainFragment extends DaggerFragment {

    private static final String TAG = "SendOffChainFragment";

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private SendViewModel mViewModel;

    @Inject
    AccessFactory mAccessFactory;

    private SendActivity parentActivity;

    private TextView mPaymentRequestTextView;
    private EditText mAmountEditText;
    private EditText mSatByteEditText;
    private FloatingActionButton mPayButton;

    Map<String, Object> parameterMap = new LinkedHashMap<>();
    private String paymentRequest;
    private String address;
    private double mAmount;
    private long mSatByte;

    public ProgressBar mProgressBar;


    @Inject
    public SendOnChainFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        parentActivity = (SendActivity) getActivity();

        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(SendViewModel.class);
    }

    private void parsePaymentRequest() {

        mPaymentRequestTextView.setText(paymentRequest);

        paymentRequest = paymentRequest.replaceAll(AppConstants.uriSchemeBitcoin, "")
                .replaceAll(AppConstants.uriSchemeBitcoin.toUpperCase(), "").trim();

        if (paymentRequest.contains("?")) {
            String[] addressSplitTokens = paymentRequest.split("\\?", 2);

            if (addressSplitTokens.length == 0)
                showError();

            address = addressSplitTokens[0];
            Log.d(TAG, "found address");

            if (addressSplitTokens.length > 1) {
                Log.d(TAG, "found params");
                String[] nameValuePairTokens = addressSplitTokens[1].split("&");

                for (String nameValuePairToken : nameValuePairTokens) {
                    final int sepIndex = nameValuePairToken.indexOf('=');
                    if (sepIndex == -1 || sepIndex == 0)
                        showError();

                    String nameToken = nameValuePairToken.substring(0, sepIndex).toLowerCase();
                    String valueToken = nameValuePairToken.substring(sepIndex + 1);

                    if (AppConstants.amount.equals(nameToken)) {
                        if (valueToken.contains(","))
                            showError();
                        try {
                            String amount = new BigDecimal(valueToken).movePointRight(6)
                                    .setScale(2, RoundingMode.HALF_EVEN).toPlainString();
                            mAmount = Double.valueOf(amount);
                            putWithValidation(AppConstants.amount, mAmount);
                        } catch (NumberFormatException e) {
                            showError();
                        }
                    } else {
                        try {
                            putWithValidation(nameToken, URLDecoder.decode(valueToken, "UTF-8"));
                        } catch (UnsupportedEncodingException e) {
                            showError();
                        }
                    }
                }
            }
            if (mAmount > 0)
                showAmount();
        } else {
            address = paymentRequest;
            if (address.isEmpty())
                showError();
        }
    }

    private void putWithValidation(String key, Object value) {
        if (parameterMap.containsKey(key)) {
            showError();
        } else {
            parameterMap.put(key, value);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(SendViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_send_onchain, container, false);

        mPaymentRequestTextView = root.findViewById(R.id.tv_payment_request);
        mAmountEditText = root.findViewById(R.id.et_send_amount);
        mAmountEditText.setEnabled(true);
        mSatByteEditText = root.findViewById(R.id.et_send_fee_sats_byte);
        mProgressBar = root.findViewById(R.id.pb_send);
        mPayButton = root.findViewById(R.id.btn_pay);
        mPayButton.setEnabled(false);
        setListeners();

        return root;
    }

    public void setListeners() {
        mPayButton.setOnClickListener(v -> {
            getUserInput();
            mPayButton.setEnabled(false);
            mProgressBar.setVisibility(View.VISIBLE);
            payOnChain();
        });
        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getUserInput();
                if (mAmount > 0 && mSatByte > 0) {
                    mPayButton.setEnabled(true);
                } else {
                    mPayButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        mAmountEditText.addTextChangedListener(watcher);
        mAmountEditText.addTextChangedListener(
                new AppUtils.DecimalInputWatcher(mAmountEditText, 14, 2, 0, 21000000000000.0));
        mSatByteEditText.addTextChangedListener(watcher);
        mSatByteEditText.addTextChangedListener(
                new AppUtils.DecimalInputWatcher(mSatByteEditText, 13, 0, 1, 9545454545454.0)
        );
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getArguments() != null) {
            paymentRequest = getArguments().getString(AppConstants.paymentRequest);
            parsePaymentRequest();
        } else {
            showError();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            parentActivity.finish();
        } catch (NullPointerException ignored) {}
    }

    private void getUserInput() {
        try {
            mAmount = Double.parseDouble(mAmountEditText.getText().toString());
            mSatByte = Long.parseLong(mSatByteEditText.getText().toString());
        } catch (NumberFormatException ignored) {}
    }

    private void payOnChain() {
        mViewModel.payOnChain(address, mAmount, mSatByte)
                .observe(this, this::parseGrpcResponse);
    }

    private void parseGrpcResponse(GrpcResponse grpcResponse) {
        Log.d(TAG, "parsing gRPC response");
        if (grpcResponse == null)
            return;
        mProgressBar.setVisibility(View.INVISIBLE);
        if (grpcResponse.getMessage() != null) {
            GeneratedMessageLite responseType =
                    grpcResponse.getMessage().getDefaultInstanceForType();
        if (responseType == PayOnChainResponse.getDefaultInstance()) {
                PayOnChainResponse response = (PayOnChainResponse) grpcResponse.getMessage();
                showTxid(response.getTxid());
                parentActivity.finish();
            }
        } else if (grpcResponse.getErrorDescription() != null &&
                !grpcResponse.getErrorDescription().equals("")) {
            Toast.makeText(parentActivity,
                    grpcResponse.getErrorDescription(), Toast.LENGTH_LONG).show();
            parentActivity.finish();
        } else {
            Toast.makeText(parentActivity, getString(R.string.error), Toast.LENGTH_LONG).show();
            parentActivity.finish();
        }
    }

    private void showTxid(String txid) {
        Toast.makeText(parentActivity, getString(R.string.paid_onchain) + " " + txid, Toast.LENGTH_LONG).show();
    }

    private void showAmount() {
        mAmountEditText.setEnabled(false);
        mAmountEditText.setText(String.valueOf(mAmount));
    }

    private void showError() {
        Toast.makeText(parentActivity, R.string.incorrect_pay_req, Toast.LENGTH_LONG).show();
        Log.d(TAG, "incorrect payment request");
        parentActivity.finish();
    }

}

