/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.channels;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import it.inbitcoin.globular.R;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.Channel;

public class ChannelsRecyclerViewAdapter
        extends RecyclerView.Adapter<ChannelsRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "ChannelsRecyclerViewAdapter";

    private Channel mChannel;

    private final List<Channel> mChannels;

    private OnChannelClickListener mClickListener;


    ChannelsRecyclerViewAdapter(List<Channel> items) {
        mChannels = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_channel, parent, false);
        return new ViewHolder(view);
    }

    public interface OnChannelClickListener {
        void onChannelClick(View view, int position);
    }

    void setClickListener(OnChannelClickListener listener) {
        mClickListener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        mChannel = mChannels.get(position);

        if (mClickListener != null)
            holder.itemView.setOnClickListener(view -> mClickListener.onChannelClick(view, position));


        showChannel(holder);
    }

    private void showChannel(ViewHolder holder) {

        Double localBalance = mChannel.getLocalBalance();
        holder.mLocalBalanceTextView.setText(AppUtils.formatNumber(localBalance.toString(), 5));

        Double remoteBalance = mChannel.getRemoteBalance();
        holder.mRemoteBalanceTextView.setText(AppUtils.formatNumber(remoteBalance.toString(), 5));

        String remotePubkey = mChannel.getRemotePubkey();
        holder.mRemotePubkeyTextView.setText(AppUtils.formatRemotePubKeyShort(remotePubkey));

        String channelId = mChannel.getChannelId();
        if (!TextUtils.isEmpty(channelId)) holder.mChannelIdTextView.setText(channelId);

        Double capacity = mChannel.getCapacity();
        holder.mChannelBalanceProgressBar.setMax(capacity.intValue());
        holder.mChannelBalanceProgressBar.setProgress(localBalance.intValue());

        String alias = mChannel.getAlias();
        if (!TextUtils.isEmpty(alias)) holder.mRemotePubkeyTextView.setText(alias);

        String color = mChannel.getColor();
        if (!TextUtils.isEmpty(color))
            holder.mChannelColorView.setBackgroundColor(Color.parseColor(color));
    }

    Channel getChannel(int id) {
        return mChannels.get(id);
    }

    @Override
    public int getItemCount() {
        if (mChannels != null)
            return mChannels.size();
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final View mView;
        private final TextView mChannelIdTextView;
        private final TextView mLocalBalanceTextView;
        private final TextView mRemoteBalanceTextView;
        private final TextView mRemotePubkeyTextView;
        private final ProgressBar mChannelBalanceProgressBar;
        private final View mChannelColorView;

        private ViewHolder(View view) {
            super(view);
            mView = view;
            mChannelIdTextView = view.findViewById(R.id.tv_channel_id);
            mRemotePubkeyTextView = view.findViewById(R.id.tv_channel_remote_pubkey);
            mLocalBalanceTextView = view.findViewById(R.id.tv_channel_local_balance);
            mRemoteBalanceTextView = view.findViewById(R.id.tv_channel_remote_balance);
            mChannelBalanceProgressBar = view.findViewById(R.id.pb_channel_balance);
            mChannelColorView = view.findViewById(R.id.vw_channel_color);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) mClickListener.onChannelClick(v, getAdapterPosition());
        }
    }

    void updateChannels(List<Channel> channels) {
        mChannels.clear();
        mChannels.addAll(channels);
        notifyDataSetChanged();
    }

    void clearList() {
        int size = mChannels.size();
        if (size > 0) {
            mChannels.subList(0, size).clear();
            notifyItemRangeChanged(0, size);
        }
    }

}
