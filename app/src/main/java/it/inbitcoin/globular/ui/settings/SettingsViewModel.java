/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.settings;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Single;
import it.inbitcoin.globular.data.AppRepository;
import it.inbitcoin.globular.data.model.GrpcResponse;

@Singleton
public class SettingsViewModel extends ViewModel {

    @NonNull
    private final AppRepository appRepo;


    @Inject
    SettingsViewModel(@NonNull AppRepository appRepo) {
        this.appRepo = appRepo;
    }

    Single<GrpcResponse> getInfo() {
        return appRepo.getInfo();
    }

    Single<GrpcResponse> unlockLighter(String password, boolean unlockNode) {
        return appRepo.unlockLighter(password, unlockNode);
    }

    Single<GrpcResponse> lockLighter(String password) {
        return appRepo.lockLighter(password);
    }

    Single<GrpcResponse> unlockNode(String password) {
        return appRepo.unlockNode(password);
    }

    Completable updateLighterHost() {
        return appRepo.updateLighterHostAsync();
    }

    Completable updateLighterPort() {
        return appRepo.updateLighterPortAsync();
    }

    Completable updateLighterSecure() {
        return appRepo.updateLighterSecureAsync();
    }

    Completable updateLighterTLSCertificate() {
        return appRepo.updateLighterTLSCertificateAsync();
    }

    Completable updateLighterMacaroon() {
        return appRepo.updateLighterMacaroonAsync();
    }

    Completable recryptData() {
        return appRepo.recryptData();
    }

    Completable updateLighterDemoMode() {
        return appRepo.updateLighterDemoModeAsync();
    }

}
