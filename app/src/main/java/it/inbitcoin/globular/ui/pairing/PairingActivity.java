/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.pairing;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.zxing.integration.android.IntentIntegrator;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.schedulers.Schedulers;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.prefs.PreferencesHelper;
import it.inbitcoin.globular.ui.scan.ScanActivity;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.TimeoutUtil;
import it.inbitcoin.globular.utils.UpdateObserver;

public class PairingActivity extends DaggerAppCompatActivity
        implements StartPairingFragment.onStartPairingListener,
        ConnectionCheckFragment.onConnectionCheckedListener {

    private static final String TAG = PairingActivity.class.getSimpleName();

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private PairingViewModel mViewModel;

    @Inject
    StartPairingFragment mStartPairingFragment;

    @Inject
    ConnectionCheckFragment mConnectionCheckFragment;

    @Inject
    EndPairingFragment mEndPairingFragment;

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    PreferencesHelper mPreferencesHelper;

    @Inject
    TimeoutUtil mTimeoutUtil;

    private Toolbar mToolbar;

    ProgressBar mProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pairing);

        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(PairingViewModel.class);

        mToolbar = findViewById(R.id.toolbar_pairing);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mProgressBar = findViewById(R.id.pb_pairing);

        showStartPairingFragment();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        AppUtils.isTimedOut(mAccessFactory, mTimeoutUtil, this);
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
        mTimeoutUtil.updatePin();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        mViewModel.opCompleted = true;
        super.onDestroy();
    }

    private void showStartPairingFragment() {
        AppUtils.addFragmentToActivity(
                getSupportFragmentManager(), mStartPairingFragment, R.id.fragment_pairing,
                false, false);
    }

    private void showConnectionCheckFragment(){
        AppUtils.addFragmentToActivity(
                getSupportFragmentManager(), mConnectionCheckFragment, R.id.fragment_pairing,
                true, true);
    }

    private void showEndPairingFragment() {
        AppUtils.addFragmentToActivity(
                getSupportFragmentManager(), mEndPairingFragment, R.id.fragment_pairing,
                true, true);
    }

    public void scanLighterconnectUri() {
        new IntentIntegrator(this).setBeepEnabled(false)
                .setOrientationLocked(true).setPrompt("")
                .setDesiredBarcodeFormats(IntentIntegrator.QR_CODE).setCameraId(0)
                .setRequestCode(ScanActivity.REQ_LIGHTERCONNECT_URI)
                .setCaptureActivity(ScanActivity.class)
                .initiateScan();
    }

    private void scanMacaroonUri() {
        new IntentIntegrator(this).setBeepEnabled(false)
                .setOrientationLocked(true).setPrompt("")
                .setDesiredBarcodeFormats(IntentIntegrator.QR_CODE).setCameraId(0)
                .setCaptureActivity(ScanActivity.class)
                .setRequestCode(ScanActivity.REQ_MACAROON_URI)
                .initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "Request code: " + requestCode + ", result code: " + resultCode);

        if (resultCode == ScanActivity.ABORT) {
            Log.d(TAG, "scan abort");
            return;
        }

        if (requestCode == ScanActivity.REQ_LIGHTERCONNECT_URI) {
            String lighterconnectUri = AppUtils.getDataFromScan(
                    requestCode, resultCode, data, this);
            Log.d(TAG, "Scanned lighterconnect uri: " + lighterconnectUri);
            parseLighterconnectUri(lighterconnectUri);
        }

        if (requestCode == ScanActivity.REQ_MACAROON_URI) {
            String macaroonUri = AppUtils.getDataFromScan(
                    requestCode, resultCode, data, this);
            Log.d(TAG, "Scanned macaroon uri: " + macaroonUri);
            parseMacaroonUri(macaroonUri);
        }
    }

    private void parseLighterconnectUri(String lighterconnectUri) {
        Uri uri = Uri.parse(lighterconnectUri);

        String scheme = uri.getScheme();
        String host = uri.getHost();
        int port = uri.getPort();
        String cert = "";

        if (scheme != null && scheme.startsWith("lighterconnect") && host != null && port > 1) {
            Log.d(TAG, "lighterconnect uri correct");
            Log.d(TAG, "URI host " + host);
            Log.d(TAG, "URI port " + port);
            Log.d(TAG, "URI scheme " + scheme);
            try {
                cert = uri.getQueryParameter("cert");
                Log.d(TAG, "URI cert " + cert);
            } catch (UnsupportedOperationException | NullPointerException ignored) {
            }
            mViewModel.host = host;
            mViewModel.port = port;
            if (cert != null && !cert.isEmpty()) {
                cert = cert.replace('-', '+').replace('_', '/');
                cert = AppUtils.addNewlineEveryNChars(cert, 64);
                mViewModel.cert = "-----BEGIN CERTIFICATE-----\n" + cert + "-----END CERTIFICATE-----";
            }
            showConnectionCheckFragment();
        } else {
            Log.d(TAG, "lighterconnect uri incorrect");
            Toast.makeText(this, R.string.incorrect_lighterconnect_uri, Toast.LENGTH_LONG).show();
            scanLighterconnectUri();
        }
    }

    private void parseMacaroonUri(String macaroonUri) {
        Uri uri = Uri.parse(macaroonUri);
        String scheme = uri.getScheme();
        if (scheme != null && scheme.equals("macaroon")) {
            String macaroon = uri.getSchemeSpecificPart();
            Log.d(TAG, "MACAROON: " + macaroon);
            if (!macaroon.isEmpty()) {
                mViewModel.macaroon = macaroon;
                showEndPairingFragment();
            }
        }
        if (mViewModel.macaroon == null || mViewModel.macaroon.isEmpty()) {
            Log.d(TAG, "macaroon uri incorrect");
            Toast.makeText(this, R.string.incorrect_macaroon_uri, Toast.LENGTH_LONG).show();
            scanMacaroonUri();
        }
    }

    @Override
    public void startPairing() {
        mViewModel.cert = null;
        scanLighterconnectUri();
    }

    @Override
    public void connectionChecked() {
        mViewModel.macaroon = null;
        scanMacaroonUri();
    }

    void setSecureConnection(boolean secure) {
        mPreferencesHelper.setLighterSecureConn(secure);
        mViewModel.updateLighterSecure().observeOn(Schedulers.io())
                .subscribe(new UpdateObserver(mProgressBar));
    }

    void saveConnectionData() {
        Log.d(TAG, String.format("saving host %s and port %s", mViewModel.host, mViewModel.port));
        mPreferencesHelper.setLighterHost(mViewModel.host);
        mViewModel.updateLighterHost().observeOn(Schedulers.io())
                .subscribe(new UpdateObserver(mProgressBar));

        mPreferencesHelper.setLighterPort(String.valueOf(mViewModel.port));
        mViewModel.updateLighterPort().observeOn(Schedulers.io())
                .subscribe(new UpdateObserver(mProgressBar));

        if (mViewModel.cert != null && !mViewModel.cert.isEmpty()) {
            Log.d(TAG, "saving cert " + mViewModel.cert);
            AppUtils.cryptPref(mAccessFactory, mPreferencesHelper,
                               PreferencesHelper.LIGHTER_TLS_CERTIFICATE, mViewModel.cert);
            mViewModel.updateLighterTLSCertificate().observeOn(Schedulers.io())
                    .subscribe(new UpdateObserver(mProgressBar));
            setSecureConnection(true);
        } else
            setSecureConnection(false);
    }

    void saveMacaroonData() {
        Log.d(TAG, "saving macaroon " + mViewModel.macaroon);
        AppUtils.cryptPref(mAccessFactory, mPreferencesHelper,
                           PreferencesHelper.LIGHTER_MACAROON, mViewModel.macaroon);
        mViewModel.updateLighterMacaroon().observeOn(Schedulers.io())
                .subscribe(new UpdateObserver(mProgressBar));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
