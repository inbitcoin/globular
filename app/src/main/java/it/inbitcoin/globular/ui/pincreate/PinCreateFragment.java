/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.pincreate;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.PinEntryView;

public class PinCreateFragment extends DaggerFragment {

    private static final String ARG_TITLE = "TITLE";
    private static final String ARG_DESC = "DESC";
    private onPinEntryListener mListener;
    private PinEntryView entryView;
    private StringBuilder passPhrase = new StringBuilder();
    private ImageView[] pinEntries;
    private LinearLayout maskPinContainer;
    private String mTitle = null, mDescription = null;
    private static final String TAG = "PinCreateFragment";


    @Inject
    public PinCreateFragment() {

    }

    public static PinCreateFragment newInstance(String title, String description) {
        PinCreateFragment fragment = new PinCreateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_DESC, description);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.mTitle = arguments.getString(ARG_TITLE);
            this.mDescription = arguments.getString(ARG_DESC);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pinentry, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        entryView = view.findViewById(R.id.pin_entry_view);
        maskPinContainer = view.findViewById(R.id.pin_mask_container);
        initMaskView();
        if (this.mTitle != null)
            ((TextView) view.findViewById(R.id.pin_entry_title)).setText(this.mTitle);
        if (this.mDescription != null)
            ((TextView) view.findViewById(R.id.pin_entry_description)).setText(this.mDescription);
        setListeners();
    }

    private void setListeners() {
        entryView.setEntryListener((key, view) -> {
            Log.d(TAG, "onPinEntered");
            if (passPhrase.length() >= AccessFactory.PIN_LENGTH)
                return;
            passPhrase = passPhrase.append(key);
            addKeyText();
            propagateToActivity();
        });
        entryView.setClearListener(clearType -> {
            if (clearType == PinEntryView.KeyClearTypes.CLEAR) {
                if (passPhrase.length() - 1 >= 0)
                    passPhrase.deleteCharAt(passPhrase.length() - 1);
            } else {
                Log.d(TAG, "long press delete");
                passPhrase.setLength(0);
            }
            propagateToActivity();
            addKeyText();
        });
    }

    public void clearPin() {
        Log.d(TAG, "clearPin");
        passPhrase.setLength(0);
        propagateToActivity();
        addKeyText();
        entryView.resetPin();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof onPinEntryListener) {
            Log.d(TAG, "attached!");
            mListener = (onPinEntryListener) context;
        }
    }

    private void initMaskView() {
        pinEntries = new ImageView[AccessFactory.PIN_LENGTH];
        for (int i = 0; i < AccessFactory.PIN_LENGTH; i++) {
            pinEntries[i] = new ImageView(getActivity());


            pinEntries[i].setImageDrawable(ResourcesCompat.getDrawable(
                    getResources(), R.drawable.circle_dot_white, null));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(8, 0, 8, 0);
            maskPinContainer.addView(pinEntries[i], params);
        }
    }

    private void addKeyText() {
        for (int i = 0; i < AccessFactory.PIN_LENGTH; i++) {
            if (passPhrase.toString().length() > i) {
                pinEntries[i].getDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.ADD);
            } else {
                pinEntries[i].setImageDrawable(ResourcesCompat.getDrawable(
                        getResources(), R.drawable.circle_dot_white, null));
            }
        }
    }

    private void propagateToActivity() {
        if (this.mListener != null) {
            Log.d(TAG, "giving passphrase to activity: " + passPhrase);
            this.mListener.PinEntry(passPhrase.toString());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface onPinEntryListener {
        void PinEntry(String pin);
    }

}
