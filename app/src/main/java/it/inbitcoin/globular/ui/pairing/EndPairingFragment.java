/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.pairing;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.prefs.PreferencesHelper;
import it.inbitcoin.globular.ui.settings.SettingsFragment;

public class EndPairingFragment extends DaggerFragment {

    private static final String TAG = EndPairingFragment.class.getSimpleName();

    @Inject
    PreferencesHelper mPreferencesHelper;

    private PairingActivity parentActivity;

    private LinearLayout completeButton;
    private LinearLayout abortButton;


    @Inject
    public EndPairingFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        parentActivity = (PairingActivity) getActivity();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(
                    R.layout.fragment_end_pairing, container, false);

        completeButton = root.findViewById(R.id.btn_complete);
        abortButton = root.findViewById(R.id.btn_abort);

        setListeners();

        return root;
    }

    private void setListeners() {
        abortButton.setOnClickListener(v -> parentActivity.finish());
        completeButton.setOnClickListener(v -> {
            parentActivity.saveConnectionData();
            parentActivity.saveMacaroonData();
            parentActivity.setResult(SettingsFragment.LIGHTER_PAIRING_OK);
            parentActivity.finish();
        });
    }

}
