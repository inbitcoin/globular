/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.landing;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.text.HtmlCompat;

import dagger.android.support.DaggerAppCompatActivity;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.ui.login.LoginActivity;
import it.inbitcoin.globular.ui.pincreate.PinCreateActivity;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;

public class LandingActivity extends DaggerAppCompatActivity {

    public static final String TAG = "LandingActivity";

    private Button startButton;

    private TextView versionTextView;
    private TextView learnMoreTextView;

    private static final int LANDING_EXIT_REQUEST = 1;
    public static final int LANDING_EXIT_COMMAND = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        startButton = findViewById(R.id.btn_start);
        versionTextView = findViewById(R.id.tv_app_version);
        String version = AppUtils.getAppVersion(this);
        versionTextView.setText(version);
        Spanned learnMore = HtmlCompat.fromHtml(
                getString(R.string.learn_more), HtmlCompat.FROM_HTML_MODE_LEGACY);
        learnMoreTextView = findViewById(R.id.tv_learn_more);
        learnMoreTextView.setText(learnMore);
        learnMoreTextView.setMovementMethod(LinkMovementMethod.getInstance());

        setListeners();
    }

    private void setListeners() {
        startButton.setOnClickListener(view -> {
            Intent intent = new Intent(LandingActivity.this, PinCreateActivity.class);
            intent.putExtra(AppConstants.requestKey, PinCreateActivity.FROM_LANDING);
            startActivityForResult(intent, LANDING_EXIT_REQUEST);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LANDING_EXIT_REQUEST) {
            if (resultCode == LANDING_EXIT_COMMAND)
                finish();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(LoginActivity.LOGIN_EXIT_COMMAND);
        finish();
    }

}

