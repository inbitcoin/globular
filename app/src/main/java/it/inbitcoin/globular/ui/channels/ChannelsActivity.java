/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.channels;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.util.Pair;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.protobuf.GeneratedMessageLite;
import com.google.zxing.integration.android.IntentIntegrator;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import it.inbitcoin.globular.ListChannelsResponse;
import it.inbitcoin.globular.ListPeersResponse;
import it.inbitcoin.globular.Peer;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.AppRepository;
import it.inbitcoin.globular.data.network.NetworkService;
import it.inbitcoin.globular.ui.channeldetails.ChannelDetailsActivity;
import it.inbitcoin.globular.ui.openchannel.OpenChannelActivity;
import it.inbitcoin.globular.ui.scan.ScanActivity;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.Channel;
import it.inbitcoin.globular.utils.IncomingHandler;
import it.inbitcoin.globular.utils.IncomingHandlerCallback;
import it.inbitcoin.globular.utils.NetworkServiceConnection;
import it.inbitcoin.globular.utils.TimeoutUtil;

public class ChannelsActivity extends DaggerAppCompatActivity
        implements ChannelsRecyclerViewAdapter.OnChannelClickListener {

    private static final String TAG = "ChannelsActivity";

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private ChannelsViewModel mViewModel;

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    TimeoutUtil mTimeoutUtil;

    private Toolbar mToolbar;
    private ProgressBar mProgressBar;

    private CoordinatorLayout activityLayout;

    private Looper activityLooper;
    private IncomingHandler incomingHandler;
    private final NetworkServiceConnection mConnection = new NetworkServiceConnection();

    private ChannelsRecyclerViewAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private FloatingActionButton mOpenChannelButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_channels);

        activityLayout = findViewById(R.id.channels_coordinator_layout);

        mToolbar = findViewById(R.id.toolbar_channels);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mProgressBar = findViewById(R.id.pb_channels);

        mRecyclerView = findViewById(R.id.list_channels);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ChannelsRecyclerViewAdapter(new ArrayList<>());
        mSwipeRefreshLayout = findViewById(R.id.swiperefresh_channels);
        mRecyclerView.setAdapter(mAdapter);

        final DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                this, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(Objects.requireNonNull(ResourcesCompat.getDrawable(
                getResources(), R.drawable.divider, null)));
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        mOpenChannelButton = findViewById(R.id.btn_open_channel);

        activityLooper = Looper.myLooper();
        incomingHandler = new IncomingHandler(
                activityLooper, this, activityLayout, new ChannelsHandlerCallback());
        mConnection.mMessenger = new Messenger(incomingHandler);

        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(ChannelsViewModel.class);

        setListeners();
    }

    SingleObserver<AppRepository.ChannelsResponse> channelsObserver =
            new SingleObserver<AppRepository.ChannelsResponse>() {
        @Override
        public void onSubscribe(Disposable d) {
            mSwipeRefreshLayout.setRefreshing(false);
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onSuccess(AppRepository.ChannelsResponse channelsResponse) {
            updateChannels(channelsResponse);
            mProgressBar.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onError(Throwable e) {
            showError();
        }
    };

    private void setListeners() {
        mAdapter.setClickListener(this);
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            Log.d(TAG, "swipe refresh");
            mProgressBar.setVisibility(View.VISIBLE);
            mViewModel.getChannelsData().observeOn(AndroidSchedulers.mainThread())
                    .subscribe(channelsObserver);
        });
        mOpenChannelButton.setOnClickListener(v -> openScanner());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!AppUtils.isTimedOut(mAccessFactory, mTimeoutUtil, this))
            doBindService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mTimeoutUtil.updatePin();
        doUnbindService();
    }

    private void showError() {
        Toast.makeText(this, R.string.error, Toast.LENGTH_LONG).show();
        mAdapter.clearList();
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    private void openScanner() {
        new IntentIntegrator(this)
                .setBeepEnabled(false).setOrientationLocked(true)
                .setDesiredBarcodeFormats(IntentIntegrator.QR_CODE).setCameraId(0)
                .setCaptureActivity(ScanActivity.class)
                .initiateScan();
    }

    private void updateChannels(AppRepository.ChannelsResponse channelsResponse) {
        GeneratedMessageLite listChannelsMsg = channelsResponse.listChannelsResponse.getMessage();
        GeneratedMessageLite listPeersMsg = channelsResponse.listPeersResponse.getMessage();

        if (listChannelsMsg == null)
            showError();

        List<Channel> tmpChannels = new ArrayList<>();
        ListChannelsResponse channelsRes = (ListChannelsResponse) listChannelsMsg;
        List<it.inbitcoin.globular.Channel> channelsList = new ArrayList<>();
        if (channelsRes != null)
            channelsList = channelsRes.getChannelsList();

        ListPeersResponse peersRes = (ListPeersResponse) listPeersMsg;
        List<Peer> peersList = new ArrayList<>();
        if (peersRes != null)
            peersList = peersRes.getPeersList();

        ArrayMap<String, Pair<String, String>> tmpPeers = new ArrayMap<>();
        for (Peer peer : peersList)
            tmpPeers.put(peer.getPubkey(), new Pair<>(peer.getAlias(), peer.getColor()));


        for (it.inbitcoin.globular.Channel grpcChannel : channelsList) {
            Channel channel = new Channel();
            String remotePubkey = grpcChannel.getRemotePubkey();
            channel.setRemotePubkey(remotePubkey);
            channel.setShortChannelId(grpcChannel.getShortChannelId());
            channel.setChannelId(grpcChannel.getChannelId());
            channel.setFundingTxid(grpcChannel.getFundingTxid());
            channel.setCapacity(grpcChannel.getCapacity());
            channel.setLocalBalance(grpcChannel.getLocalBalance());
            channel.setRemoteBalance(grpcChannel.getRemoteBalance());
            channel.setToSelfDelay(grpcChannel.getToSelfDelay());
            channel.setChannelState(grpcChannel.getStateValue());

            if (tmpPeers.containsKey(remotePubkey)) {
                channel.setAlias(Objects.requireNonNull(tmpPeers.get(remotePubkey)).first);
                channel.setColor(Objects.requireNonNull(tmpPeers.get(remotePubkey)).second);
            }

            tmpChannels.add(channel);
        }
        mAdapter.updateChannels(tmpChannels);
    }

    @Override
    public void onChannelClick(View view, int position) {
        showChannelDetailsActivity(mAdapter.getChannel(position));
    }

    private void showChannelDetailsActivity(Channel channel) {
        Log.d(TAG, "channel clicked: " + channel);
        Intent channelIntent = new Intent(this, ChannelDetailsActivity.class);
        channelIntent.putExtra(AppConstants.channel, channel);
        startActivity(channelIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d(TAG, "Request code: " + requestCode + ", result code: " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        String nodeUri = AppUtils.getDataFromScan(requestCode, resultCode, data, this);
        if (nodeUri.isEmpty()) {
            AppUtils.cancelRequest(this);
            return;
        }
        showOpenChannelActivity(nodeUri);
    }

    private void showOpenChannelActivity(String nodeUri) {
        Intent intent = new Intent(ChannelsActivity.this, OpenChannelActivity.class);
        intent.putExtra(AppConstants.nodeUri, nodeUri);
        startActivity(intent);
    }

    void doBindService() {
        bindService(new Intent(ChannelsActivity.this, NetworkService.class),
                mConnection, Context.BIND_AUTO_CREATE);
        mConnection.mIsBound = true;
    }

    void doUnbindService() {
        if (mConnection.mIsBound) {
            if (mConnection.mMessengerService != null) {
                try {
                    Message msg = Message.obtain(null, NetworkService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mConnection.mMessenger;
                    mConnection.mMessengerService.send(msg);
                } catch (RemoteException ignored) { }
            }
            unbindService(mConnection);
            mConnection.mIsBound = false;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    class ChannelsHandlerCallback implements IncomingHandlerCallback {

        @Override
        public void handleView(Message msg) {
            if (msg.what == NetworkService.MSG_LAST_STATE &&
                    msg.arg2 == NetworkService.CONNECTION_OK) {
                mOpenChannelButton.setEnabled(true);
                mViewModel.getChannelsData().observeOn(AndroidSchedulers.mainThread())
                        .subscribe(channelsObserver);
            }
        }
    }

}
