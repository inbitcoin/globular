/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.about;

import android.os.Bundle;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.text.HtmlCompat;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.TimeoutUtil;

public class AboutActivity extends DaggerAppCompatActivity {

    @Inject
    TimeoutUtil mTimeoutUtil;

    @Inject
    AccessFactory mAccessFactory;

    private Toolbar mToolbar;

    private TextView versionTextView;
    private TextView aboutGlobularTextView;
    private TextView aboutLighterTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        mToolbar = findViewById(R.id.toolbar_about);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        versionTextView = findViewById(R.id.tv_about_app_version);
        String version = AppUtils.getAppVersion(this);
        versionTextView.setText(version);

        aboutGlobularTextView = findViewById(R.id.tv_about_globular_link);
        Spanned globularUrl = HtmlCompat.fromHtml(
                getString(R.string.about_globular_url), HtmlCompat.FROM_HTML_MODE_LEGACY);
        aboutGlobularTextView.setText(globularUrl);
        aboutGlobularTextView.setMovementMethod(LinkMovementMethod.getInstance());

        aboutLighterTextView = findViewById(R.id.tv_about_lighter_link);
        Spanned lighterUrl = HtmlCompat.fromHtml(
                getString(R.string.about_lighter_url), HtmlCompat.FROM_HTML_MODE_LEGACY);
        aboutLighterTextView.setText(lighterUrl);
        aboutLighterTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppUtils.isTimedOut(mAccessFactory, mTimeoutUtil, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mTimeoutUtil.updatePin();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
