/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.nodeinfo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.ShareActionProvider;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import it.inbitcoin.globular.GetInfoResponse;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.model.GrpcResponse;
import it.inbitcoin.globular.data.network.RestInterface;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.TimeoutUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NodeInfoActivity extends DaggerAppCompatActivity {

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private NodeInfoViewModel mViewModel;

    @Inject
    TimeoutUtil mTimeoutUtil;

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    public RestInterface restInterface;

    private Context mContext;

    private Toolbar mToolbar;
    private ProgressBar mProgressBar;
    private TextView mNodeUriTextView;
    private ImageView mNodeUriQrImage;

    private String mNodeUri;

    private MenuItem mShareMenuItem;
    private boolean noShare = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node_info);

        mContext = this;

        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(NodeInfoViewModel.class);

        mToolbar = findViewById(R.id.toolbar_about);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mProgressBar = findViewById(R.id.pb_node_info);

        mNodeUriTextView = findViewById(R.id.tv_node_uri);

        mNodeUriQrImage = findViewById(R.id.iv_node_uri_qr);
        mNodeUriQrImage.setVisibility(View.GONE);

        mViewModel.getInfo().observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<GrpcResponse>() {
            @Override
            public void onSubscribe(Disposable d) {
                mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(GrpcResponse grpcResponse) {
                parseGetInfo(grpcResponse);
            }

            @Override
            public void onError(Throwable e) {
                mProgressBar.setVisibility(View.INVISIBLE);
            }
        });

        setListeners();
    }

    private void setListeners() {
        mNodeUriQrImage.setOnClickListener(v -> {
            Intent sharingIntent = AppUtils.getShareIntent(
                    this, mNodeUri, AppConstants.qrNodeUri);
            startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_via)));
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppUtils.isTimedOut(mAccessFactory, mTimeoutUtil, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mTimeoutUtil.updatePin();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share, menu);
        mShareMenuItem = menu.findItem(R.id.menu_item_share);
        ShareActionProvider shareActionProvider =
                (ShareActionProvider) MenuItemCompat.getActionProvider(mShareMenuItem);
        shareActionProvider.setShareIntent(
                AppUtils.getShareIntent(this, mNodeUri, AppConstants.qrNodeUri));
        return true;
    }

    private void parseGetInfo(GrpcResponse grpcResponse) {
        if (grpcResponse.getMessage() == null) {
            mProgressBar.setVisibility(View.INVISIBLE);
            return;
        }
        GetInfoResponse response = (GetInfoResponse) grpcResponse.getMessage();

        mNodeUri = response.getNodeUri();
        mNodeUriTextView.setText(mNodeUri);
        showNodeUriQr(mNodeUri);
    }

    private void showNodeUriQr(String nodeUri) {
        restInterface.getQrcode(nodeUri).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call,
                                   @NonNull Response<ResponseBody> response) {
                if (response.body() != null) {
                    Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                    AppUtils.saveImage(mContext, bmp, AppConstants.qrNodeUri);
                    mNodeUriQrImage.setImageBitmap(bmp);
                    noShare = false;
                    invalidateOptionsMenu();
                } else {
                    mNodeUriQrImage.setImageResource(R.drawable.img_qrcat);
                    noShare = true;
                    invalidateOptionsMenu();
                }
                mNodeUriQrImage.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                mNodeUriQrImage.setImageResource(R.drawable.img_qrcat);
                mNodeUriQrImage.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.INVISIBLE);
            }
        });

    }

}
