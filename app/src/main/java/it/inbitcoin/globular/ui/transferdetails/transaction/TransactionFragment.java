/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.transferdetails.transaction;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.ui.transferdetails.TransferDetailsActivity;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.Transfer;

public class TransactionFragment extends DaggerFragment {

    private TransferDetailsActivity parentActivity;

    private Transfer mTransfer;

    private TextView mTxidTextView;
    private TextView mAmountTextView;
    private TextView mFeeTextView;
    private TextView mStatusTextView;
    private TextView mTimestampTextView;
    private TextView mDestAddressesTextView;
    private TextView mNumConfirmations;
    private TextView mBlockHash;
    private TextView mBlockHeight;


    @Inject
    public TransactionFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentActivity = (TransferDetailsActivity) getActivity();
        assert parentActivity != null;
        Intent intent = parentActivity.getIntent();
        if (intent != null && intent.hasExtra(AppConstants.transfer)) {
            Bundle extras = intent.getExtras();
            if (extras != null)
                mTransfer = extras.getParcelable(AppConstants.transfer);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_transaction, container, false);

        mTxidTextView = root.findViewById(R.id.tv_txid);
        mAmountTextView = root.findViewById(R.id.tv_amount_transaction);
        mFeeTextView = root.findViewById(R.id.tv_fee);
        mStatusTextView = root.findViewById(R.id.tv_status);
        mTimestampTextView = root.findViewById(R.id.tv_timestamp);
        mDestAddressesTextView = root.findViewById(R.id.tv_dest_addresses);
        mNumConfirmations = root.findViewById(R.id.tv_num_confirmations);
        mBlockHash = root.findViewById(R.id.tv_block_hash);
        mBlockHeight = root.findViewById(R.id.tv_block_height);

        showTransaction();
        return root;
    }

    private void showTransaction() {
        String txid = mTransfer.getDescription();
        if (!TextUtils.isEmpty(txid)) mTxidTextView.setText(txid);

        double amount = mTransfer.getAmount();
        if (amount != 0) mAmountTextView.setText(AppUtils.formatNumber(String.valueOf(amount), 5));

        long feeSat = mTransfer.getFeeSat();
        if (feeSat > 0) mFeeTextView.setText(String.valueOf(feeSat));

        long timestamp = mTransfer.getTimestamp();
        if (timestamp > 0) mTimestampTextView.setText(AppUtils.getIsoDate(timestamp));

        String status = mTransfer.getStatusString(getContext());
        if (!TextUtils.isEmpty(status)) mStatusTextView.setText(status);

        List<String> destAddresses = mTransfer.getDestAddresses();
        if (destAddresses != null) mDestAddressesTextView.setText(
                TextUtils.join("\n\n", mTransfer.getDestAddresses()));

        int numConfirmations = mTransfer.getNumConfirmations();
        mNumConfirmations.setText(String.valueOf(numConfirmations));

        String blockHash = mTransfer.getBlockHash();
        if (!TextUtils.isEmpty(blockHash)) mBlockHash.setText(blockHash);

        int blockHeight = mTransfer.getBlockHeight();
        if (blockHeight > 0) mBlockHeight.setText(String.valueOf(blockHeight));
    }

}
