/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.send;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.zxing.integration.android.IntentIntegrator;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.network.NetworkService;
import it.inbitcoin.globular.ui.scan.ScanActivity;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.IncomingHandler;
import it.inbitcoin.globular.utils.NetworkServiceConnection;
import it.inbitcoin.globular.utils.TimeoutUtil;

public class SendActivity extends DaggerAppCompatActivity {

    private static final String TAG = "SendActivity";

    @Inject
    SendOffChainFragment mSendOffChainFragment;

    @Inject
    SendOnChainFragment mSendOnChainFragment;

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    TimeoutUtil mTimeoutUtil;

    private final int ONCHAIN = 0;
    private final int OFFCHAIN = 1;
    private final int INCORRECT = 3;

    private CoordinatorLayout activityLayout;
    private FrameLayout privateLayout;

    private Looper activityLooper;
    private IncomingHandler incomingHandler;

    private Toolbar mToolbar;

    private final NetworkServiceConnection mConnection = new NetworkServiceConnection();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        if (Objects.requireNonNull(getIntent().getExtras()).getBoolean(AppConstants.viaMenu)) {
            new IntentIntegrator(this)
                    .setBeepEnabled(false).setOrientationLocked(true).setPrompt("")
                    .setDesiredBarcodeFormats(IntentIntegrator.QR_CODE).setCameraId(0)
                    .setCaptureActivity(ScanActivity.class)
                    .initiateScan();
        }

        setContentView(R.layout.activity_send);

        activityLayout = findViewById(R.id.send_coordinator_layout);
        privateLayout = findViewById(R.id.fragment_send);

        mToolbar = findViewById(R.id.toolbar_send);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        activityLooper = Looper.myLooper();
        incomingHandler = new IncomingHandler(
                activityLooper, this, activityLayout, null);
        mConnection.mMessenger = new Messenger(incomingHandler);
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        if (!AppUtils.isTimedOut(mAccessFactory, mTimeoutUtil,this)) {
            privateLayout.setVisibility(View.VISIBLE);
            doBindService();
        }
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
        mTimeoutUtil.updatePin();
        privateLayout.setVisibility(View.GONE);
        doUnbindService();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    private int detectPaymentRequestType(String paymentRequest) {
        if (paymentRequest.toLowerCase().startsWith("ln")) return OFFCHAIN;
        if (paymentRequest.toLowerCase().startsWith(AppConstants.uriSchemeLightning)) return OFFCHAIN;
        if (paymentRequest.toLowerCase().startsWith(AppConstants.uriSchemeBitcoin)) return ONCHAIN;
        List<String> bitcoinPrefixes = Arrays.asList("1", "3", "bc1", "BC1", "m", "n", "2", "tb1", "TB1");
        for (String prefix : bitcoinPrefixes) {
            if (paymentRequest.startsWith(prefix)) return ONCHAIN;
        }
        return INCORRECT;
    }

    private void checkInvoice(String paymentRequest) {
        if (paymentRequest != null) {
            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.paymentRequest, paymentRequest);
            switch (detectPaymentRequestType(paymentRequest)) {
                case OFFCHAIN:
                    showOffChainFragment(bundle);
                    break;
                case ONCHAIN:
                    showOnChainFragment(bundle);
                    break;
                case INCORRECT:
                    Toast.makeText(this,
                            R.string.incorrect_pay_req, Toast.LENGTH_LONG).show();
                    Log.d(TAG, "incorrect payment request");
                    finish();
                    break;
                default:
                    break;
            }
        }
    }

    private void showOffChainFragment(Bundle bundle){
        mSendOffChainFragment.setArguments(bundle);
        AppUtils.addFragmentToActivity(
                getSupportFragmentManager(), mSendOffChainFragment, R.id.fragment_send,
                false, false);
    }

    private void showOnChainFragment(Bundle bundle){
        mSendOnChainFragment.setArguments(bundle);
        AppUtils.addFragmentToActivity(
                getSupportFragmentManager(), mSendOnChainFragment, R.id.fragment_send,
                false, false);
    }

    void doBindService() {
        bindService(new Intent(SendActivity.this, NetworkService.class),
                mConnection, Context.BIND_AUTO_CREATE);
        mConnection.mIsBound = true;
    }

    void doUnbindService() {
        if (mConnection.mIsBound) {
            if (mConnection.mMessengerService != null) {
                try {
                    Message msg = Message.obtain(null, NetworkService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mConnection.mMessenger;
                    mConnection.mMessengerService.send(msg);
                } catch (RemoteException ignored) {}
            }
            unbindService(mConnection);
            mConnection.mIsBound = false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "Request code: " + requestCode + ", result code: " + resultCode);

        String paymentRequest = AppUtils.getDataFromScan(
                requestCode, resultCode, data, this);

        if (paymentRequest.isEmpty()) {
            AppUtils.cancelRequest(this);
            finish();
            return;
        }

        checkInvoice(paymentRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
