/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.network.NetworkService;
import it.inbitcoin.globular.data.prefs.PreferencesHelper;
import it.inbitcoin.globular.ui.landing.LandingActivity;
import it.inbitcoin.globular.ui.main.MainActivity;
import it.inbitcoin.globular.ui.pinentry.PinEntryActivity;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.NetworkServiceConnection;
import it.inbitcoin.globular.utils.TimeoutUtil;

public class LoginActivity extends DaggerAppCompatActivity {

    private static final String TAG = "LoginActivity";

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    TimeoutUtil mTimeoutUtil;

    @Inject
    PreferencesHelper mPreferencesHelper;

    private static final int REQUEST_LANDING = 2;
    public static final int LOGIN_EXIT_COMMAND = 3;
    private static final int REQUEST_MAIN = 4;
    private static final int REQUEST_PIN_ENTRY = 5;

    private final NetworkServiceConnection mConnection = new NetworkServiceConnection();

    private ConstraintLayout activityLayout;

    private ProgressBar mProgressBar;

    private Handler incomingHandler;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        activityLayout = findViewById(R.id.cl_constraint);
        incomingHandler = new Handler();
        mConnection.mMessenger = new Messenger(incomingHandler);

        mProgressBar = findViewById(R.id.pb_login);
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        mProgressBar.setVisibility(View.VISIBLE);
        checkAccessState();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
        doUnbindService();
    }

    private void checkAccessState() {
        boolean firstAccess = mPreferencesHelper.getAccessToken().isEmpty();
        boolean hasTimedOut = mTimeoutUtil.isTimedOut();
        boolean isLoggedIn = mAccessFactory.isLoggedIn();
        boolean hasAccess = isLoggedIn && !hasTimedOut;
        if (firstAccess) {
            Log.d(TAG, "firstAccess");
            mAccessFactory.setIsLoggedIn(false);
            showLandingActivity();
        } else if (hasAccess) {
            Log.d(TAG, "hasAccess");
            if (!mConnection.mIsBound)
                doBindService();
            mTimeoutUtil.updatePin();
            showMainActivity();
        } else {
            Log.d(TAG, "hasTimedOut: " + hasTimedOut + " isLoggedIn: " + isLoggedIn);
            mAccessFactory.setIsLoggedIn(false);
            mAccessFactory.resetAccessKey();
            showPinEntryActivity();
        }

    }

    private void showMainActivity() {
        Log.d(TAG, "showing MainActivity");
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtra(AppConstants.requestKey, PinEntryActivity.FROM_LOGIN);
        startActivityForResult(intent, REQUEST_MAIN);
    }

    private void showPinEntryActivity() {
        Log.d(TAG, "showing PinEntryActivity");
        Intent intent = new Intent(LoginActivity.this, PinEntryActivity.class);
        intent.putExtra(AppConstants.requestKey, PinEntryActivity.FROM_LOGIN);
        startActivityForResult(intent, REQUEST_PIN_ENTRY);
    }

    private void showLandingActivity() {
        Intent intent = new Intent(LoginActivity.this, LandingActivity.class);
        startActivityForResult(intent, REQUEST_LANDING);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult " + requestCode + " " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_MAIN && resultCode == LOGIN_EXIT_COMMAND) {
            Log.d(TAG, "finish from main");
            finish();
        }
        if (requestCode == REQUEST_PIN_ENTRY && resultCode == LOGIN_EXIT_COMMAND) {
            Log.d(TAG, "finish from pin entry");
            finish();
        }
        if (requestCode == REQUEST_LANDING && resultCode == LOGIN_EXIT_COMMAND)
            finish();
    }

    private void doBindService() {
        Intent intent = new Intent(LoginActivity.this, NetworkService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        mConnection.mIsBound = true;
    }

    private void doUnbindService() {
        if (mConnection.mIsBound) {
            if (mConnection.mMessengerService != null) {
                try {
                    Message msg = Message.obtain(null, NetworkService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mConnection.mMessenger;
                    mConnection.mMessengerService.send(msg);
                } catch (RemoteException ignored) {}
            }
            unbindService(mConnection);
            mConnection.mIsBound = false;
        }
    }

}
