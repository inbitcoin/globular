/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.ProgressBar;

import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.network.NetworkService;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.IncomingHandler;
import it.inbitcoin.globular.utils.NetworkServiceConnection;
import it.inbitcoin.globular.utils.TimeoutUtil;

import static it.inbitcoin.globular.utils.AppUtils.addFragmentToActivity;

public class SettingsActivity extends DaggerAppCompatActivity {

    private static final String TAG = "SettingsActivity";

    @Inject
    SettingsFragment mSettingsFragment;

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    TimeoutUtil mTimeoutUtil;

    private CoordinatorLayout activityLayout;

    private Looper activityLooper;
    private IncomingHandler incomingHandler;

    private Toolbar mToolbar;

    public final NetworkServiceConnection mConnection = new NetworkServiceConnection();

    public ProgressBar mProgressBar;

    private boolean fragmentsAttached = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        activityLayout = findViewById(R.id.settings_container);

        mToolbar = findViewById(R.id.toolbar_preference);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        activityLooper = Looper.myLooper();
        incomingHandler = new IncomingHandler(
                activityLooper, this, activityLayout, null);
        mConnection.mMessenger = new Messenger(incomingHandler);

        mProgressBar = findViewById(R.id.pb_settings);

        this.showFragment();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (!AppUtils.isTimedOut(mAccessFactory, mTimeoutUtil, this))
            doBindService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        mTimeoutUtil.updatePin();
        doUnbindService();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    private void showFragment() {
        if (!fragmentsAttached) {
            Log.d(TAG, "showing fragment");
            addFragmentToActivity(
                    getSupportFragmentManager(), mSettingsFragment, R.id.fragment_preference,
                    false, false);
            fragmentsAttached = true;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void doBindService() {
        Intent intent = new Intent(SettingsActivity.this, NetworkService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        mConnection.mIsBound = true;
    }

    void doUnbindService() {
        if (mConnection.mIsBound) {
            if (mConnection.mMessengerService != null) {
                try {
                    Message msg = Message.obtain(null, NetworkService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mConnection.mMessenger;
                    mConnection.mMessengerService.send(msg);
                } catch (RemoteException ignored) {}
            }
            unbindService(mConnection);
            mConnection.mIsBound = false;
        }
    }

}
