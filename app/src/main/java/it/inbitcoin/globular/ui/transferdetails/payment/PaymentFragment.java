/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.transferdetails.payment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.ui.transferdetails.TransferDetailsActivity;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.Transfer;

public class PaymentFragment extends DaggerFragment {

    private TransferDetailsActivity parentActivity;

    private Transfer mTransfer;

    private TextView mAmountTextView;
    private TextView mPaymentPreimageTextView;
    private TextView mTimestampTextView;
    private TextView mFeeTextView;
    private TextView mStatusTextView;


    @Inject
    public PaymentFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentActivity = (TransferDetailsActivity) getActivity();
        assert parentActivity != null;
        Bundle extras = parentActivity.getIntent().getExtras();
        if (extras != null)
            mTransfer = extras.getParcelable(AppConstants.transfer);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_payment, container, false);
        mAmountTextView = root.findViewById(R.id.tv_amount_payment);
        mPaymentPreimageTextView = root.findViewById(R.id.tv_preimage);
        mTimestampTextView = root.findViewById(R.id.tv_timestamp);
        mFeeTextView = root.findViewById(R.id.tv_fee);
        mStatusTextView = root.findViewById(R.id.tv_status);
        showPayment();
        return root;
    }

    private void showPayment() {
        String description = mTransfer.getDescription();
        if (description != null) mPaymentPreimageTextView.setText(description);

        double amount = mTransfer.getAmount();
        if (amount != 0) mAmountTextView.setText(AppUtils.formatNumber(String.valueOf(amount), 5));

        long timestamp = mTransfer.getTimestamp();
        if (timestamp > 0) mTimestampTextView.setText(AppUtils.getIsoDate(timestamp));


        long feeBaseMsat = mTransfer.getFeeBaseMsat();
        if (feeBaseMsat > 0) mFeeTextView.setText(String.valueOf(feeBaseMsat));

        String status = mTransfer.getStatusString(getContext());
        if (!TextUtils.isEmpty(status)) mStatusTextView.setText(status);
    }

}
