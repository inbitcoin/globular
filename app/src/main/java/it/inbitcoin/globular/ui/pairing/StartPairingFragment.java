/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.pairing;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.utils.AppConstants;

public class StartPairingFragment extends DaggerFragment {

    private static final String TAG = StartPairingFragment.class.getSimpleName();

    private PairingActivity parentActivity;

    private LinearLayout startButton;
    private LinearLayout abortButton;
    private TextView abortTextView;

    private onStartPairingListener startPairingListener;


    @Inject
    public StartPairingFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            startPairingListener = (onStartPairingListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    parentActivity.toString() + " must implement onStartPairingListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        parentActivity = (PairingActivity) getActivity();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(
                    R.layout.fragment_start_pairing, container, false);
        startButton = root.findViewById(R.id.btn_start);
        abortButton = root.findViewById(R.id.btn_abort);
        abortTextView = root.findViewById(R.id.tv_btn_abort);
        Bundle extras = parentActivity.getIntent().getExtras();
        if (extras != null && extras.getBoolean(AppConstants.viaPinCreate))
            abortTextView.setText(R.string.skip);
        setListeners();
        return root;
    }

    private void setListeners() {
        abortButton.setOnClickListener(v -> parentActivity.finish());
        startButton.setOnClickListener(v -> {
            startPairingListener.startPairing();
        });
    }

    public interface onStartPairingListener {
        void startPairing();
    }

}
