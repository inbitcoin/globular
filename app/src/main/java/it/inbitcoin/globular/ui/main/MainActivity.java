/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import net.i2p.android.ext.floatingactionbutton.FloatingActionButton;
import net.i2p.android.ext.floatingactionbutton.FloatingActionsMenu;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import it.inbitcoin.globular.ChannelBalanceResponse;
import it.inbitcoin.globular.GetInfoResponse;
import it.inbitcoin.globular.Invoice;
import it.inbitcoin.globular.ListInvoicesResponse;
import it.inbitcoin.globular.ListPaymentsResponse;
import it.inbitcoin.globular.ListTransactionsResponse;
import it.inbitcoin.globular.Payment;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.Transaction;
import it.inbitcoin.globular.WalletBalanceResponse;
import it.inbitcoin.globular.data.AppRepository;
import it.inbitcoin.globular.data.model.GrpcResponse;
import it.inbitcoin.globular.data.network.NetworkService;
import it.inbitcoin.globular.data.prefs.PreferencesHelper;
import it.inbitcoin.globular.ui.about.AboutActivity;
import it.inbitcoin.globular.ui.channels.ChannelsActivity;
import it.inbitcoin.globular.ui.login.LoginActivity;
import it.inbitcoin.globular.ui.nodeinfo.NodeInfoActivity;
import it.inbitcoin.globular.ui.receive.ReceiveActivity;
import it.inbitcoin.globular.ui.send.SendActivity;
import it.inbitcoin.globular.ui.settings.SettingsActivity;
import it.inbitcoin.globular.ui.transferdetails.TransferDetailsActivity;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.ApiEndPoint;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.IncomingHandler;
import it.inbitcoin.globular.utils.IncomingHandlerCallback;
import it.inbitcoin.globular.utils.NetworkServiceConnection;
import it.inbitcoin.globular.utils.TimeoutUtil;
import it.inbitcoin.globular.utils.Transfer;

public class MainActivity extends DaggerAppCompatActivity
        implements TransfersRecyclerViewAdapter.OnClickTransferListener {

    private static final String TAG = "MainActivity";

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private MainViewModel mViewModel;

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    TimeoutUtil mTimeoutUtil;

    @Inject
    PreferencesHelper mPreferencesHelper;

    @Inject
    ApiEndPoint mApiEndPoint;

    private CoordinatorLayout activityLayout;
    private LinearLayout privateLayout;

    private Looper activityLooper;
    private MainIncomingHandler incomingHandler;

    private Toolbar mToolbar;

    private TextView mChannelBalanceTextView;
    private TextView mWalletBalanceTextView;

    private ProgressBar mProgressBar;

    private TransfersRecyclerViewAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private FloatingActionsMenu mFabMenu;
    private FloatingActionButton mSendFab;
    private FloatingActionButton mReceiveFab;

    private final NetworkServiceConnection mConnection = new NetworkServiceConnection();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_main);

        activityLayout = findViewById(R.id.main_coordinator_layout);
        privateLayout  = findViewById(R.id.main_private_layout);

        mToolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(mToolbar);

        mChannelBalanceTextView = findViewById(R.id.tv_channel_balance);
        mWalletBalanceTextView = findViewById(R.id.tv_wallet_balance);

        mProgressBar = findViewById(R.id.pb_main);

        mRecyclerView = findViewById(R.id.list_main);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new TransfersRecyclerViewAdapter(new ArrayList<>());
        mSwipeRefreshLayout = findViewById(R.id.swiperefresh_main);
        mRecyclerView.setAdapter(mAdapter);

        final DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                this, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(Objects.requireNonNull(ResourcesCompat.getDrawable(
                getResources(), R.drawable.divider, null)));
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        mFabMenu = findViewById(R.id.wallet_menu);
        mSendFab = findViewById(R.id.fab_send);
        mReceiveFab = findViewById(R.id.fab_receive);

        activityLooper = Looper.myLooper();
        incomingHandler = new MainIncomingHandler(
                activityLooper, this, activityLayout, new MainHandlerCallback());
        mConnection.mMessenger = new Messenger(incomingHandler);

        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(MainViewModel.class);

        mViewModel.updateLighterCredentials();

        setListeners();
    }

    private void setListeners() {
        mAdapter.setClickListener(this);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mFabMenu.collapse();
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            Log.d(TAG, "swipe refresh");
            mProgressBar.setVisibility(View.VISIBLE);
            mViewModel.getMainData().observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<AppRepository.MergedResponse>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }

                        @Override
                        public void onSuccess(AppRepository.MergedResponse mergedResponse) {
                            updateMain(mergedResponse);
                            mProgressBar.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onError(Throwable e) {
                            mProgressBar.setVisibility(View.INVISIBLE);
                        }
                    });
        });
        mSendFab.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this,  SendActivity.class);
            intent.putExtra(AppConstants.viaMenu, true);
            startActivity(intent);
            mFabMenu.collapse();
        });
        mReceiveFab.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, ReceiveActivity.class);
            intent.putExtra(AppConstants.viaMenu, true);
            startActivity(intent);
            mFabMenu.collapse();
        });
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        if (!AppUtils.isTimedOut(mAccessFactory, mTimeoutUtil, this)) {
            privateLayout.setVisibility(View.VISIBLE);
            doBindService();
        }
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
        mTimeoutUtil.updatePin();
        privateLayout.setVisibility(View.GONE);
        doUnbindService();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.channels_menu:
                startActivity(new Intent(MainActivity.this, ChannelsActivity.class));
                return true;
            case R.id.node_info_menu:
                startActivity(new Intent(MainActivity.this, NodeInfoActivity.class));
                return true;
            case R.id.settings_menu:
                Intent preferenceIntent = new Intent(MainActivity.this,
                        SettingsActivity.class);
                preferenceIntent.putExtra(AppConstants.viaMenu, true);
                startActivity(preferenceIntent);
                return true;
            case R.id.about_menu:
                startActivity(new Intent(MainActivity.this, AboutActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void transferDetails(Transfer transfer) {
        Log.d(TAG, "transfer clicked: " + transfer);
        Intent transferIntent = new Intent(this, TransferDetailsActivity.class);
        transferIntent.putExtra(AppConstants.transfer, transfer);
        startActivity(transferIntent);
    }

    private void parseGetInfo(GrpcResponse grpcResponse) {
        if (grpcResponse.getMessage() == null) {
            clearMain();
            return;
        }
        GetInfoResponse response = (GetInfoResponse) grpcResponse.getMessage();
        updateNetwork(response.getNetwork());
    }

    private void parseInvoices(GrpcResponse grpcResponse) {
        List<Transfer> tmpInvoices = new ArrayList<>();
        if (grpcResponse.getMessage() == null) {
            mAdapter.updateTransfers(tmpInvoices, Transfer.INVOICES);
            return;
        }
        ListInvoicesResponse response = (ListInvoicesResponse) grpcResponse.getMessage();
        List<Invoice> invoicesList = response.getInvoicesList();
        for (int i = 0; i < response.getInvoicesCount(); i++) {
            Transfer transfer = new Transfer();
            Invoice invoice = invoicesList.get(i);
            transfer.setTransferType(Transfer.INVOICES);
            transfer.setPaymentRequest(invoice.getPaymentRequest());
            transfer.setAmount(invoice.getAmountBits());
            transfer.setDescription(invoice.getDescription());
            if (invoice.getDescription().isEmpty())
                transfer.setDescription(getString(R.string.no_description));
            transfer.setStatus(invoice.getStateValue());
            transfer.setTimestamp(invoice.getTimestamp());
            transfer.setExpiryTime(invoice.getExpiryTime());
            tmpInvoices.add(i, transfer);
        }
        Log.d(TAG, "num invoices arrived " + response.getInvoicesCount());
        mAdapter.updateTransfers(tmpInvoices, Transfer.INVOICES);
    }

    private void parsePayments(GrpcResponse grpcResponse) {
        List<Transfer> tmpPayments = new ArrayList<>();
        if (grpcResponse.getMessage() == null) {
            mAdapter.updateTransfers(tmpPayments, Transfer.PAYMENTS);
            return;
        }
        ListPaymentsResponse response = (ListPaymentsResponse) grpcResponse.getMessage();
        List<Payment> paymentsList = response.getPaymentsList();
        for (int i = 0; i < response.getPaymentsCount(); i++) {
            Transfer transfer = new Transfer();
            Payment payment = paymentsList.get(i);
            transfer.setTransferType(Transfer.PAYMENTS);
            transfer.setAmount(0 - payment.getAmountBits());
            transfer.setDescription(payment.getPaymentPreimage());
            transfer.setTimestamp(payment.getTimestamp());
            transfer.setFeeBaseMsat(payment.getFeeBaseMsat());
            tmpPayments.add(i, transfer);
        }
        Log.d(TAG, "num payments arrived " + response.getPaymentsCount());
        mAdapter.updateTransfers(tmpPayments, Transfer.PAYMENTS);
    }

    private void parseTransactions(GrpcResponse grpcResponse) {
        List<Transfer> tmpTransactions = new ArrayList<>();
        if (grpcResponse.getMessage() == null) {
            mAdapter.updateTransfers(tmpTransactions, Transfer.TRANSACTIONS);
            return;
        }
        ListTransactionsResponse response = (ListTransactionsResponse) grpcResponse.getMessage();
        List<Transaction> transactionsList = response.getTransactionsList();
        for (int i=0; i < response.getTransactionsCount(); i++) {
            Transfer transfer = new Transfer();
            Transaction transaction = transactionsList.get(i);
            transfer.setTransferType(Transfer.TRANSACTIONS);
            transfer.setDescription(transaction.getTxid());
            transfer.setAmount(transaction.getAmountBits());
            transfer.setTimestamp(transaction.getTimestamp());
            transfer.setDestAddresses(transaction.getDestAddressesList());
            transfer.setNumConfirmations(transaction.getNumConfirmations());
            transfer.setBlockHash(transaction.getBlockHash());
            transfer.setBlockHeight(transaction.getBlockheight());
            transfer.setFeeSat(transaction.getFeeSat());
            tmpTransactions.add(i, transfer);
        }
        Log.d(TAG, "num transactions arrived " + response.getTransactionsCount());
        mAdapter.updateTransfers(tmpTransactions, Transfer.TRANSACTIONS);
    }

    private void updateNetwork(String network) {
        if (mPreferencesHelper.getLighterDemoMode())
            if (network.isEmpty())
                network += "- " + getString(R.string.demo_mode_title);
            else
                network += " - " + getString(R.string.demo_mode_title);
        mToolbar.setTitle(getString(R.string.app_name) + " " + network);
    }

    @Override
    public void onTransferClick(View view, int position) {
        transferDetails(mAdapter.getTransfer(position));
    }

    class MainHandlerCallback implements IncomingHandlerCallback {

        @Override
        public void handleView(Message msg) {
            if (msg.what == NetworkService.MSG_LAST_STATE) {
                if (msg.arg2 == NetworkService.CONNECTION_OK) {
                    mViewModel.getMainData().observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new SingleObserver<AppRepository.MergedResponse>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                    mProgressBar.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onSuccess(AppRepository.MergedResponse mergedResponse) {
                                    Log.d(TAG, "success");
                                    updateMain(mergedResponse);
                                    mProgressBar.setVisibility(View.INVISIBLE);
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.d(TAG, "error");
                                    clearMain();
                                    mProgressBar.setVisibility(View.INVISIBLE);
                                }
                            });
                }
                if (msg.arg2 != NetworkService.CONNECTION_OK) {
                    clearMain();
                    mProgressBar.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    private void updateMain(AppRepository.MergedResponse mergedResponse) {
        parseGetInfo(mergedResponse.getInfoResponse);
        parseChannelBalance(mergedResponse.channelBalanceResponse);
        parseWalletBalance(mergedResponse.walletBalanceResponse);
        parseInvoices(mergedResponse.invoicesResponse);
        parsePayments(mergedResponse.paymentsResponse);
        parseTransactions(mergedResponse.transactionsResponse);
    }

    private void clearMain() {
        mAdapter.clearList();
        updateChannelBalance(getString(R.string.none));
        updateWalletBalance(getString(R.string.none));
        updateNetwork("");
    }

    private void parseChannelBalance(GrpcResponse grpcResponse) {
        if (grpcResponse.getMessage() == null)
            return;
        ChannelBalanceResponse response = (ChannelBalanceResponse) grpcResponse.getMessage();
        updateChannelBalance(String.valueOf(response.getBalance()));
    }

    private void parseWalletBalance(GrpcResponse grpcResponse) {
        if (grpcResponse.getMessage() == null)
            return;
        WalletBalanceResponse response = (WalletBalanceResponse) grpcResponse.getMessage();
        updateWalletBalance(String.valueOf(response.getBalance()));
    }

    private void updateChannelBalance(String balance) {
        mChannelBalanceTextView.setText(AppUtils.formatNumber(balance, 5));
    }

    private void updateWalletBalance(String balance) {
        mWalletBalanceTextView.setText(AppUtils.formatNumber(balance, 5));
    }

    class MainIncomingHandler extends IncomingHandler {

        MainIncomingHandler(Looper looper, Context context, View view,
                            IncomingHandlerCallback callback) {
            super(looper, context, view, callback);
        }

        @Override
        public void handleMessage(Message msg) {
            if (!getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED))
                return;
            super.handleMessage(msg);
        }
    }

    private void doBindService() {
        Intent intent = new Intent(MainActivity.this, NetworkService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        mConnection.mIsBound = true;
    }

    private void doUnbindService() {
        if (mConnection.mIsBound) {
            if (mConnection.mMessengerService != null) {
                try {
                    Message msg = Message.obtain(null, NetworkService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mConnection.mMessenger;
                    mConnection.mMessengerService.send(msg);
                } catch (RemoteException ignored) {}
            }
            unbindService(mConnection);
            mConnection.mIsBound = false;
        }
    }

    @Override
    public void onBackPressed() {
        setResult(LoginActivity.LOGIN_EXIT_COMMAND);
        finish();
    }

}

