/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.scan;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.journeyapps.barcodescanner.ViewfinderView;

import java.lang.reflect.Field;

import it.inbitcoin.globular.R;

public class ScanActivity extends AppCompatActivity {

    private static final String TAG = ScanActivity.class.getSimpleName();

    public static final int COPY = 100;
    public static final int ABORT = 101;

    public static final int REQ_CERTIFICATE = 3;
    public static final int REQ_MACAROON = 4;
    public static final int REQ_LIGHTERCONNECT_URI = 5;
    public static final int REQ_MACAROON_URI = 6;

    private CaptureManager capture;
    DecoratedBarcodeView barcodeScannerView;
    ImageView switchFlashlightButton;
    ImageView copyFromClipboardButton;
    private ViewfinderView viewfinderView;
    private boolean flashOn = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        barcodeScannerView = findViewById(R.id.zxing_barcode_scanner);
        switchFlashlightButton = findViewById(R.id.btn_switch_flashlight);
        copyFromClipboardButton = findViewById(R.id.btn_copy_from_clipboard);
        viewfinderView = findViewById(R.id.zxing_viewfinder_view);

        if (!hasFlash())
            switchFlashlightButton.setVisibility(View.GONE);

        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        capture.decode();

        ViewfinderView viewFinder = barcodeScannerView.getViewFinder();
        Field scannerAlphaField;
        try {
            scannerAlphaField = viewFinder.getClass().getDeclaredField("SCANNER_ALPHA");
            scannerAlphaField.setAccessible(true);
            scannerAlphaField.set(viewFinder, new int[1]);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        setListeners();
    }

    private void setListeners() {
        switchFlashlightButton.setOnClickListener(v -> switchFlashlight());
        copyFromClipboardButton.setOnClickListener(v -> {
            setResult(COPY);
            finish();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    public void switchFlashlight() {
        if (!flashOn) {
            barcodeScannerView.setTorchOn();
            flashOn = true;
            switchFlashlightButton.setImageAlpha(100);
        } else {
            barcodeScannerView.setTorchOff();
            flashOn = false;
            switchFlashlightButton.setImageAlpha(255);
        }
    }

    private boolean hasFlash() {
        return getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "back pressed");
        setResult(ABORT);
        finish();
    }

}
