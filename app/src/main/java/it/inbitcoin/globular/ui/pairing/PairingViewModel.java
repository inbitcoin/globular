/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.pairing;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import it.inbitcoin.globular.data.AppRepository;

@Singleton
public class PairingViewModel extends ViewModel {

    @NonNull
    private final AppRepository appRepo;

    String host;
    int port;
    String cert;
    String macaroon;
    boolean opCompleted = false;


    @Inject
    PairingViewModel(@NonNull AppRepository appRepo) {
        this.appRepo = appRepo;
    }

    Completable updateLighterHost() {
        return appRepo.updateLighterHostAsync();
    }

    Completable updateLighterPort() {
        return appRepo.updateLighterPortAsync();
    }

    Completable updateLighterSecure() {
        return appRepo.updateLighterSecureAsync();
    }

    Completable updateLighterTLSCertificate() {
        return appRepo.updateLighterTLSCertificateAsync();
    }

    Completable updateLighterMacaroon() {
        return appRepo.updateLighterMacaroonAsync();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (opCompleted) {
            host = null;
            port = 0;
            cert = null;
            macaroon = null;
        }
    }

}
