/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.pincreate;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.prefs.PreferencesHelper;
import it.inbitcoin.globular.ui.landing.LandingActivity;
import it.inbitcoin.globular.ui.pairing.PairingActivity;
import it.inbitcoin.globular.ui.pinentry.PinEntryActivity;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.CipherUtil;
import it.inbitcoin.globular.utils.TimeoutUtil;
import it.inbitcoin.globular.utils.ViewPager;

public class PinCreateActivity extends DaggerAppCompatActivity implements
        PinCreateFragment.onPinEntryListener {

    private static final String TAG = "PinCreateActivity";

    @Inject
    PinCreateFragment mPinCreateFragment;

    PinCreateFragment mPinEntryConfirmFragment;

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    TimeoutUtil mTimeoutUtil;

    @Inject
    PreferencesHelper mPreferencesHelper;

    private ViewPager pin_create_viewpager;

    private PinCreateActivity.PagerAdapter adapter;
    private LinearLayout finishButton;
    private String pinCode = "", pinCodeConfirm = "";

    public static final int FROM_LANDING = 1;
    public static final int FROM_SETTINGS = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_create);
        pin_create_viewpager = findViewById(R.id.pin_create_viewpager);
        finishButton = findViewById(R.id.btn_forward);
        if (getActionBar() != null)
            getActionBar().hide();
        adapter = new PinCreateActivity.PagerAdapter(getSupportFragmentManager());
        pin_create_viewpager.enableSwipe(false);
        pin_create_viewpager.setAdapter(adapter);
        pin_create_viewpager.setCurrentItem(0);
    }

    public void nextButton(View view) {
        int count = pin_create_viewpager.getCurrentItem();
        if (count == 0) {
            if (pinCode.length() == AccessFactory.PIN_LENGTH) {
                setForwardButtonEnable(false);
                finishButton.setVisibility(View.GONE);
                pin_create_viewpager.setCurrentItem(++count);
            } else {
                Toast.makeText(this, R.string.pin_6, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void abortButton(View view) {
        int count = pin_create_viewpager.getCurrentItem();
        switch (count) {
            case 0: {
                Log.d(TAG, "finish");
                finish();
                break;
            }
            case 1: {
                finish();
                break;
            }
        }
    }

    private void setForwardButtonEnable(boolean enable) {
        finishButton.setClickable(enable);
        finishButton.setAlpha(enable ? 1 : 0.2f);
    }

    @Override
    public void PinEntry(String pin) {
        if (pin_create_viewpager.getCurrentItem() == 0) {
            pinCode = pin;
            if (pinCode.length() == AccessFactory.PIN_LENGTH) {
                setForwardButtonEnable(true);
            } else {
                setForwardButtonEnable(false);
            }
        } else if (pin_create_viewpager.getCurrentItem() == 1) {
            pinCodeConfirm = pin;
            boolean pinIsCorrect = pinCodeConfirm.equals(pinCode);
            if (pinIsCorrect) {
                setPin(pinCode);
            } else if (pinCodeConfirm.length() == AccessFactory.PIN_LENGTH) {
                setForwardButtonEnable(false);
                Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
                pin_create_viewpager.startAnimation(shake);
                mPinEntryConfirmFragment.clearPin();
            } else {
                setForwardButtonEnable(false);
            }
        }
    }

    private class PagerAdapter extends FragmentPagerAdapter {

        PagerAdapter(FragmentManager fm) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                setForwardButtonEnable(false);
                return mPinCreateFragment;
            } else {
                mPinEntryConfirmFragment = PinCreateFragment
                        .newInstance(getString(R.string.pin_6_confirm), getString(R.string.re_enter_your_pin_code));
                return mPinEntryConfirmFragment;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    private void setPin(final String pin) {
        Completable.create(emitter -> {
            Looper.prepare();

            String scryptParamsJson = mAccessFactory.createScryptParams();
            Log.d(TAG, "scryptParasJson " + scryptParamsJson);
            byte[] accessKey = mAccessFactory.getAccessKey(pin, scryptParamsJson);
            Log.d(TAG, "retrieved accessKey");
            String cryptedAccessToken = CipherUtil.crypt(AppConstants.accessToken, accessKey);
            String msg;
            boolean validToken = mAccessFactory.validateToken(cryptedAccessToken, accessKey);
            Log.d(TAG, "cryptedAccessToken: " + cryptedAccessToken);
            if (validToken) {
                msg = getString(R.string.pin_set);
                mPreferencesHelper.setScryptParams(scryptParamsJson);
                mPreferencesHelper.setAccessToken(cryptedAccessToken);
                mAccessFactory.setAccessKey(accessKey);
                Log.d(TAG, "pin set: " + pin);
                mAccessFactory.setIsLoggedIn(true);
                mTimeoutUtil.updatePin();
                emitter.onComplete();
                restartApp();
            } else {
                msg = getString(R.string.pin_set_error);
            }
            Toast.makeText(PinCreateActivity.this, msg, Toast.LENGTH_SHORT).show();

            Looper.loop();
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    public void restartApp() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            switch (extras.getInt(AppConstants.requestKey)) {
                case FROM_LANDING:
                    Intent intent = new Intent(this, PairingActivity.class);
                    intent.putExtra(AppConstants.viaPinCreate, true);
                    setResult(LandingActivity.LANDING_EXIT_COMMAND);
                    startActivity(intent);
                    finish();
                    break;
                case FROM_SETTINGS:
                    Log.d(TAG, "change pin ok");
                    setResult(PinEntryActivity.CHANGE_PIN_OK);
                    finish();
                    break;
            }
        }
    }

}
