/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.settings;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.NumberPicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import java.util.Objects;

import it.inbitcoin.globular.R;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.PrefDefaults;

public class NumberPickerDialogFragment extends DialogFragment {

    private static final String TAG = "NumberPickerDialogFragment";

    private NumberPicker.OnValueChangeListener valueChangeListener;

    private static final int NUMBER_OF_VALUES = 12;
    static final int PICKER_RANGE = 5;


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        Activity parentActivity = getActivity();
        if (parentActivity == null)
            return super.onCreateDialog(savedInstanceState);

        final NumberPicker numberPicker = new NumberPicker(parentActivity);

        String[] displayedValues  = new String[NUMBER_OF_VALUES];
        for (int i=0; i < NUMBER_OF_VALUES; i++)
            displayedValues[i] = String.valueOf(PICKER_RANGE * (i + 1));
        numberPicker.setDisplayedValues(displayedValues);

        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(displayedValues.length - 1);

        int defaultValue = PrefDefaults.netServiceBackgroundInterval;
        if (getArguments() != null)
            defaultValue = getArguments().getInt(AppConstants.lastServiceInterval);
        defaultValue = (defaultValue / PICKER_RANGE) - 1;
        numberPicker.setValue(defaultValue);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.network_service_interval);
        builder.setMessage(R.string.network_service_interval_dialog_summary);

        builder.setPositiveButton(R.string.ok, (dialog, which) ->
                valueChangeListener.onValueChange(
                        numberPicker, numberPicker.getValue(), numberPicker.getValue()));

        builder.setNegativeButton(R.string.cancel, (dialog, which) ->
                Objects.requireNonNull(getDialog()).cancel());

        builder.setView(numberPicker);
        return builder.create();
    }

    public NumberPicker.OnValueChangeListener getValueChangeListener() {
        return valueChangeListener;
    }

    void setValueChangeListener(NumberPicker.OnValueChangeListener valueChangeListener) {
        this.valueChangeListener = valueChangeListener;
    }

}
