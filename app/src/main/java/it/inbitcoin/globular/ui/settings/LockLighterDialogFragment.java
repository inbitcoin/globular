/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.settings;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.inputmethod.EditorInfoCompat;
import androidx.fragment.app.DialogFragment;

import java.util.Objects;

import it.inbitcoin.globular.R;
import it.inbitcoin.globular.utils.AppConstants;

public class LockLighterDialogFragment extends DialogFragment {

    private static final String TAG = "LockLighterDialogFragment";


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Activity parentActivity = getActivity();
        if (parentActivity == null)
            return super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder = new AlertDialog.Builder(parentActivity);
        LayoutInflater inflater = requireActivity().getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.dialog_lock_lighter, null);
        EditText mPasswordEditText = dialogView.findViewById(R.id.lock_password);
        mPasswordEditText.setImeOptions(EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING);
        CheckBox mShowPasswordCheckBox = dialogView.findViewById(R.id.cb_show_password);
        mShowPasswordCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            int passwordLength = mPasswordEditText.getText().length();
            if (isChecked) {
                mPasswordEditText.setTransformationMethod(
                        HideReturnsTransformationMethod.getInstance());
                mPasswordEditText.setSelection(passwordLength);
            } else {
                mPasswordEditText.setTransformationMethod(
                        PasswordTransformationMethod.getInstance());
                mPasswordEditText.setSelection(passwordLength);
            }

        });

        builder.setView(dialogView);
        builder.setTitle(R.string.lock_lighter);
        builder.setMessage(R.string.lock_lighter_dialog_message);
        builder.setPositiveButton(R.string.lock, (dialog, id) -> {
            String password = mPasswordEditText.getText().toString();
            Intent intent = getActivity().getIntent().putExtra(
                    AppConstants.lockPassword, password);
            Objects.requireNonNull(getTargetFragment()).onActivityResult(
                    getTargetRequestCode(), SettingsFragment.LOCK_LIGHTER_OK, intent);
        });
        builder.setNegativeButton(R.string.cancel, (dialog, id) -> {
            Objects.requireNonNull(LockLighterDialogFragment.this.getDialog()).cancel();
            Intent intent = new Intent();
            Objects.requireNonNull(getTargetFragment()).onActivityResult(
                    getTargetRequestCode(), SettingsFragment.LOCK_LIGHTER_FAILED, intent);

        });
        return builder.create();
    }
}
