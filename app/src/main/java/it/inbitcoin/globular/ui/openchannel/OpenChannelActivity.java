/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.openchannel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import it.inbitcoin.globular.OpenChannelResponse;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.model.GrpcResponse;
import it.inbitcoin.globular.data.network.NetworkService;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.IncomingHandler;
import it.inbitcoin.globular.utils.NetworkServiceConnection;
import it.inbitcoin.globular.utils.TimeoutUtil;

public class OpenChannelActivity extends DaggerAppCompatActivity {

    private static final String TAG = "OpenChannelActivity";

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private OpenChannelViewModel mViewModel;

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    TimeoutUtil mTimeoutUtil;

    private Toolbar mToolbar;
    private ProgressBar mProgressBar;

    private RelativeLayout activityLayout;

    private Looper activityLooper;
    private IncomingHandler incomingHandler;

    private final NetworkServiceConnection mConnection = new NetworkServiceConnection();

    private TextView mNodeUriTextView;
    private EditText mFundingBitsEditText;
    private EditText mPushBitsEditText;
    private CheckBox mPrivateChannelCheckBox;

    private String nodeUri;
    private double fundingBits;
    private double pushBits;
    private boolean privateChannel;

    private FloatingActionButton mOpenChannelButton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_channel);
        Bundle extras = getIntent().getExtras();
        if (extras != null &&
                !Objects.requireNonNull(extras.getString(AppConstants.nodeUri)).isEmpty()) {
            nodeUri = extras.getString(AppConstants.nodeUri);
        }

        activityLayout = findViewById(R.id.open_channel_private_layout);

        mToolbar = findViewById(R.id.toolbar_open_channel);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mProgressBar = findViewById(R.id.pb_open_channel);

        mNodeUriTextView = findViewById(R.id.tv_node_uri_open_channel);
        mFundingBitsEditText = findViewById(R.id.et_funding_bits);
        mPushBitsEditText = findViewById(R.id.et_push_bits);
        mPrivateChannelCheckBox = findViewById(R.id.cb_private_channel);

        mNodeUriTextView.setText(nodeUri);

        mOpenChannelButton = findViewById(R.id.btn_open_channel_oc);
        mOpenChannelButton.setEnabled(false);

        activityLooper = Looper.myLooper();
        incomingHandler = new IncomingHandler(
                activityLooper, this, activityLayout, null);
        mConnection.mMessenger = new Messenger(incomingHandler);

        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(OpenChannelViewModel.class);

        setListeners();
    }

    private void setListeners() {
        final TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                updateState();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        mFundingBitsEditText.addTextChangedListener(watcher);
        mFundingBitsEditText.addTextChangedListener(
                new AppUtils.DecimalInputWatcher(mFundingBitsEditText, 6, 2, 0, 167772.16));
        mPushBitsEditText.addTextChangedListener(watcher);
        mPushBitsEditText.addTextChangedListener(
                new AppUtils.DecimalInputWatcher(mPushBitsEditText, 6, 5, 0, 167772.16));
        mPrivateChannelCheckBox.setOnClickListener(v -> updateState());
        mOpenChannelButton.setOnClickListener(
                v -> openChannel(nodeUri, fundingBits, pushBits, privateChannel));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!AppUtils.isTimedOut(mAccessFactory, mTimeoutUtil, this)) {
            activityLayout.setVisibility(View.VISIBLE);
            doBindService();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mTimeoutUtil.updatePin();
        activityLayout.setVisibility(View.GONE);
        doUnbindService();
    }

    private void updateState() {
        String fundingBitsStr = mFundingBitsEditText.getText().toString();
        if (!fundingBitsStr.isEmpty()) {
            try {
                fundingBits = Double.valueOf(fundingBitsStr);
                if (fundingBits > 0) {
                    mOpenChannelButton.setEnabled(true);
                } else {
                    mOpenChannelButton.setEnabled(false);
                }
            } catch (NumberFormatException e) {
                mOpenChannelButton.setEnabled(false);
            }
        } else {
            mOpenChannelButton.setEnabled(false);
        }
        String pushBitsStr = mPushBitsEditText.getText().toString();
        if (!pushBitsStr.isEmpty())
            try {
                pushBits = Double.valueOf(pushBitsStr);
            } catch (NumberFormatException e) {
                pushBits = 0;
            }
        else
            pushBits = 0;
        privateChannel = mPrivateChannelCheckBox.isChecked();
    }

    private void openChannel(String nodeUri, double fundingBits, double pushBits,
                             boolean privateChannel) {
        mViewModel.openChannel(nodeUri, fundingBits, pushBits, privateChannel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<GrpcResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mProgressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onSuccess(GrpcResponse grpcResponse) {
                        mProgressBar.setVisibility(View.INVISIBLE);
                        parseOpenChannel(grpcResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mProgressBar.setVisibility(View.INVISIBLE);
                        // TODO - toast error
                        finish();
                    }
                });
    }

    private void parseOpenChannel(GrpcResponse grpcResponse) {
        if (grpcResponse.getMessage() != null) {
            OpenChannelResponse response = (OpenChannelResponse) grpcResponse.getMessage();
            String fundingTxid = response.getFundingTxid();
            if (fundingTxid.isEmpty())
                Toast.makeText(this, R.string.channel_open_requested,
                        Toast.LENGTH_LONG).show();
            else
                Toast.makeText(this,
                        String.format(getString(R.string.channel_opened), fundingTxid),
                        Toast.LENGTH_LONG).show();
            finish();
        } else {
            if (grpcResponse.getErrorDescription() != null) {
                Log.d(TAG, grpcResponse.getErrorDescription());
                Toast.makeText(this, R.string.error, Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    void doBindService() {
        bindService(new Intent(OpenChannelActivity.this, NetworkService.class),
                mConnection, Context.BIND_AUTO_CREATE);
        mConnection.mIsBound = true;
    }

    void doUnbindService() {
        if (mConnection.mIsBound) {
            if (mConnection.mMessengerService != null) {
                try {
                    Message msg = Message.obtain(null, NetworkService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mConnection.mMessenger;
                    mConnection.mMessengerService.send(msg);
                } catch (RemoteException ignored) { }
            }
            unbindService(mConnection);
            mConnection.mIsBound = false;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
