/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.main;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.inbitcoin.globular.R;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.Transfer;

public class TransfersRecyclerViewAdapter extends
        RecyclerView.Adapter<TransfersRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "MyTransfersRecyViewAdap";

    private Transfer mTransfer;

    private final List<Transfer> mTransfers;
    private List<Transfer> mInvoices;
    private List<Transfer> mPayments;
    private List<Transfer> mTransactions;

    private OnClickTransferListener mListener;


    TransfersRecyclerViewAdapter(List<Transfer> items) {
        mTransfers = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_transfer, parent, false);
        return new ViewHolder(view);
    }

    public interface OnClickTransferListener {
        void onTransferClick(View view, int position);
    }

    void setClickListener(OnClickTransferListener listener) {
        mListener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        mTransfer = mTransfers.get(position);

        if (mListener != null)
            holder.itemView.setOnClickListener(view -> mListener.onTransferClick(view, position));

        String description = mTransfer.getDescription();
        holder.mDescriptionTextView.setText(description);
        String amount = String.valueOf(mTransfer.getAmount());
        holder.mAmountTextView.setText(AppUtils.formatNumber(amount, 5));
        try {
            if (Float.parseFloat(amount) < 0) {
                holder.mAmountTextView.setTextColor(holder.mView.getContext()
                        .getColor(R.color.send));
            } else {
                holder.mAmountTextView.setTextColor(holder.mView.getContext()
                        .getColor(R.color.recv));
            }
        } catch (NullPointerException ignored) {}

        holder.mTimestamp.setText(AppUtils.getIsoDate(mTransfer.getTimestamp()));

        if (mTransfer.getTransferType() == Transfer.TRANSACTIONS)
            holder.mPaymentTypeImageView.setImageResource(R.drawable.ic_bitcoin);
        else
            holder.mPaymentTypeImageView.setImageResource(R.drawable.ic_lightning);

    }

    Transfer getTransfer(int id) {
        return mTransfers.get(id);
    }

    @Override
    public int getItemCount() {
        if (mTransfers != null) {
            return mTransfers.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final View mView;
        private final ImageView mPaymentTypeImageView;
        private final TextView mDescriptionTextView;
        private final TextView mAmountTextView;
        private final TextView mTimestamp;

        private ViewHolder(View view) {
            super(view);
            mView = view;
            mPaymentTypeImageView = view.findViewById(R.id.payment_type);
            mDescriptionTextView = view.findViewById(R.id.description);
            mAmountTextView = view.findViewById(R.id.amount);
            mTimestamp = view.findViewById(R.id.timestamp);
            itemView.setOnClickListener(this);
        }

        @NonNull
        @Override
        public String toString() {
            return super.toString() + " '" + mDescriptionTextView.getText() + "'";
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) mListener.onTransferClick(v, getAdapterPosition());
        }
    }

    void updateTransfers(List<Transfer> transfers, int transferType) {
        switch (transferType) {
            case Transfer.INVOICES:
                mInvoices = new ArrayList<>();
                mInvoices.addAll(transfers);
                break;
            case Transfer.PAYMENTS:
                mPayments = new ArrayList<>();
                mPayments.addAll(transfers);
                break;
            case Transfer.TRANSACTIONS:
                mTransactions = new ArrayList<>();
                mTransactions.addAll(transfers);
                break;
            default:
        }
        if (mInvoices != null && mPayments != null && mTransactions != null) {
            Log.d(TAG, "updating transfers");
            mTransfers.clear();
            mTransfers.addAll(mInvoices);
            mTransfers.addAll(mPayments);
            mTransfers.addAll(mTransactions);
            Collections.sort(mTransfers);
            notifyDataSetChanged();
        }
    }

    void clearList() {
        int size = mTransfers.size();
        if (size > 0) {
            mTransfers.subList(0, size).clear();
            notifyItemRangeRemoved(0, size);
        }
    }

}
