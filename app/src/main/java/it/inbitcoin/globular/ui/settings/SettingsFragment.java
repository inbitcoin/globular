/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.preference.Preference;
import androidx.preference.PreferenceScreen;

import com.takisoft.preferencex.PreferenceFragmentCompat;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.HasAndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import io.grpc.Status;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.model.GrpcResponse;
import it.inbitcoin.globular.data.prefs.PreferencesHelper;
import it.inbitcoin.globular.ui.pairing.PairingActivity;
import it.inbitcoin.globular.ui.pinentry.PinEntryActivity;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.ApiEndPoint;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.UpdateObserver;

import static it.inbitcoin.globular.utils.AppUtils.getPreferenceKey;

public class SettingsFragment extends PreferenceFragmentCompat
        implements HasAndroidInjector, NumberPicker.OnValueChangeListener,
        SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = SettingsFragment.class.getSimpleName();

    @Inject
    ApiEndPoint mApiEndPoint;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private SettingsViewModel mViewModel;

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    PreferencesHelper mPreferencesHelper;

    private SettingsActivity parentActivity;

    // Globular configuration
    private PreferenceScreen mChangePinScreen;
    private PreferenceScreen mNetServiceIntervalPref;

    // Lighter configuration
    private PreferenceScreen mUnlockLighterScreen;
    private PreferenceScreen mLockLighterScreen;
    private PreferenceScreen mUnlockNodeScreen;
    private PreferenceScreen mLighterPairingScreen;
    private Preference mConnectionSettingsScreen;

    private ProgressBar mProgressBar;

    private static final int CHANGE_PIN_REQUEST = 3;
    public static final int CHANGE_PIN_OK = 4;
    public static final int PIN_FAILED = 5;
    private static final int UNLOCK_LIGHTER_REQUEST = 6;
    static final int UNLOCK_LIGHTER_OK = 7;
    static final int UNLOCK_LIGHTER_FAILED = 8;
    private static final int LOCK_LIGHTER_REQUEST = 9;
    static final int LOCK_LIGHTER_OK = 10;
    static final int LOCK_LIGHTER_FAILED = 11;
    private static final int UNLOCK_NODE_REQUEST = 12;
    static final int UNLOCK_NODE_OK = 13;
    static final int UNLOCK_NODE_FAILED = 14;
    private static final int LIGHTER_PAIRING_REQUEST = 15;
    public static final int LIGHTER_PAIRING_OK = 16;

    @Inject
    public SettingsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        parentActivity = (SettingsActivity) getActivity();
        assert parentActivity != null;
        mProgressBar = parentActivity.mProgressBar;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(SettingsViewModel.class);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPreferencesHelper.getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPreferencesHelper.getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {

        setPreferencesFromResource(R.xml.preferences, rootKey);

        // Globular settings
        mChangePinScreen = findPreference("change_pin");
        mNetServiceIntervalPref = findPreference(PreferencesHelper.NET_SERVICE_INTERVAL);

        // Lighter settings
        mUnlockLighterScreen = findPreference("unlock_lighter");
        mLockLighterScreen = findPreference("lock_lighter");
        mUnlockNodeScreen = findPreference("unlock_node");
        mLighterPairingScreen = findPreference("lighter pairing");
        mConnectionSettingsScreen = findPreference("advanced_connection");

        setListeners();
    }

    private void setListeners() {
        mChangePinScreen.setOnPreferenceClickListener(preference -> {
            showPinEntryActivity();
            return false;
        });
        mNetServiceIntervalPref.setOnPreferenceClickListener(preference -> {
            openNumberPickerDialog();
            return false;
        });
        mUnlockLighterScreen.setOnPreferenceClickListener(preference -> {
            mUnlockLighterScreen.setEnabled(false);
            mViewModel.getInfo().observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<GrpcResponse>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            mProgressBar.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onSuccess(GrpcResponse grpcResponse) {
                            mProgressBar.setVisibility(View.INVISIBLE);
                            mUnlockLighterScreen.setEnabled(true);
                            if (grpcResponse.getMessage() != null) {
                                Toast.makeText(parentActivity, R.string.already_unlocked,
                                        Toast.LENGTH_LONG).show();
                                return;
                            }
                            openUnlockLighterDialog();
                        }

                        @Override
                        public void onError(Throwable e) {
                            mProgressBar.setVisibility(View.INVISIBLE);
                            mUnlockLighterScreen.setEnabled(true);
                            Toast.makeText(parentActivity, R.string.error, Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
            return false;
        });
        mLockLighterScreen.setOnPreferenceClickListener(preference -> {
            mLockLighterScreen.setEnabled(false);
            mViewModel.getInfo().observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<GrpcResponse>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            mProgressBar.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onSuccess(GrpcResponse grpcResponse) {
                            mProgressBar.setVisibility(View.INVISIBLE);
                            mLockLighterScreen.setEnabled(true);
                            if (grpcResponse.getMessage() != null) {
                                Log.d(TAG, "lighter is unlocked");
                                openLockDialog();
                                return;
                            }
                            if (grpcResponse.getError() != null &&
                                    grpcResponse.getErrorCode().equals(Status.Code.UNIMPLEMENTED)) {
                                Log.d(TAG, "lighter is already locked");
                                Toast.makeText(parentActivity, R.string.already_locked,
                                        Toast.LENGTH_LONG).show();
                                return;
                            }
                            Toast.makeText(parentActivity, R.string.error, Toast.LENGTH_LONG)
                                    .show();
                        }

                        @Override
                        public void onError(Throwable e) {
                            mProgressBar.setVisibility(View.INVISIBLE);
                            mLockLighterScreen.setEnabled(true);
                            Toast.makeText(parentActivity, R.string.error, Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
            return false;
        });
        mUnlockNodeScreen.setOnPreferenceClickListener(preference -> tryOpenUnlockNodeDialog());
        mLighterPairingScreen.setOnPreferenceClickListener(preference -> {
            mLighterPairingScreen.setEnabled(false);
            return showPairingActivity();
        });
        mConnectionSettingsScreen.setOnPreferenceClickListener(
                preference -> showConnectionSettingsFragment());
    }

    private void openUnlockLighterDialog() {
        DialogFragment unlockDialog = new UnlockLighterDialogFragment();
        unlockDialog.setTargetFragment(SettingsFragment.this, UNLOCK_LIGHTER_REQUEST);
        unlockDialog.show(parentActivity.getSupportFragmentManager(), AppConstants.unlockLighterDialog);
    }

    private void openLockDialog() {
        Log.d(TAG, "openLockDialog");
        DialogFragment lockDialog = new LockLighterDialogFragment();
        lockDialog.setTargetFragment(SettingsFragment.this, LOCK_LIGHTER_REQUEST);
        lockDialog.show(parentActivity.getSupportFragmentManager(), AppConstants.lockDialog);
    }

    private boolean tryOpenUnlockNodeDialog() {
        mViewModel.getInfo().observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<GrpcResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mProgressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onSuccess(GrpcResponse grpcResponse) {
                        mProgressBar.setVisibility(View.INVISIBLE);
                        if (grpcResponse.getMessage() != null) {
                            Log.d(TAG, "lighter is unlocked");
                            openUnlockNodeDialog();
                            return;
                        }
                        if (grpcResponse.getError() != null &&
                                grpcResponse.getErrorCode().equals(Status.Code.UNIMPLEMENTED)) {
                            Log.d(TAG, "lighter is locked");
                            Toast.makeText(parentActivity, R.string.lighter_locked,
                                    Toast.LENGTH_LONG).show();
                            return;
                        }
                        Toast.makeText(parentActivity, R.string.error, Toast.LENGTH_LONG)
                                .show();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mProgressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(parentActivity, R.string.error, Toast.LENGTH_LONG)
                                .show();
                    }
                });
        return true;
    }

    private void openUnlockNodeDialog() {
        DialogFragment unlockDialog = new UnlockNodeDialogFragment();
        unlockDialog.setTargetFragment(SettingsFragment.this, UNLOCK_NODE_REQUEST);
        unlockDialog.show(parentActivity.getSupportFragmentManager(), AppConstants.unlockNodeDialog);
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        return super.onPreferenceTreeClick(preference);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            switch (key) {
                case PreferencesHelper.LIGHTER_HOST:
                    Log.d(TAG, "changed host");
                    mProgressBar.setVisibility(View.VISIBLE);
                    mViewModel.updateLighterHost().observeOn(Schedulers.io())
                            .subscribe(new UpdateObserver(mProgressBar));
                    mApiEndPoint.setChanged(true);
                    break;
                case PreferencesHelper.LIGHTER_PORT:
                    mProgressBar.setVisibility(View.VISIBLE);
                    mViewModel.updateLighterPort().observeOn(Schedulers.io())
                            .subscribe(new UpdateObserver(mProgressBar));
                    mApiEndPoint.setChanged(true);
                    break;
                case PreferencesHelper.LIGHTER_SECURE_CONN:
                    mProgressBar.setVisibility(View.VISIBLE);
                    mViewModel.updateLighterSecure().observeOn(Schedulers.io())
                            .subscribe(new UpdateObserver(mProgressBar));
                    mApiEndPoint.setChanged(true);
                    break;
                case PreferencesHelper.LIGHTER_DEMO_MODE:
                    mProgressBar.setVisibility(View.VISIBLE);
                    mViewModel.updateLighterDemoMode().observeOn(Schedulers.io())
                            .subscribe(new UpdateObserver(mProgressBar));
                    mApiEndPoint.setChanged(true);
                    break;
                default:
                    break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "Request code: " + requestCode + ", result code: " + resultCode);

        if (requestCode == UNLOCK_LIGHTER_REQUEST) {
            Log.d(TAG, "unlock Lighter request");
            if (resultCode == UNLOCK_LIGHTER_OK) {
                Bundle extras = parentActivity.getIntent().getExtras();
                if (extras == null)
                    return;
                String password = extras.getString(AppConstants.unlockPassword);
                boolean unlockNode = extras.getBoolean(AppConstants.unlockNodeCb);
                Log.d(TAG, "received password " + password);
                mViewModel.unlockLighter(password, unlockNode).observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<GrpcResponse>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                mProgressBar.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onSuccess(GrpcResponse grpcResponse) {
                                Log.d(TAG, "onSuccess");
                                mProgressBar.setVisibility(View.INVISIBLE);
                                parseUnlockLighter(grpcResponse);
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.d(TAG, "Unlock error: " + e);
                                mProgressBar.setVisibility(View.INVISIBLE);
                                Toast.makeText(parentActivity, R.string.error, Toast.LENGTH_LONG)
                                        .show();
                            }
                        });
            }
            if (resultCode == UNLOCK_LIGHTER_FAILED)
                mProgressBar.setVisibility(View.INVISIBLE);

            return;
        }

        if (requestCode == LOCK_LIGHTER_REQUEST) {
            Log.d(TAG, "lock Lighter request");
            if (resultCode == LOCK_LIGHTER_OK) {
                Bundle extras = parentActivity.getIntent().getExtras();
                if (extras == null)
                    return;
                String password = extras.getString(AppConstants.lockPassword);
                Log.d(TAG, "received password " + password);
                mViewModel.lockLighter(password).observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<GrpcResponse>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                mProgressBar.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onSuccess(GrpcResponse grpcResponse) {
                                Log.d(TAG, "onSuccess");
                                mProgressBar.setVisibility(View.INVISIBLE);
                                parseLockLighter(grpcResponse);
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.d(TAG, "Unlock error: " + e);
                                mProgressBar.setVisibility(View.INVISIBLE);
                                Toast.makeText(parentActivity, R.string.error, Toast.LENGTH_LONG)
                                        .show();
                            }
                        });
            }
            if (resultCode == UNLOCK_LIGHTER_FAILED)
                mProgressBar.setVisibility(View.INVISIBLE);

            return;
        }

        if (requestCode == UNLOCK_NODE_REQUEST) {
            Log.d(TAG, "unlock Lighter request");
            if (resultCode == UNLOCK_NODE_OK) {
                Bundle extras = parentActivity.getIntent().getExtras();
                if (extras == null)
                    return;
                String password = extras.getString(AppConstants.unlockPassword);
                Log.d(TAG, "received password " + password);
                mViewModel.unlockNode(password).observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<GrpcResponse>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                mProgressBar.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onSuccess(GrpcResponse grpcResponse) {
                                Log.d(TAG, "onSuccess");
                                mProgressBar.setVisibility(View.INVISIBLE);
                                parseUnlockNode(grpcResponse);
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.d(TAG, "Unlock error: " + e);
                                mProgressBar.setVisibility(View.INVISIBLE);
                                Toast.makeText(parentActivity, R.string.error, Toast.LENGTH_LONG)
                                        .show();
                            }
                        });
            }
            if (resultCode == UNLOCK_LIGHTER_FAILED)
                mProgressBar.setVisibility(View.INVISIBLE);

            return;
        }

        if (requestCode == CHANGE_PIN_REQUEST) {
            Log.d(TAG, "change pin request");
            if (resultCode == CHANGE_PIN_OK) {
                Log.d(TAG, "change pin ok");
                mProgressBar.setVisibility(View.VISIBLE);
                mViewModel.recryptData().observeOn(Schedulers.io())
                        .subscribe(new UpdateObserver(mProgressBar));
            } else if (resultCode == PIN_FAILED) {
                showPinEntryActivity();
            } else {
                AppUtils.cancelRequest(parentActivity);
                mAccessFactory.setOldAccessKey(null);
            }
            return;
        }

        if (requestCode == LIGHTER_PAIRING_REQUEST) {
            mProgressBar.setVisibility(View.INVISIBLE);
            mLighterPairingScreen.setEnabled(true);
            if (resultCode == LIGHTER_PAIRING_OK) {
                Log.d(TAG, "pairing success");
                Toast.makeText(parentActivity, R.string.success, Toast.LENGTH_LONG).show();
            } else {
                Log.d(TAG, "pairing unsuccess");
                AppUtils.cancelRequest(parentActivity);
            }
            return;
        }

        String preferenceKey = getPreferenceKey(requestCode);

        mProgressBar.setVisibility(View.VISIBLE);

        String clearPref = AppUtils.getDataFromScan(requestCode, resultCode, data, parentActivity);
        Log.d(TAG, "clear pref: " + clearPref);

        if (clearPref.isEmpty())
            return;

        AppUtils.cryptPref(mAccessFactory, mPreferencesHelper, preferenceKey, clearPref);

        updatePref(preferenceKey);
    }

    private void showPinEntryActivity() {
        Intent intent = new Intent(parentActivity, PinEntryActivity.class);
        intent.putExtra(AppConstants.requestKey, PinEntryActivity.FROM_SETTINGS);
        startActivityForResult(intent, CHANGE_PIN_REQUEST);
    }

    private boolean showPairingActivity() {
        Intent intent = new Intent(parentActivity, PairingActivity.class);
        startActivityForResult(intent, LIGHTER_PAIRING_REQUEST);
        return true;
    }

    private boolean showConnectionSettingsFragment() {
        parentActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_preference, new ConnectionSettingsFragment())
                .addToBackStack(null)
                .commit();
        return true;
    }

    private void updatePref(String preferenceKey) {
        if (preferenceKey.equals(PreferencesHelper.LIGHTER_TLS_CERTIFICATE)) {
            mViewModel.updateLighterTLSCertificate().observeOn(Schedulers.io())
                    .subscribe(new UpdateObserver(mProgressBar));
        }
        if (preferenceKey.equals(PreferencesHelper.LIGHTER_MACAROON)) {
            mViewModel.updateLighterMacaroon().observeOn(Schedulers.io())
                    .subscribe(new UpdateObserver(mProgressBar));
        }
        mApiEndPoint.setChanged(true);
    }

    private void parseUnlockLighter(GrpcResponse grpcResponse) {
        if (grpcResponse.getMessage() != null) {
            Toast.makeText(parentActivity, R.string.unlocked, Toast.LENGTH_LONG).show();
            return;
        }
        if (grpcResponse.getErrorDescription().contains(AppConstants.wrongPassword)) {
            Toast.makeText(parentActivity, R.string.wrong_password, Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(parentActivity, R.string.error, Toast.LENGTH_LONG).show();
    }

    private void parseLockLighter(GrpcResponse grpcResponse) {
        if (grpcResponse.getMessage() != null) {
            Toast.makeText(parentActivity, R.string.locked, Toast.LENGTH_LONG).show();
            return;
        }
        if (grpcResponse.getErrorDescription().contains(AppConstants.wrongPassword)) {
            Toast.makeText(parentActivity, R.string.wrong_password, Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(parentActivity, R.string.error, Toast.LENGTH_LONG).show();
    }

    private void parseUnlockNode(GrpcResponse grpcResponse) {
        if (grpcResponse.getMessage() != null) {
            Toast.makeText(parentActivity, R.string.unlocked, Toast.LENGTH_LONG).show();
            return;
        }
        if (grpcResponse.getErrorDescription().contains(AppConstants.wrongPassword)) {
            Toast.makeText(parentActivity, R.string.wrong_password, Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(parentActivity, R.string.error, Toast.LENGTH_LONG).show();
    }

    private void openNumberPickerDialog(){
        NumberPickerDialogFragment newFragment = new NumberPickerDialogFragment();
        newFragment.setValueChangeListener(this);
        Bundle extras = new Bundle();
        extras.putInt(AppConstants.lastServiceInterval, (int) mPreferencesHelper.getNetServiceInterval());
        newFragment.setArguments(extras);
        newFragment.show(parentActivity.getSupportFragmentManager(), AppConstants.intervalDialog);
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        long interval = (newVal + 1) * NumberPickerDialogFragment.PICKER_RANGE;
        Log.d(TAG, "background service interval changed to " + interval);
        mPreferencesHelper.setNetServiceInterval(interval);
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return null;
    }

}
