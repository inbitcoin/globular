/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.channeldetails;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import java.util.Objects;

import it.inbitcoin.globular.R;

public class CloseChannelDialogFragment extends DialogFragment {

    private static final String TAG = "CloseChannelDialogFragment";

    CloseChannelDialogListener callback;

    private boolean forceClose = false;


    public interface CloseChannelDialogListener {
        void onCloseChannelRequest(boolean forceClose);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        callback = (CloseChannelDialogListener) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Activity parentActivity = getActivity();
        if (parentActivity == null)
            return super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder = new AlertDialog.Builder(parentActivity);
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_close_channel, null);
        CheckBox mForceCloseCheckBox = dialogView.findViewById(R.id.cb_force_close);
        mForceCloseCheckBox.setOnCheckedChangeListener(((buttonView, isChecked) ->
                forceClose = isChecked
        ));
        builder.setView(dialogView);
        builder.setTitle(R.string.close_channel);
        builder.setMessage(R.string.close_channel_summary);
        builder.setPositiveButton(R.string.close, (dialog, id) -> {
            callback.onCloseChannelRequest(forceClose);

        });
        builder.setNegativeButton(R.string.cancel, (dialog, id) -> {
            Objects.requireNonNull(CloseChannelDialogFragment.this.getDialog()).cancel();
        });
        return builder.create();
    }
}
