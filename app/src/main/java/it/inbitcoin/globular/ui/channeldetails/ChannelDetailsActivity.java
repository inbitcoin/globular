/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.channeldetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import it.inbitcoin.globular.CloseChannelResponse;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.model.GrpcResponse;
import it.inbitcoin.globular.data.network.NetworkService;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.Channel;
import it.inbitcoin.globular.utils.IncomingHandler;
import it.inbitcoin.globular.utils.NetworkServiceConnection;
import it.inbitcoin.globular.utils.TimeoutUtil;

public class ChannelDetailsActivity extends DaggerAppCompatActivity implements
        CloseChannelDialogFragment.CloseChannelDialogListener {

    private static final String TAG = "ChannelDetailsActivity";

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private ChannelDetailsViewModel mViewModel;

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    TimeoutUtil mTimeoutUtil;

    private LinearLayout activityLayout;

    private Looper activityLooper;
    private IncomingHandler incomingHandler;
    private final NetworkServiceConnection mConnection = new NetworkServiceConnection();

    private Channel mChannel;

    private Toolbar mToolbar;
    private ProgressBar mProgressBar;

    private TextView mRemotePubkeyTextView;
    private TextView mAliasTextView;
    private TextView mChannelIdTextView;
    private TextView mShortChannelIdTextView;
    private TextView mFundingTxidTextView;
    private TextView mCapacityTextView;
    private TextView mLocalBalanceTextView;
    private TextView mRemoteBalanceTextView;
    private TextView mToSelfDelayTextView;
    private TextView mChannelStateTextView;

    private Button mCloseChannelButton;

    private String channelId;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_channel_details);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (getIntent().hasExtra(AppConstants.channel))
                mChannel = extras.getParcelable(AppConstants.channel);
        }

        activityLayout = findViewById(R.id.channel_details_coordinator_layout);

        activityLooper = Looper.myLooper();
        incomingHandler = new IncomingHandler(
                activityLooper, this, activityLayout, null);
        mConnection.mMessenger = new Messenger(incomingHandler);

        mToolbar = findViewById(R.id.toolbar_channel_details);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mProgressBar = findViewById(R.id.pb_channel_details);

        mRemotePubkeyTextView = findViewById(R.id.tv_remote_pubkey_details);
        mAliasTextView = findViewById(R.id.tv_alias_details);
        mChannelIdTextView = findViewById(R.id.tv_channel_id_details);
        mShortChannelIdTextView = findViewById(R.id.tv_short_channel_id_details);
        mFundingTxidTextView = findViewById(R.id.tv_funding_txid_details);
        mCapacityTextView = findViewById(R.id.tv_capacity_details);
        mLocalBalanceTextView = findViewById(R.id.tv_local_balance_details);
        mRemoteBalanceTextView = findViewById(R.id.tv_remote_balance_details);
        mToSelfDelayTextView = findViewById(R.id.tv_to_self_delay_details);
        mChannelStateTextView = findViewById(R.id.tv_channel_state_details);

        mCloseChannelButton = findViewById(R.id.btn_close_channel);

        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(ChannelDetailsViewModel.class);

        showChannel();
        setListeners();
    }

    private void setListeners() {
        mCloseChannelButton.setOnClickListener(v -> {
            openCloseDialog();
        });
    }

    private void showChannel() {
        String remotePubkey = mChannel.getRemotePubkey();
        if (!TextUtils.isEmpty(remotePubkey)) mRemotePubkeyTextView.setText(remotePubkey);

        String alias = mChannel.getAlias();
        if (!TextUtils.isEmpty(alias)) mAliasTextView.setText(alias);

        channelId = mChannel.getChannelId();
        if (!TextUtils.isEmpty(channelId)) mChannelIdTextView.setText(channelId);

        String shortChannelId = mChannel.getShortChannelId();
        if (!TextUtils.isEmpty(shortChannelId)) mShortChannelIdTextView.setText(shortChannelId);

        String fundingTxid = mChannel.getFundingTxid();
        if (!TextUtils.isEmpty(fundingTxid)) mFundingTxidTextView.setText(fundingTxid);

        String capacity = AppUtils.formatNumber(String.valueOf(mChannel.getCapacity()), 5);
        if (capacity != null) mCapacityTextView.setText(capacity);

        String localBalance = AppUtils.formatNumber(String.valueOf(mChannel.getLocalBalance()), 5);
        if (localBalance != null) mLocalBalanceTextView.setText(localBalance);

        String remoteBalance = AppUtils.formatNumber(String.valueOf(mChannel.getRemoteBalance()), 5);
        if (remoteBalance != null) mRemoteBalanceTextView.setText(remoteBalance);

        String toSelfDelay = String.valueOf(mChannel.getToSelfDelay());
        if (!TextUtils.isEmpty(toSelfDelay)) mToSelfDelayTextView.setText(toSelfDelay);

        int channelStateValue = mChannel.getChannelState();
        String channelState = getChannelStateDescription(channelStateValue);
        if (!channelState.isEmpty()) mChannelStateTextView.setText(channelState);

        if (TextUtils.isEmpty(channelId) || channelStateValue != 1)
            mCloseChannelButton.setEnabled(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!AppUtils.isTimedOut(mAccessFactory, mTimeoutUtil, this))
            doBindService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mTimeoutUtil.updatePin();
        doUnbindService();
    }

    private String getChannelStateDescription(int channelState) {
        switch (channelState) {
            case 0:
                return getString(R.string.channel_pending_open);
            case 1:
                return getString(R.string.channel_open);
            case 2:
                return getString(R.string.channel_pending_mutual_close);
            case 3:
                return getString(R.string.channel_pending_force_close);
            case 4:
                return getString(R.string.channel_unknown);
            default:
                return "";
        }
    }

    void doBindService() {
        bindService(new Intent(ChannelDetailsActivity.this, NetworkService.class),
                mConnection, Context.BIND_AUTO_CREATE);
        mConnection.mIsBound = true;
    }

    void doUnbindService() {
        if (mConnection.mIsBound) {
            if (mConnection.mMessengerService != null) {
                try {
                    Message msg = Message.obtain(null, NetworkService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mConnection.mMessenger;
                    mConnection.mMessengerService.send(msg);
                } catch (RemoteException ignored) { }
            }
            unbindService(mConnection);
            mConnection.mIsBound = false;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void openCloseDialog() {
        DialogFragment closeDialog = new CloseChannelDialogFragment();
        closeDialog.show(getSupportFragmentManager(), AppConstants.closeDialog);
    }

    private void parseCloseChannel(GrpcResponse grpcResponse) {
        if (grpcResponse.getMessage() == null) {
            Toast.makeText(this, R.string.error, Toast.LENGTH_LONG).show();
            mCloseChannelButton.setEnabled(true);
        } else {
            CloseChannelResponse response = (CloseChannelResponse) grpcResponse.getMessage();
            if (response.getClosingTxid().isEmpty())
                Toast.makeText(this, R.string.closing_channel, Toast.LENGTH_LONG).show();
            else {
                String closingTXID = String.format(getString(R.string.closed_channel), response.getClosingTxid());
                Toast.makeText(this, closingTXID, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onCloseChannelRequest(boolean forceClose) {
        mViewModel.closeChannel(channelId, forceClose)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<GrpcResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mCloseChannelButton.setEnabled(false);
                        mProgressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onSuccess(GrpcResponse grpcResponse) {
                        mProgressBar.setVisibility(View.INVISIBLE);
                        parseCloseChannel(grpcResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mProgressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(getParent(), R.string.error, Toast.LENGTH_LONG).show();
                        mCloseChannelButton.setEnabled(true);
                    }
                });
    }

}
