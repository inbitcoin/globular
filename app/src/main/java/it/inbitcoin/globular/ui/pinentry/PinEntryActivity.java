/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.pinentry;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.prefs.PreferencesHelper;
import it.inbitcoin.globular.ui.login.LoginActivity;
import it.inbitcoin.globular.ui.pincreate.PinCreateActivity;
import it.inbitcoin.globular.ui.settings.SettingsFragment;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.ScrambledPin;
import it.inbitcoin.globular.utils.TimeoutUtil;

public class PinEntryActivity extends DaggerAppCompatActivity {

    @Inject
    AccessFactory mAccessFactory;

    @Inject
    TimeoutUtil mTimeoutUtil;

    @Inject
    PreferencesHelper mPreferencesHelper;

    private static final String TAG = "PinEntryActivity";

    private static final int maxFailures = 3;

    private Button btn0 = null;
    private Button btn1 = null;
    private Button btn2 = null;
    private Button btn3 = null;
    private Button btn4 = null;
    private Button btn5 = null;
    private Button btn6 = null;
    private Button btn7 = null;
    private Button btn8 = null;
    private Button btn9 = null;
    private ImageButton sendButton = null;
    private ImageButton backButton = null;
    private Vibrator vibrator;

    private TextView promptTextView = null;
    private TextView userInputTextView = null;

    private ScrambledPin keypad = null;

    private StringBuilder userInput = null;

    public static final int FROM_LOGIN = 1;
    public static final int FROM_SETTINGS = 2;
    private static final int CHANGE_PIN_REQUEST = 3;
    public static final int CHANGE_PIN_OK = 4;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         getWindow().setFlags(
                 WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.grid);
        userInput = new StringBuilder();
        keypad = new ScrambledPin();

        userInputTextView = findViewById(R.id.userInput);
        userInputTextView.setText("");

        vibrator = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);

        promptTextView = findViewById(R.id.prompt2);

        boolean scramble = mPreferencesHelper.getScramblePin();

        btn1 = findViewById(R.id.ta);
        btn1.setText(scramble ? Integer.toString(keypad.getMatrix().get(0).getValue()) : "1");
        btn2 = findViewById(R.id.tb);
        btn2.setText(scramble ? Integer.toString(keypad.getMatrix().get(1).getValue()) : "2");
        btn3 = findViewById(R.id.tc);
        btn3.setText(scramble ? Integer.toString(keypad.getMatrix().get(2).getValue()) : "3");
        btn4 = findViewById(R.id.td);
        btn4.setText(scramble ? Integer.toString(keypad.getMatrix().get(3).getValue()) : "4");
        btn5 = findViewById(R.id.te);
        btn5.setText(scramble ? Integer.toString(keypad.getMatrix().get(4).getValue()) : "5");
        btn6 = findViewById(R.id.tf);
        btn6.setText(scramble ? Integer.toString(keypad.getMatrix().get(5).getValue()) : "6");
        btn7 = findViewById(R.id.tg);
        btn7.setText(scramble ? Integer.toString(keypad.getMatrix().get(6).getValue()) : "7");
        btn8 = findViewById(R.id.th);
        btn8.setText(scramble ? Integer.toString(keypad.getMatrix().get(7).getValue()) : "8");
        btn9 = findViewById(R.id.ti);
        btn9.setText(scramble ? Integer.toString(keypad.getMatrix().get(8).getValue()) : "9");
        btn0 = findViewById(R.id.tj);
        btn0.setText(scramble ? Integer.toString(keypad.getMatrix().get(9).getValue()) : "0");
        sendButton = findViewById(R.id.tsend);
        backButton = findViewById(R.id.tback);
        sendButton.setVisibility(View.INVISIBLE);

        setListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();
        userInput.setLength(0);
        displayUserInput();
    }

    private void setListeners() {
        sendButton.setOnClickListener(view -> {
            boolean pinLengthIsOk = userInput.toString().length() == AccessFactory.PIN_LENGTH;
            if (pinLengthIsOk) {
                if (mPreferencesHelper.getHapticPin())
                    vibrator.vibrate(55);
                Log.d(TAG, "length of pin is ok");
                sendButton.setEnabled(false);
                validateThread(userInput.toString());
            }
        });
        backButton.setOnClickListener(view -> {
            if (userInput.toString().length() > 0) {
                userInput.deleteCharAt(userInput.length() - 1);
                if (mPreferencesHelper.getHapticPin())
                    vibrator.vibrate(55);
            }
            displayUserInput();

        });
        backButton.setOnLongClickListener(view -> {
            if (userInput.toString().length() > 0) {
                userInput.setLength(0);
                if (mPreferencesHelper.getHapticPin())
                    vibrator.vibrate(55);
            }
            displayUserInput();
            return false;
        });
    }

    public void OnNumberPadClick(View view) {
        if (userInput.toString().length() == AccessFactory.PIN_LENGTH) {
            Log.d(TAG, "reached pin max length");
            return;
        }
        if (mPreferencesHelper.getHapticPin())
            vibrator.vibrate(55);
        userInput.append(((Button) view).getText().toString());
        displayUserInput();
    }

    private void displayUserInput() {

        userInputTextView.setText("");

        for (int i = 0; i < userInput.toString().length(); i++) {
            userInputTextView.append("*");
        }

        boolean pinLengthIsOk = userInput.toString().length() == AccessFactory.PIN_LENGTH;

        if (pinLengthIsOk) {
            sendButton.setVisibility(View.VISIBLE);
        } else {
            sendButton.setVisibility(View.INVISIBLE);
        }

    }

    private void validateThread(final String pin) {
        validateThreadAsync(pin)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "thread validated");
                        restartApp();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }

    private Completable validateThreadAsync(final String pin) {
        return Completable.create(emitter -> {
            Log.d(TAG, "validating thread, pin: " + pin);

            Looper.prepare();

            boolean pinLengthIsOk = pin.length() == AccessFactory.PIN_LENGTH;
            if (!pinLengthIsOk) {
                Toast.makeText(PinEntryActivity.this, R.string.pin_error,
                        Toast.LENGTH_SHORT).show();
            }
            String scryptParamsJson = mPreferencesHelper.getScryptParams();
            String cryptedAccessToken = mPreferencesHelper.getAccessToken();
            Log.d(TAG, "cryptedAccessToken: " + cryptedAccessToken);
            byte[] accessKey = mAccessFactory.getAccessKey(pin, scryptParamsJson);
            Log.d(TAG, "got accessKey");
            boolean validToken = mAccessFactory.validateToken(cryptedAccessToken, accessKey);
            if (validToken) {
                Log.d(TAG, "login success");
                mPreferencesHelper.setPinFailedAttempts(0);
                mAccessFactory.setAccessKey(accessKey);
                mAccessFactory.setIsLoggedIn(true);
                mTimeoutUtil.updatePin();
                emitter.onComplete();
            } else {
                Log.d(TAG, "login failed");
                int failures = mPreferencesHelper.getPinFailedAttempts() + 1;
                mPreferencesHelper.setPinFailedAttempts(failures);
                String failuresCount = getText(R.string.login_error) + ": " + failures + "/" +
                        maxFailures;
                if (failures == maxFailures) {
                    Toast.makeText(PinEntryActivity.this,
                            failuresCount + ": " + getText(R.string.pref_deletion),
                            Toast.LENGTH_LONG).show();
                    deletePreferences();
                } else {
                    Toast.makeText(PinEntryActivity.this, failuresCount,
                            Toast.LENGTH_SHORT).show();
                    int requestCode = 0;
                    try {
                        Bundle extras = getIntent().getExtras();
                        if (extras != null)
                            requestCode = extras.getInt(AppConstants.requestKey);
                    } catch (NullPointerException ignored) {}
                    if (requestCode == FROM_SETTINGS)
                        setResult(SettingsFragment.PIN_FAILED);
                    finish();
                }
            }

            Looper.loop();
        });
    }

    void deletePreferences() {
        mPreferencesHelper.deleteAllPreferences();
        Intent intent = new Intent(PinEntryActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    public void restartApp() {
        Intent intent;
        Bundle extras = getIntent().getExtras();
        if (extras == null)
            return;
        switch (extras.getInt(AppConstants.requestKey)) {
            case FROM_SETTINGS:
                intent = new Intent(this, PinCreateActivity.class);
                mAccessFactory.setOldAccessKey(mAccessFactory.getAccessKey());
                intent.putExtra(AppConstants.requestKey, PinCreateActivity.FROM_SETTINGS);
                startActivityForResult(intent, CHANGE_PIN_REQUEST);
                break;
            case FROM_LOGIN:
                intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getInt(AppConstants.requestKey) == FROM_LOGIN) {
            Log.d(TAG, "setting exit command");
            setResult(LoginActivity.LOGIN_EXIT_COMMAND);
            finish();
        } else {
            Log.d(TAG, "super onBackPressed");
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHANGE_PIN_REQUEST) {
            if (resultCode == CHANGE_PIN_OK) {
                Log.d(TAG, "change pin ok");
                setResult(SettingsFragment.CHANGE_PIN_OK);
            }
            finish();
        }
    }

}