/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.receive;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.ShareActionProvider;
import androidx.core.view.MenuItemCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import net.i2p.android.ext.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import it.inbitcoin.globular.CreateInvoiceResponse;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.model.GrpcResponse;
import it.inbitcoin.globular.data.network.RestInterface;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceiveOffChainFragment extends DaggerFragment {

    private static final String TAG = "ReceiveOffChainFragment";

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private ReceiveViewModel mViewModel;

    @Inject
    RestInterface restInterface;

    private ReceiveActivity parentActivity;

    MenuItem mShareMenuItem;
    public boolean noShare = true;

    private EditText mDescriptionEditText;
    private EditText mAmountEditText;
    private TextView mPaymentRequestTextView;
    FloatingActionButton mCreateInvoiceButton;
    private ImageView mQrcodeImageView;
    private RelativeLayout mLoadingPanel;

    private String paymentRequest;


    @Inject
    public ReceiveOffChainFragment() {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(ReceiveViewModel.class);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentActivity = (ReceiveActivity) getActivity();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_receive_offchain, container, false);

        mDescriptionEditText = root.findViewById(R.id.et_description);
        mAmountEditText = root.findViewById(R.id.et_receive_amount);
        mAmountEditText.requestFocus();
        mPaymentRequestTextView = root.findViewById(R.id.tv_payment_request);
        mPaymentRequestTextView.setVisibility(View.GONE);
        mQrcodeImageView = root.findViewById(R.id.img_qrcode_invoice);
        mQrcodeImageView.setVisibility(View.GONE);
        mCreateInvoiceButton = root.findViewById(R.id.btn_create_invoice);
        mCreateInvoiceButton.setEnabled(false);
        mLoadingPanel = root.findViewById(R.id.loading_panel);
        mLoadingPanel.setVisibility(View.GONE);

        setListeners();

        return root;
    }

    private void setListeners() {
        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                hideInvoice();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        mAmountEditText.setOnEditorActionListener(new AppUtils.EditorDoneListener());
        mAmountEditText.addTextChangedListener(watcher);
        mAmountEditText.addTextChangedListener(
                new AppUtils.DecimalInputWatcher(mAmountEditText, 5, 5, 0, 42949.67296)
        );
        mDescriptionEditText.setOnEditorActionListener(new AppUtils.EditorDoneListener());
        mDescriptionEditText.addTextChangedListener(watcher);
        mCreateInvoiceButton.setOnClickListener(v -> {
            mCreateInvoiceButton.setEnabled(false);
            mQrcodeImageView.setVisibility(View.GONE);
            mLoadingPanel.setVisibility(View.VISIBLE);
            AppUtils.hideKeyboard(mCreateInvoiceButton);
            String description = mDescriptionEditText.getText().toString();
            double amount;
            try {
                amount = Double.parseDouble(mAmountEditText.getText().toString());
                getInvoice(amount, description);
            } catch (NumberFormatException e) {
                getInvoice(0, description);
            }
        });
        mQrcodeImageView.setOnClickListener(v -> {
            Intent sharingIntent = AppUtils.getShareIntent(
                    parentActivity, paymentRequest, AppConstants.qrLastInvoice);
            startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_via)));
        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.share, menu);
        super.onCreateOptionsMenu(menu,inflater);
        mShareMenuItem = menu.findItem(R.id.menu_item_share);
        ShareActionProvider shareActionProvider =
                (ShareActionProvider) MenuItemCompat.getActionProvider(mShareMenuItem);
        shareActionProvider.setShareIntent(AppUtils.getShareIntent(
                parentActivity, paymentRequest, AppConstants.qrLastInvoice));
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (noShare)
            mShareMenuItem.setVisible(false);
        else
            mShareMenuItem.setVisible(true);
    }

    private void getInvoice(double amount, String description) {
        mViewModel.onCleared();
        mViewModel.createInvoice(amount, description).observe(this, this::parseResponse);
    }

    private void parseResponse(GrpcResponse grpcResponse) {
        if (grpcResponse.getMessage() != null) {
            CreateInvoiceResponse response = (CreateInvoiceResponse) grpcResponse.getMessage();
            paymentRequest = response.getPaymentRequest();
            showInvoice();

        } else {
            if (grpcResponse.getErrorDescription() != null
                    && !grpcResponse.getErrorDescription().equals("")) {
                mCreateInvoiceButton.setEnabled(true);
                mLoadingPanel.setVisibility(View.GONE);
                Toast.makeText(parentActivity,
                        grpcResponse.getErrorDescription(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void showInvoice() {
        mPaymentRequestTextView.setText(paymentRequest);
        // LN invoices are case insensitive, we can take advantage of qr alphanumeric encoding
        restInterface.getQrcode(paymentRequest.toUpperCase()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call,
                                   @NonNull Response<ResponseBody> response) {
                if (response.body() != null) {
                    Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                    mQrcodeImageView.setImageBitmap(bmp);
                    AppUtils.saveImage(parentActivity, bmp, AppConstants.qrLastInvoice);
                    noShare = false;
                    Objects.requireNonNull(getActivity()).invalidateOptionsMenu();
                } else {
                    noShare = true;
                    Objects.requireNonNull(getActivity()).invalidateOptionsMenu();
                    mQrcodeImageView.setImageResource(R.drawable.img_qrcat);
                }
                mLoadingPanel.setVisibility(View.GONE);
                mQrcodeImageView.setVisibility(View.VISIBLE);
                mPaymentRequestTextView.setVisibility(View.VISIBLE);
                mCreateInvoiceButton.setEnabled(true);
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                mQrcodeImageView.setImageResource(R.drawable.img_qrcat);
                mLoadingPanel.setVisibility(View.GONE);
                mQrcodeImageView.setVisibility(View.VISIBLE);
                mPaymentRequestTextView.setVisibility(View.VISIBLE);
                mCreateInvoiceButton.setEnabled(true);
            }
        });
    }

    private void hideInvoice() {
        mQrcodeImageView.setVisibility(View.GONE);
        mPaymentRequestTextView.setVisibility(View.GONE);
    }

}
