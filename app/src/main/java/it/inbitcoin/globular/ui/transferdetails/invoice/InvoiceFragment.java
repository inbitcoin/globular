/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.transferdetails.invoice;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.network.RestInterface;
import it.inbitcoin.globular.ui.transferdetails.TransferDetailsActivity;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;
import it.inbitcoin.globular.utils.Transfer;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceFragment extends DaggerFragment {

    private static final String TAG = "InvoiceFragment";

    @Inject
    RestInterface restInterface;

    private TransferDetailsActivity parentActivity;

    private Transfer mTransfer;

    private TextView mAmountTextView;
    private TextView mExpiryTimeTextView;
    private TextView mDescriptionTextView;
    private ImageView mQrcodeImageView;

    private CountDownTimer countDownTimer;

    public String mPaymentRequest;


    @Inject
    public InvoiceFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentActivity = (TransferDetailsActivity) getActivity();
        assert parentActivity != null;
        Intent intent = parentActivity.getIntent();
        if (intent != null && intent.hasExtra(AppConstants.transfer)) {
            Bundle extras = intent.getExtras();
            if (extras != null)
                mTransfer = extras.getParcelable(AppConstants.transfer);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_invoice, container, false);
        mAmountTextView = root.findViewById(R.id.tv_amount_invoice);
        mExpiryTimeTextView = root.findViewById(R.id.tv_expires_invoice);
        mDescriptionTextView = root.findViewById(R.id.tv_description);
        mQrcodeImageView = root.findViewById(R.id.invoice_qrcode);
        mQrcodeImageView.setVisibility(View.GONE);
        showInvoice();
        setListeners();
        return root;
    }

    private void setListeners() {
        mQrcodeImageView.setOnClickListener(v -> {
            Intent sharingIntent = AppUtils.getShareIntent(
                    parentActivity, mPaymentRequest, AppConstants.qrInvoiceDetail);
            startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_via)));
        });
    }

    private void showInvoice() {

        double amount = mTransfer.getAmount();
        if (amount != 0) mAmountTextView.setText(AppUtils.formatNumber(String.valueOf(amount), 5));

        String description = mTransfer.getDescription();
        if (!TextUtils.isEmpty(description)) mDescriptionTextView.setText(description);

        boolean paid = mTransfer.getStatus() == Transfer.PAID;
        long expiryTimestamp = mTransfer.getTimestamp() * 1000 + mTransfer.getExpiryTime() * 1000;
        long countdown = expiryTimestamp - System.currentTimeMillis();
        if (paid) {
            mExpiryTimeTextView.setText(R.string.paid);
        } else if (countdown > 0) {
            if (mTransfer.getPaymentRequest() != null) {
                showQrcode(mTransfer.getPaymentRequest());
            }
            countDownTimer = new CountDownTimer(countdown, 1000) {

                public void onTick(long millisUntilFinished) {

                    String hms = String.format(Locale.getDefault(), "%02d:%02d:%02d",
                            TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));

                    mExpiryTimeTextView.setText(hms);
                }

                public void onFinish() {
                    mExpiryTimeTextView.setText(R.string.expired);
                }
            }.start();
        } else {
            mExpiryTimeTextView.setText(R.string.expired);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

    private void showQrcode(String paymentRequest) {
        // LN invoices are case insensitive, we can take advantage of qr alphanumeric encoding
        mPaymentRequest = paymentRequest.toUpperCase();
        restInterface.getQrcode(paymentRequest).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.body() != null) {
                    Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                    AppUtils.saveImage(parentActivity, bmp, AppConstants.qrInvoiceDetail);
                    mQrcodeImageView.setImageBitmap(bmp);
                    parentActivity.noShare = false;
                    parentActivity.invalidateOptionsMenu();
                } else {
                    mQrcodeImageView.setImageResource(R.drawable.img_qrcat);
                    parentActivity.noShare = true;
                    parentActivity.invalidateOptionsMenu();
                }
                mQrcodeImageView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                mQrcodeImageView.setImageResource(R.drawable.img_qrcat);
                mQrcodeImageView.setVisibility(View.VISIBLE);
            }
        });
    }

}
