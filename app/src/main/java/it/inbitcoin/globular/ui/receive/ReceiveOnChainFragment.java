/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.receive;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.ShareActionProvider;
import androidx.core.view.MenuItemCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import net.i2p.android.ext.floatingactionbutton.FloatingActionButton;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import it.inbitcoin.globular.AddressType;
import it.inbitcoin.globular.NewAddressResponse;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.model.GrpcResponse;
import it.inbitcoin.globular.data.network.RestInterface;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceiveOnChainFragment extends DaggerFragment {

    private static final String TAG = "ReceiveOnChainFragment";

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private ReceiveViewModel mViewModel;

    @Inject
    RestInterface restInterface;

    private ReceiveActivity parentActivity;

    MenuItem mShareMenuItem;
    public boolean noShare = true;

    private EditText mAmountEditText;
    private Switch mAddressTypeSwitch;
    private TextView mAddressTextView;
    FloatingActionButton mCreateAddressButton;
    private ImageView mQrcodeImageView;
    private RelativeLayout mLoadingPanel;

    private String address;
    private double amount;
    private AddressType addressType = AddressType.BECH32;


    @Inject
    public ReceiveOnChainFragment() {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(ReceiveViewModel.class);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentActivity = (ReceiveActivity) getActivity();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_receive_onchain, container, false);

        mAmountEditText = root.findViewById(R.id.et_receive_amount);
        mAmountEditText.requestFocus();
        mAddressTypeSwitch = root.findViewById(R.id.switch_address_type);
        mAddressTextView = root.findViewById(R.id.tv_payment_request);
        mAddressTextView.setVisibility(View.GONE);
        mQrcodeImageView = root.findViewById(R.id.img_qrcode_address);
        mQrcodeImageView.setVisibility(View.GONE);
        mCreateAddressButton = root.findViewById(R.id.btn_create_address);
        mCreateAddressButton.setEnabled(false);
        mLoadingPanel = root.findViewById(R.id.loading_panel);
        mLoadingPanel.setVisibility(View.GONE);

        setListeners();

        return root;
    }

    private void setListeners() {
        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                hideAddress();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        mAmountEditText.setOnEditorActionListener(new AppUtils.EditorDoneListener());
        mAmountEditText.addTextChangedListener(watcher);
        mAmountEditText.addTextChangedListener(
                new AppUtils.DecimalInputWatcher(mAmountEditText, 14, 2, 0, 21000000000000.0)
        );
        mAddressTypeSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                addressType = AddressType.BECH32;
            else
                addressType = AddressType.NP2WKH;
        });
        mCreateAddressButton.setOnClickListener(v -> {
            mCreateAddressButton.setEnabled(false);
            mQrcodeImageView.setVisibility(View.GONE);
            mLoadingPanel.setVisibility(View.VISIBLE);
            AppUtils.hideKeyboard(mCreateAddressButton);
            if (!mAmountEditText.getText().toString().isEmpty())
                amount = Double.parseDouble(mAmountEditText.getText().toString());
            getAddress();
        });
        mQrcodeImageView.setOnClickListener(v -> {
            Intent sharingIntent = AppUtils.getShareIntent(
                    parentActivity, address, AppConstants.qrLastAddress);
            startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_via)));
        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.share, menu);
        super.onCreateOptionsMenu(menu,inflater);
        mShareMenuItem = menu.findItem(R.id.menu_item_share);
        ShareActionProvider shareActionProvider =
                (ShareActionProvider) MenuItemCompat.getActionProvider(mShareMenuItem);
        shareActionProvider.setShareIntent(AppUtils.getShareIntent(
                parentActivity, address, AppConstants.qrLastAddress));
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (noShare)
            mShareMenuItem.setVisible(false);
        else
            mShareMenuItem.setVisible(true);
    }

    private void getAddress() {
        mViewModel.onCleared();
        mViewModel.createAddress(addressType).observe(this, this::parseResponse);
    }

    private void parseResponse(GrpcResponse grpcResponse) {
        if (grpcResponse.getMessage() != null) {
            NewAddressResponse response = (NewAddressResponse) grpcResponse.getMessage();
            address = response.getAddress();
            showAddress();
        } else {
            if (grpcResponse.getErrorDescription() != null
                    && !grpcResponse.getErrorDescription().equals("")) {
                mCreateAddressButton.setEnabled(true);
                mLoadingPanel.setVisibility(View.GONE);
                Toast.makeText(parentActivity,
                        grpcResponse.getErrorDescription(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void showAddress() {
        address = AppConstants.uriSchemeBitcoin + address;
        if (amount > 0) {
            BigDecimal amountInBTC = new BigDecimal(amount)
                    .divide(new BigDecimal(Math.pow(10, 6)))
                    .setScale(8, RoundingMode.HALF_EVEN);
            address = address + AppConstants.amountParam + amountInBTC.toPlainString();
        }
        mAddressTextView.setText(address);
        restInterface.getQrcode(address).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call,
                                   @NonNull Response<ResponseBody> response) {
                if (response.body() != null) {
                    Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                    mQrcodeImageView.setImageBitmap(bmp);
                    AppUtils.saveImage(parentActivity, bmp, AppConstants.qrLastAddress);
                    noShare = false;
                    Objects.requireNonNull(getActivity()).invalidateOptionsMenu();
                } else {
                    noShare = true;
                    Objects.requireNonNull(getActivity()).invalidateOptionsMenu();
                    mQrcodeImageView.setImageResource(R.drawable.img_qrcat);
                }
                mLoadingPanel.setVisibility(View.GONE);
                mQrcodeImageView.setVisibility(View.VISIBLE);
                mAddressTextView.setVisibility(View.VISIBLE);
                mCreateAddressButton.setEnabled(true);
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                mQrcodeImageView.setImageResource(R.drawable.img_qrcat);
                mLoadingPanel.setVisibility(View.GONE);
                mQrcodeImageView.setVisibility(View.VISIBLE);
                mAddressTextView.setVisibility(View.VISIBLE);
                mCreateAddressButton.setEnabled(true);
            }
        });
    }

    private void hideAddress() {
        mQrcodeImageView.setVisibility(View.GONE);
        mAddressTextView.setVisibility(View.GONE);
    }

}
