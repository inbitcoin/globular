/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.pairing;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.prefs.PreferencesHelper;
import it.inbitcoin.globular.ui.settings.SettingsFragment;

public class ConnectionCheckFragment extends DaggerFragment {

    private static final String TAG = ConnectionCheckFragment.class.getSimpleName();

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private PairingViewModel mViewModel;

    @Inject
    PreferencesHelper mPreferencesHelper;

    private PairingActivity parentActivity;

    private TextView mPairingContinueText;

    private LinearLayout mAbortButton;
    private LinearLayout mFinishButton;
    private LinearLayout mContinueButton;
    private TextView mContinueButtonText;

    onConnectionCheckedListener connectionCheckedListener;


    @Inject
    public ConnectionCheckFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        parentActivity = (PairingActivity) getActivity();
        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(PairingViewModel.class);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            connectionCheckedListener = (onConnectionCheckedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    parentActivity.toString() + " must implement onConnectionCheckedListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(PairingViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_conn_check, container, false);
        mPairingContinueText = root.findViewById(R.id.pairing_continue);
        mAbortButton = root.findViewById(R.id.btn_abort);
        mFinishButton = root.findViewById(R.id.btn_finish);
        mContinueButton = root.findViewById(R.id.btn_continue);
        mContinueButtonText = root.findViewById(R.id.btn_continue_text);
        if (mViewModel.cert != null && !mViewModel.cert.isEmpty()) {
            mPairingContinueText.setText(R.string.pairing_cert);
        } else {
            mPairingContinueText.setText(R.string.pairing_no_cert);
            mContinueButton.setEnabled(false);
            mContinueButtonText.setTextColor(
                    getResources().getColor(R.color.colorPrimaryDark, null));
        }
        setListeners();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setListeners() {
        mAbortButton.setOnClickListener(v -> parentActivity.finish());
        mFinishButton.setOnClickListener(v -> {
            parentActivity.saveConnectionData();
            parentActivity.setResult(SettingsFragment.LIGHTER_PAIRING_OK);
            parentActivity.finish();
        });
        mContinueButton.setOnClickListener(v -> {
            connectionCheckedListener.connectionChecked();
        });
    }

    public interface onConnectionCheckedListener{
        void connectionChecked();
    }

}
