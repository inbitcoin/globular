/*
 * Globular - a Lightning Network Wallet
 * Copyright (C) 2019  inbitcoin s.r.l.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.inbitcoin.globular.ui.send;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.protobuf.GeneratedMessageLite;

import net.i2p.android.ext.floatingactionbutton.FloatingActionButton;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import it.inbitcoin.globular.DecodeInvoiceResponse;
import it.inbitcoin.globular.PayInvoiceResponse;
import it.inbitcoin.globular.R;
import it.inbitcoin.globular.data.model.GrpcResponse;
import it.inbitcoin.globular.utils.AccessFactory;
import it.inbitcoin.globular.utils.AppConstants;
import it.inbitcoin.globular.utils.AppUtils;

public class SendOffChainFragment extends DaggerFragment {

    private static final String TAG = "SendOffChainFragment";

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private SendViewModel mViewModel;

    @Inject
    AccessFactory mAccessFactory;

    private SendActivity parentActivity;

    private TextView mPaymentRequestTextView;
    private EditText mAmountEditText;
    private FloatingActionButton mPayButton;

    private double mAmount;

    private ProgressBar mProgressBar;


    @Inject
    public SendOffChainFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        parentActivity = (SendActivity) getActivity();

        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(SendViewModel.class);

        if (getArguments() != null) {
            String paymentRequest = getArguments().getString(AppConstants.paymentRequest);

            // LN invoices should appear lower case
            paymentRequest = paymentRequest.toLowerCase()
                    .replaceAll(AppConstants.uriSchemeLightning, "").trim();

            mViewModel.decodeInvoice(paymentRequest).observe(this, this::parseGrpcResponse);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(SendViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_send_offchain, container, false);

        mPaymentRequestTextView = root.findViewById(R.id.tv_payment_request);
        mAmountEditText = root.findViewById(R.id.et_send_amount);
        mAmountEditText.setEnabled(false);
        mProgressBar = root.findViewById(R.id.pb_send);
        mPayButton = root.findViewById(R.id.btn_pay);
        mPayButton.setEnabled(false);
        setListeners();

        return root;
    }

    public void setListeners() {
        mPayButton.setOnClickListener(v -> {
            getUserInput();
            mPayButton.setEnabled(false);
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.VISIBLE);
            payOffChain();
        });
        mAmountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getUserInput();
                if (mAmount > 0) {
                    mPayButton.setEnabled(true);
                } else {
                    mPayButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mAmountEditText.addTextChangedListener(
                new AppUtils.DecimalInputWatcher(mAmountEditText, 5, 5, 0, 42949.67296));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            parentActivity.finish();
        } catch (NullPointerException e) {
            Log.d(TAG, e.toString());
        }
    }

    private void getUserInput() {
        try {
            mAmount = Double.parseDouble(mAmountEditText.getText().toString());
        } catch (NumberFormatException ignored) {}
    }

    private void payOffChain() {
        mViewModel.payOffChain(mAmount).observe(this, this::parseGrpcResponse);
    }

    private void parseGrpcResponse(GrpcResponse grpcResponse) {
        Log.d(TAG, "parsing gRPC response");
        if (grpcResponse == null)
            return;
        mProgressBar.setVisibility(View.INVISIBLE);
        if (grpcResponse.getMessage() != null) {
            GeneratedMessageLite responseType =
                    grpcResponse.getMessage().getDefaultInstanceForType();
            if (responseType == DecodeInvoiceResponse.getDefaultInstance()) {
                DecodeInvoiceResponse response = (DecodeInvoiceResponse) grpcResponse.getMessage();
                Log.d(TAG, "decode " + response);
                if (mViewModel.cachedPaymentRequest != null)
                    mPaymentRequestTextView.setText(mViewModel.cachedPaymentRequest);
                mAmount = response.getAmountBits();
                Log.d(TAG, String.format("amount %.5f", mAmount));
                if (mAmount == 0 && mViewModel.hasAmountEncoded()) {
                    // quick solution to lnd being unable to detect amount under 1 satoshi
                    Toast.makeText(parentActivity, R.string.decode_bug, Toast.LENGTH_LONG).show();
                    parentActivity.finish();
                }
                showAmount();
            } else if (responseType == PayInvoiceResponse.getDefaultInstance()) {
                PayInvoiceResponse response = (PayInvoiceResponse) grpcResponse.getMessage();
                showPaymentPreimage(response.getPaymentPreimage());
                parentActivity.finish();
            }
        } else if (grpcResponse.getErrorDescription() != null &&
                !grpcResponse.getErrorDescription().equals("")) {
            Toast.makeText(parentActivity,
                    grpcResponse.getErrorDescription(), Toast.LENGTH_LONG).show();
            parentActivity.finish();
        } else {
            Toast.makeText(parentActivity, getString(R.string.error), Toast.LENGTH_LONG).show();
            parentActivity.finish();
        }
    }

    private void showPaymentPreimage(String paymentPreimage) {
        Toast.makeText(parentActivity, getString(R.string.paid_invoice) + " " + paymentPreimage,
                Toast.LENGTH_LONG).show();
    }

    private void showAmount() {
        mAmountEditText.setEnabled(false);
        if (mAmount != 0) {
            mAmountEditText.setText(String.valueOf(mAmount));
            mPayButton.setEnabled(true);
        } else {
            mAmountEditText.setEnabled(true);
        }
    }

}

